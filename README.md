# Odin
Fast, Easy-to-use and Customizable Java object to text serialization.  
All information about the library : https://www.newpixel.coffee/odin/

This README is a summary of the documentation available on the website.

## Introduction
Odin is a Java library that can be used for object serialization, configuration files, network transfers or whatever you want.  
This library comes with a new data format: odn ( Object Data Notation ), 
specially designed to remove the limitations of current serialization formats like JSON or XML.

The odn format has been created with multiple objectives:  
 - Being able to serialize all POO patterns without any code modification.
 - Being usable for all the needs in terms of text files: properties, i18n, data...
 - Being easy to read and modify.

### Format
Output for an object using abstract class, type definitions are added in order to know the type during reading.  
Odin contains an advanced reflect types manager to minimize the use of these type definitions.
```odn  
{
    date = <local datetime> "2018-02-25 21:05:30"
    anObject = <com.newpixelcoffee.odin.AnObject> {
        value = "2018-02-25 21:05:30"
    }
}
```
The format can be indented or compressed, by default Odin compress the output.
```odn
{a string="In short format line breaks are escape",a int=1}
```

The format can also contains comments and blank lines with the Odin elements model
```odn
# Empty lines can be inserted between or around comments
 
a boolean = true
a node = {
    # Comments and blank lines are kept during the rewriting
    a text = "
        Odin can also read and write
        Multilines String like this
        containing two \n
    "
}
```


## Basic usages
To start using the library, there is one class to know: Odin. This class manages every input, output and types.
You can simply declare a new instance without any constructor parameters.
```java
Odin odin = new Odin();
```

You can use the One Line Serialization to get the odn result of an object.
```java
String s = odin.toOdn(objectToSerialize);
String sWithAKnowType = odin.toOdn(objectToSerialize, Foo.class);
```

And same for the deserialization.
```java
Foo result = odin.fromOdn(s);
Foo result2 = odin.fromOdn(sWithAKnowType, Foo.class);
```

You can also get the **OdinWriter** or **OdinReader** to save whatever you want wherever you want.  
Odin can take **Writer**, **OutputStream**, **File** and **Path** to create the writer and 
**Reader**, **InputStream**, **File** and **Path** to create the reader.
```java
try (OdinWriter writer = odin.writer(new FileWriter(new File("destination")))) {
    // write objects
} catch (IOException e) {
    // manage exception
}
```

## Download
```gradle
dependencies {
    implementation 'com.newpixelcoffee.odin:odin:1.0.+'
}
```

```xml
<dependency>
    <groupId>com.newpixelcoffee.odin</groupId>
    <artifactId>odin</artifactId>
    <version>1.0.+</version>
</dependency>
```

Manual download on the website

## Functionality
Odin contains:
 - Parent references to handle infinite looping
 - Some embedded adapters to save most used Java objects and collections
 - The possibility to customize the default object Adapter (skip nulls, rename fields, exceptions strategy, ...)
 - The possibility to create your own adapter
 - And much more...