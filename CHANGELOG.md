# Changelog File
This file list all release changes of Odin.  
Versions use the following format: **major.minor.internal**

## [1.2.d] - 2020-09-04
### Fixed
 - Fixed Odin#setReaderBufferSize returning a new instance instead of current

## [1.2.c] - 2020-06-23
### Fixed
 - Fixed upward fields search in the default object adapter

## [1.2.b] - 2020-02-08
### Fixed
 - Fixed integer read as float when using row.take without type in case of indented odn

## [1.2.a] - 2020-01-30
### Added
 - A way to filter the fields that are in object type and serialized by the default object adapter
### Changed
 - Default collections adapters (list, set and map) are no longer used in every implementations and need to be registered manually
 - You can now use the registerAsXX methods in Odin to add an implementation to these default collections adapters
 - Updating gradle to 6.1.1
### Fixed
 - Related to the changes, fixed all NPE during the deserialization of immutable collections (Singletons, Arrays$ArrayList, ...)

## [1.1.a] - 2019-12-29
### Added
 - Possibility to write into an object sub content using a consumer
 - Introduction to the library in the README.md file
 - This CHANGELOG.md file
#### Fixed
 - Git connection URL in the generated maven pom

## [1.0.c] - 2019-12-27
#### Fixed
 - Default object adapter saying read instead of write in exception message.
 - NPE for Comparable containing a primitive.

## [1.0.b] - 2019-12-26
#### Added
 - Automatic maven publication to SonaType
#### Changed
 - Some javadoc typo