package com.newpixelcoffee.odin.internal.defaults;

import com.newpixelcoffee.odin.Odin;
import com.newpixelcoffee.odin.Test3;
import com.newpixelcoffee.odin.Test5;
import com.newpixelcoffee.odin.exceptions.ExceptionsStrategy;
import com.newpixelcoffee.odin.exceptions.OdinAdapterException;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author DocVander
 */
class DefaultObjectAdapterTest {

    @Test
    void testNoSkipFormatException_shouldThrow() {
        String s = "{x=true,y=5,z={key=false}}";
        Odin odin = new Odin();
        assertThrows(OdinAdapterException.class, () -> odin.fromOdn(s, Test5.class));
    }

    @Test
    void testSkipFormatException_shouldNotThrow() {
        String s = "{x=true,y=5,z={key=false}}";
        Odin odin = new Odin();
        odin.getDefaultAdapterOption().setReaderExceptionStrategy(ExceptionsStrategy.CONTINUE_EXECUTION);
        Test5 test = odin.fromOdn(s, Test5.class);
        assertEquals(5, test.getY());
    }

    @Test
    void testReadInReverseOrder() {
        UUID randomId = UUID.randomUUID();
        String s = "{list=<array list>{\"" + randomId + "\"},id=\"test\"}";
        Odin odin = new Odin();
        Test3<String> result = odin.fromOdn(s, Test3.class);
        assertEquals("test", result.id);
        assertNotNull(result.getList());
        assertEquals(1, result.getList().size());
        assertEquals(randomId, result.getList().get(0));
    }
}