package com.newpixelcoffee.odin.internal.defaults;

import com.newpixelcoffee.odin.Odin;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author DocVander
 */
class DefaultLocalDateTimeAdapterTest {

    @Test
    void testFormat() {
        LocalDateTime t = LocalDateTime.of(LocalDate.of(2019, 10, 21), LocalTime.of(12, 0, 0, 568923));
        String result = new Odin().toOdn(t, LocalDateTime.class);

        assertEquals("\"2019-10-21 12:00:00.000568923\"", result);
    }

    @Test
    void testFormat_noNano() {
        LocalDateTime t = LocalDateTime.of(LocalDate.of(2019, 10, 21), LocalTime.of(12, 0, 0, 0));
        String result = new Odin().toOdn(t, LocalDateTime.class);

        assertEquals("\"2019-10-21 12:00:00\"", result);
    }

    @Test
    void read() {
        LocalDateTime date = new Odin().fromOdn("\"2019-10-21 12:00:00\"", LocalDateTime.class);
        assertEquals(2019, date.getYear());
        assertEquals(10, date.getMonthValue());
        assertEquals(21, date.getDayOfMonth());
        assertEquals(12, date.getHour());
        assertEquals(0, date.getMinute());
        assertEquals(0, date.getSecond());
    }
}