package com.newpixelcoffee.odin.internal.defaults;

import com.newpixelcoffee.odin.Odin;
import com.newpixelcoffee.odin.Test5;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author DocVander
 */
class DefaultClassAdapterTest {

    @Test
    void read() {
        Class<?> clazz = new Odin().fromOdn("<class>\"com.newpixelcoffee.odin.Test5\"");
        assertEquals(Test5.class, clazz);
    }

    @Test
    void write() {
        assertEquals("<class>\"com.newpixelcoffee.odin.Test5\"", new Odin().toOdn(Test5.class));
    }
}