package com.newpixelcoffee.odin.internal.streams;

import com.newpixelcoffee.odin.exceptions.OdinFormatException;
import org.junit.jupiter.api.Test;

import java.io.EOFException;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author DocVander
 */
class StreamReaderTest {

    private static final int TU_BUFFER_SIZE = 64;

    @Test
    void readBoolean_eof() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("true"), TU_BUFFER_SIZE);

        boolean b = reader.readBoolean();

        assertTrue(b);
        reader.close();
    }

    @Test
    void readBoolean() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("true,3553453"), TU_BUFFER_SIZE);

        boolean b = reader.readBoolean();

        assertTrue(b);
        assertEquals(3553453, reader.readInt());
        reader.close();
    }

    @Test
    void readBoolean_space() throws IOException {
        StreamReader reader = new StreamReader(new StringReader(" \t true"), TU_BUFFER_SIZE);

        assertTrue(reader.readBoolean());
        reader.close();
    }

    @Test
    void readInt_eof() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("3553453"), TU_BUFFER_SIZE);

        int i = reader.readInt();

        assertEquals(3553453, i);
        reader.close();
    }

    @Test
    void readInt() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("3553453,true"), TU_BUFFER_SIZE);

        int i = reader.readInt();

        assertEquals(3553453, i);
        assertTrue(reader.readBoolean());
        reader.close();
    }

    @Test
    void readNan_eof() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("NaN"), TU_BUFFER_SIZE);

        float f = reader.readFloat();

        assertEquals(Float.NaN, f);
        reader.close();
    }

    @Test
    void readNan() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("NaN,true"), TU_BUFFER_SIZE);

        float f = reader.readFloat();

        assertEquals(Float.NaN, f);
        assertTrue(reader.readBoolean());
        reader.close();
    }

    @Test
    void readFloat_eof() throws IOException {
        String s = "5.46E-5";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        float f = reader.readFloat();

        assertEquals(Float.parseFloat(s), f);
        reader.close();
    }

    @Test
    void readFloat() throws IOException {
        String s = "5.46E-5";
        StreamReader reader = new StreamReader(new StringReader(s+",true"), TU_BUFFER_SIZE);

        float f = reader.readFloat();

        assertEquals(Float.parseFloat(s), f);
        assertTrue(reader.readBoolean());
        reader.close();
    }

    @Test
    void readFloat_space() throws IOException {
        String s = "5.46E-5";
        StreamReader reader = new StreamReader(new StringReader(" \t "+s), TU_BUFFER_SIZE);
        assertEquals(Float.parseFloat(s), reader.readFloat());
        reader.close();
    }

    @Test
    void readFloat_multipleBuffer() throws IOException {
        String s = "5.46E-5";
        StreamReader reader = new StreamReader(new StringReader("                                                          "+s), TU_BUFFER_SIZE);
        assertEquals(Float.parseFloat(s), reader.readFloat());
        reader.close();
    }

    @Test
    void readDouble_1() throws IOException {
        double d = -0.0001;
        String s = Double.toString(d);
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        double r = reader.readDouble();
        assertEquals(Double.parseDouble(s), r);
        reader.close();
    }

    @Test
    void readDouble_2() throws IOException {
        double d = 0.20;
        String s = Double.toString(d);
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        double r = reader.readDouble();
        assertEquals(Double.parseDouble(s), r);
        reader.close();
    }

    @Test
    void readDouble_3() throws IOException {
        double d = 1454.35354142111;
        String s = Double.toString(d);
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        double r = reader.readDouble();
        assertEquals(Double.parseDouble(s), r);
        reader.close();
    }

    @Test
    void readDouble_pi() throws IOException {
        double d = Math.PI;
        String s = Double.toString(d);
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        double r = reader.readDouble();
        assertEquals(Double.parseDouble(s), r);
        reader.close();
    }

    @Test
    void readDouble_5() throws IOException {
        double d = 9.99999999;
        String s = Double.toString(d);
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        double r = reader.readDouble();
        assertEquals(Double.parseDouble(s), r);
        reader.close();
    }

    @Test
    void readBiggerThanContent() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("1"), TU_BUFFER_SIZE);
        assertFalse(reader.readNull());
        assertEquals(1, reader.readInt());
        reader.close();
    }

    @Test
    void readInt_shouldThrowException_whenNotInt() throws IOException {
        String s = "hello=5";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertThrows(OdinFormatException.class, reader::readInt);
        reader.close();
    }

    @Test
    void readInt_shouldThrowException_whenNotFullInt() throws IOException {
        String s = "5 test=5";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertThrows(OdinFormatException.class, reader::readInt);
        reader.close();
    }

    @Test
    void readBoolean_shouldThrowException_whenNotFullInt() throws IOException {
        String s = "true or false";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertThrows(OdinFormatException.class, reader::readBoolean);
        reader.close();
    }

    @Test
    void readString_withSeparator() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("key = \"  bonjour\\nle monde \""), TU_BUFFER_SIZE);

        assertEquals("key", reader.readKey());
        assertEquals("  bonjour\nle monde ", reader.readString());
        reader.close();
    }

    @Test
    void readString_multiline() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("key = \"  bonjour\n\t\tle monde \""), TU_BUFFER_SIZE);

        assertEquals("key", reader.readKey());
        assertEquals("  bonjour\nle monde ", reader.readString());
        reader.close();
    }

    @Test
    void readString_multilineLf() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("key = \"\n\tbonjour\n\tle monde\n\""), TU_BUFFER_SIZE);

        assertEquals("key", reader.readKey());
        assertEquals("bonjour\nle monde", reader.readString());
        reader.close();
    }

    @Test
    void readString_multilineCrLf() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("key = \"\r\n\tbonjour\r\n\tle monde\r\n\""), TU_BUFFER_SIZE);

        assertEquals("key", reader.readKey());
        assertEquals("bonjour\r\nle monde", reader.readString());
        reader.close();
    }

    @Test
    void readString_multilineCr() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("key = \"\r\tbonjour\r\tle monde\r\""), TU_BUFFER_SIZE);

        assertEquals("key", reader.readKey());
        assertEquals("bonjour\rle monde", reader.readString());
        reader.close();
    }

    @Test
    void readString_multilineEscape() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("key = \"\n\t\tbonjour\r\n\t\t\\tle monde\n\t\""), TU_BUFFER_SIZE);

        assertEquals("key", reader.readKey());
        assertEquals("bonjour\r\n\tle monde", reader.readString());
        reader.close();
    }

    @Test
    void readString_multilineEscapeSpace() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("key = \"\n\tbonjour\r\n\t\\ le monde\n\""), TU_BUFFER_SIZE);

        assertEquals("key", reader.readKey());
        assertEquals("bonjour\r\n le monde", reader.readString());
        reader.close();
    }

    @Test
    void readString_WithMultipleBuffer() throws IOException {
        String s = "Hello, this is a very long text in order to test buffer cache in string reader, the buffer length is 32 so i think this string is good";
        StreamReader reader = new StreamReader(new StringReader("key = \""+s+"\""), TU_BUFFER_SIZE);

        assertEquals("key", reader.readKey());
        assertEquals(s, reader.readString());
        reader.close();
    }

    @Test
    void readString_empty() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("key = \"\""), TU_BUFFER_SIZE);

        assertEquals("key", reader.readKey());
        assertEquals("", reader.readString());
        reader.close();
    }

    @Test
    void readString_eof() throws IOException {
        StreamReader reader = new StreamReader(new StringReader("key = \"  bonjour\\nle monde "), TU_BUFFER_SIZE);

        assertEquals("key", reader.readKey());
        assertThrows(EOFException.class, reader::readString);
        reader.close();
    }

    @Test
    void readValue_cr() throws IOException {
        String s = "   \tHello this is a good test\r10";
        String r = "Hello this is a good test";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals(r, reader.readValue());
        assertEquals(10, reader.readInt());
        reader.close();
    }

    @Test
    void readValue_crlf() throws IOException {
        String s = "   \tHello this is a good test\n\r10";
        String r = "Hello this is a good test";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals(r, reader.readValue());
        assertEquals(10, reader.readInt());
        reader.close();
    }

    @Test
    void readValue_lf() throws IOException {
        String s = "   \tHello this is a\\t good test\n10";
        String r = "Hello this is a\t good test";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals(r, reader.readValue());
        assertEquals(10, reader.readInt());
        reader.close();
    }

    @Test
    void readValue_escape() throws IOException {
        String s = "hello world\\nok\\r\\n";
        String r = "hello world\nok\r\n";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals(r, reader.readValue());
        reader.close();
    }

    @Test
    void readKey_WithMultipleBuffer() throws IOException {
        String s = "   \tHello                                                                    this is a good test                                                =";
        String r = "Hello                                                                    this is a good test";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals(r, reader.readKey());
        reader.close();
    }

    @Test
    void readKey_WithMultipleBuffer2() throws IOException {
        String s = "   \tHello                                                                    this is a good test                          this is a good test                                   " +
                "             =";
        String r = "Hello                                                                    this is a good test                          this is a good test";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals(r, reader.readKey());
        reader.close();
    }

    @Test
    void readKey_WithStart() throws IOException {
        String s = "hello {";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals("hello", reader.readKey());
        assertTrue(reader.eat('{'));
        reader.close();
    }

    @Test
    void readKey_WithType() throws IOException {
        String s = "hello <";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals("hello", reader.readKey());
        assertTrue(reader.eat('<'));
        reader.close();
    }

    @Test
    void readKey_escape() throws IOException {
        String s = "hello\\tworld\\r\\n=";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals("hello\tworld\r\n", reader.readKey());
        reader.close();
    }

    @Test
    void readKey_trimmed() throws IOException {
        String s = "\\ hello\\tworld    \\\t=";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals(" hello\tworld    \t", reader.readKey());
        reader.close();
    }

    @Test
    void readType() throws IOException {
        String s = "<a bc>";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertTrue(reader.eat('<'));
        assertEquals("a bc", reader.readType());
        reader.close();
    }

    @Test
    void readType_eof() throws IOException {
        String s = "<a bcd";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertTrue(reader.eat('<'));
        assertThrows(EOFException.class, reader::readType);
        reader.close();
    }

    @Test
    void readComment() throws IOException {
        String s = "hello world";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals(s, reader.readComment());
        reader.close();
    }

    @Test
    void readComments() throws IOException {
        String s = "hello world\n# second line";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals("hello world", reader.readComment());
        assertTrue(reader.eat('#'));
        assertEquals("second line", reader.readComment());
        reader.close();
    }

    @Test
    void readComments_withSeparator() throws IOException {
        String s = "hello world, containing delimiter = : {ok}";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals(s, reader.readComment());
        reader.close();
    }

    @Test
    void readComments_withEscape() throws IOException {
        String s = "hello world\\n\\ta";
        StreamReader reader = new StreamReader(new StringReader(s), TU_BUFFER_SIZE);

        assertEquals("hello world\n\ta", reader.readComment());
        reader.close();
    }
}