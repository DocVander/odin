package com.newpixelcoffee.odin.internal.streams;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author DocVander
 */
class StreamWriterTest {

    private String newLine = System.getProperty("line.separator");

    @Test
    void writeBoolean() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeBoolean(true);
        stream.close();

        assertEquals("true", s.toString());
    }

    @Test
    void writeInt() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeInt(10);
        stream.close();

        assertEquals("10", s.toString());
    }

    @Test
    void writeLong() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeLong(106546468798L);
        stream.close();

        assertEquals("106546468798", s.toString());
    }

    @Test
    void writeFloat() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeFloat(10.4898f);
        stream.close();

        assertEquals("10.4898", s.toString());
    }

    @Test
    void writeDouble() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeDouble(0.49874d);
        stream.close();

        assertEquals("0.49874", s.toString());
    }

    @Test
    void writeChar() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeChar('t');
        stream.close();

        assertEquals("'t'", s.toString());
    }

    @Test
    void writeChar_escape1() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeChar('\t');
        stream.close();

        assertEquals("'\\t'", s.toString());
    }

    @Test
    void writeChar_escape2() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeChar('\n');
        stream.close();

        assertEquals("'\\n'", s.toString());
    }

    @Test
    void writeString() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeString("Hello world");
        stream.close();

        assertEquals("\"Hello world\"", s.toString());
    }

    @Test
    void writeString_escapeLf() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeString("Hello world\ttest\n");
        stream.close();

        assertEquals("\"Hello world\\ttest\\n\"", s.toString());
    }

    @Test
    void writeString_escapeCrLf() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeString("Hello world\ttest\r\n");
        stream.close();

        assertEquals("\"Hello world\\ttest\\r\\n\"", s.toString());
    }

    @Test
    void writeString_escapeCr() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeString("Hello world\ttest\r");
        stream.close();

        assertEquals("\"Hello world\\ttest\\r\"", s.toString());
    }

    @Test
    void writeString_multilineLr() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.changeIndentation("");
        stream.writeString("Hello world\ntest");
        stream.close();

        assertEquals("\""+newLine+"\tHello world\n\ttest"+newLine+"\"", s.toString());
    }

    @Test
    void writeString_multilineCrLf() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.changeIndentation("");
        stream.writeString("Hello world\r\ntest");
        stream.close();

        assertEquals("\""+newLine+"\tHello world\r\n\ttest"+newLine+"\"", s.toString());
    }

    @Test
    void writeString_multilineCr() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.changeIndentation("");
        stream.writeString("Hello world\rtest");
        stream.close();

        assertEquals("\""+newLine+"\tHello world\r\ttest"+newLine+"\"", s.toString());
    }

    @Test
    void writeString_multilineSpace() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.changeIndentation("");
        stream.writeString("Hello world\n i");
        stream.close();

        assertEquals("\""+newLine+"\tHello world\n\t\\ i"+newLine+"\"", s.toString());
    }

    @Test
    void writeString_multilineEscape() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.changeIndentation("");
        stream.writeString("Hello world\n\ti");
        stream.close();

        assertEquals("\""+newLine+"\tHello world\n\t\\ti"+newLine+"\"", s.toString());
    }

    @Test
    void writeKey() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeKey("test value");
        stream.close();

        assertEquals("test value", s.toString());
    }

    @Test
    void writeKey_escape() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.changeIndentation("");
        stream.writeKey("test value\n\t1");
        stream.close();

        assertEquals("test value\\n\\t1", s.toString());
    }

    @Test
    void writeNull() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeNull();
        stream.close();

        assertEquals("null", s.toString());
    }

    @Test
    void writeType() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.writeType("object");
        stream.close();

        assertEquals("<object>", s.toString());
    }

    @Test
    void feed() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.feed("blbl");
        stream.close();

        assertEquals("blbl", s.toString());
    }

    @Test
    void feed_char() throws IOException {
        StringWriter s = new StringWriter();
        StreamWriter stream = new StreamWriter(s);

        stream.feed('c');
        stream.close();

        assertEquals("c", s.toString());
    }
}