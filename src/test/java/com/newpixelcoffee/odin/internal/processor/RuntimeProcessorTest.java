package com.newpixelcoffee.odin.internal.processor;

import com.newpixelcoffee.odin.*;
import com.newpixelcoffee.odin.access.OdinRowReader;
import com.newpixelcoffee.odin.adapters.OdinObjectAdapter;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author DocVander
 */
class RuntimeProcessorTest {

    @Test
    void read_generic() {
        RuntimeProcessorTestFoo foo = new Odin().fromOdn("{1:2:3}", RuntimeProcessorTestFoo.class);
        Test5 test = foo.test5;
        assertNotNull(test);
        assertEquals(1, test.getX());
        assertEquals(2, test.getY());
        assertEquals(3, test.getZ());
    }

    @Test
    void write_generic() {
        assertEquals("{1:2:3}", new Odin().toOdn(new RuntimeProcessorTestFoo(new Test5(1, 2, 3)), RuntimeProcessorTestFoo.class));
    }

    public static class RuntimeProcessorTestFoo implements OdinObjectAdapter<RuntimeProcessorTestFoo> {
        private Test5 test5;

        public RuntimeProcessorTestFoo(Test5 test5) {
            this.test5 = test5;
        }

        @Override
        public void read(OdinReader reader, ObjectType<? extends RuntimeProcessorTestFoo> type, RuntimeProcessorTestFoo o) throws IOException {
            OdinRowReader row = reader.readRow();
            test5 = new Test5(row.takeInt(), row.takeInt(), row.takeInt());
        }

        @Override
        public void write(OdinWriter writer, ObjectType<? extends RuntimeProcessorTestFoo> type, RuntimeProcessorTestFoo o) throws IOException {
            writer.writeRow().addInt(o.test5.getX()).addInt(o.test5.getY()).addInt(o.test5.getZ());
        }
    }
}