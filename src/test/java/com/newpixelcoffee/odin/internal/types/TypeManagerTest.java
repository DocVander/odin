package com.newpixelcoffee.odin.internal.types;

import com.newpixelcoffee.odin.*;
import com.newpixelcoffee.odin.adapters.OdinInlineAdapter;
import com.newpixelcoffee.odin.adapters.OdinObjectAdapter;
import com.newpixelcoffee.odin.annotations.OdinCustomAdapter;
import com.newpixelcoffee.odin.exceptions.OdinTypeException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author DocVander
 */
class TypeManagerTest {

    @Test
    void register_shouldThrowException_forPrimitive() {
        TypeManager types = new TypeManager();
        assertThrows(OdinTypeException.class, () -> types.register(new OdinObjectAdapter<Integer>() {
            @Override
            public void read(OdinReader reader, ObjectType<? extends Integer> type, Integer o) {
                // test, nothing to do
            }

            @Override
            public void write(OdinWriter writer, ObjectType<? extends Integer> type, Integer o) {
                // test, nothing to do
            }
        }));
    }

    @Test
    void testEnum_unknown() throws IOException {
        Odin odin = new Odin();
        String s = odin.toOdn(Test2.SUCCESS);
        assertEquals("<com.newpixelcoffee.odin.Test2>\"SUCCESS\"", s);
    }

    @Test
    void testArray_typeExtends() throws IOException {
        Test2[] c = { Test2.SUCCESS, Test2.ERROR };

        Odin odin = new Odin();
        String s = odin.toOdn(c, new TypeBuilder<Object[]>() {}.type);
        assertEquals("<[com.newpixelcoffee.odin.Test2>[\"SUCCESS\",\"ERROR\"]", s);
    }

    @Test
    void testArray_typeExtendsUnknown() throws IOException {
        Test2[] c = { Test2.SUCCESS, Test2.ERROR };

        Odin odin = new Odin();
        String s = odin.toOdn(c);
        assertEquals("<[com.newpixelcoffee.odin.Test2>[\"SUCCESS\",\"ERROR\"]", s);
    }

    @Test
    void testArray_typeKnow() throws IOException {
        Test2[] c = { Test2.SUCCESS, Test2.ERROR };

        Odin odin = new Odin();
        String s = odin.toOdn(c, new TypeBuilder<Test2[]>() {}.type);
        assertEquals("[\"SUCCESS\",\"ERROR\"]", s);
    }

    @Test
    void testGenericMap() throws IOException {
        Map<Test2, Test2> m = new HashMap<>();
        m.put(Test2.SUCCESS, Test2.ERROR);

        Odin odin = new Odin();
        String s = odin.toOdn(m, new TypeBuilder<Map<Test2, Test2>>() {}.type);
        assertEquals("<hash map>{\"SUCCESS\":\"ERROR\"}", s);
    }

    @Test
    void testGenericMap_know() throws IOException {
        Map<Test2, Test2> m = new HashMap<>();
        m.put(Test2.SUCCESS, Test2.ERROR);

        Odin odin = new Odin();
        String s = odin.toOdn(m, new TypeBuilder<HashMap<Test2, Test2>>() {}.type);
        assertEquals("{\"SUCCESS\":\"ERROR\"}", s);
    }

    @Test
    void testGenericMap_typeChange() throws IOException {
        Map<Test2, Test2> m = new Test6<>();
        m.put(Test2.SUCCESS, Test2.ERROR);

        Odin odin = new Odin();
        String s = odin.toOdn(m, new TypeBuilder<Map<Test2, Test2>>() {}.type);
        assertEquals("<com.newpixelcoffee.odin.Test6>{\"SUCCESS\":\"ERROR\"}", s);
    }

    @Test
    void testGenericMap_typeChangeKnow() throws IOException {
        Map<Test2, Test2> m = new Test6<>();
        m.put(Test2.SUCCESS, Test2.ERROR);

        Odin odin = new Odin();
        String s = odin.toOdn(m, new TypeBuilder<Test6<Test2>>() {}.type);
        assertEquals("{\"SUCCESS\":\"ERROR\"}", s);
    }

    @Test
    void testGenericMap_typeUnknown() throws IOException {
        Map<Test2, Test2> m = new Test6<>();
        m.put(Test2.SUCCESS, Test2.ERROR);

        Odin odin = new Odin();
        String s = odin.toOdn(m);
        assertEquals("<com.newpixelcoffee.odin.Test6>{\"SUCCESS\":<com.newpixelcoffee.odin.Test2>\"ERROR\"}", s);
    }

    @Test
    void testFieldFilter_write() {
        Odin odin = new Odin();
        odin.setFieldFilter(field -> field.getType() == String.class);

        String s = odin.toOdn(new Test7("test", new Test5(1, 2, 3)), Test7.class);
        assertEquals("{name=\"test\"}", s);
    }

    @Test
    void testFieldFilter_read() {
        Odin odin = new Odin();
        odin.setFieldFilter(field -> field.getType() == String.class);

        Test7 test = odin.fromOdn("{name=\"test\",value={x=1,y=2,z=3}}", Test7.class);
        assertEquals("test", test.getName());
        assertNull(test.getValue());
    }

    @Test
    void testDateFormatChange() {
        Odin odin = new Odin();
        odin.setDefaultDateFormat(LocalDate.class, DateTimeFormatter.ofPattern("'day' D"));

        String s = odin.toOdn(LocalDate.of(2020, 1, 15));
        assertEquals("<local date>\"day 15\"", s);
    }

    @Test
    void read_customAdapter() {
        CustomAdapterFoo foo = new Odin().fromOdn("{test5=\"1/2/3\"}", CustomAdapterFoo.class);
        Test5 test = foo.test5;
        assertNotNull(test);
        assertEquals(1, test.getX());
        assertEquals(2, test.getY());
        assertEquals(3, test.getZ());
    }

    @Test
    void write_customAdapter() {
        assertEquals("{test5=\"1/2/3\"}", new Odin().toOdn(new CustomAdapterFoo(new Test5(1, 2, 3)), CustomAdapterFoo.class));
    }

    public static class CustomAdapterFoo {
        @OdinCustomAdapter(adapter = CustomAdapterFooImpl.class)
        private Test5 test5;

        public CustomAdapterFoo(Test5 test5) {
            this.test5 = test5;
        }
    }

    public static class CustomAdapterFooImpl implements OdinInlineAdapter<Test5, String> {

        @Override
        public Test5 read(ObjectType<? extends Test5> type, String o) {
            String[] s = o.split("/");
            return new Test5(Integer.parseInt(s[0]), Integer.parseInt(s[1]), Integer.parseInt(s[2]));
        }

        @Override
        public String write(ObjectType<? extends Test5> type, Test5 o) {
            return o.getX()+"/"+o.getY()+"/"+o.getZ();
        }
    }

    @Test
    void read_classToPrimitive() {
        UnknownToPrimitiveTest foo = new Odin().fromOdn("{o=<Int>5,s=<string>\"test\"}", UnknownToPrimitiveTest.class);
        assertEquals(5, (int) foo.getO());
        assertEquals("test", foo.getS());
    }

    @Test
    void write_classToPrimitive() {
        UnknownToPrimitiveTest test = new UnknownToPrimitiveTest(5, "test");
        String result = new Odin().toOdn(test, UnknownToPrimitiveTest.class);
        assertEquals("{o=<Int>5,s=<string>\"test\"}", result);
    }

    public static class UnknownToPrimitiveTest {
        private Comparable<?> o;
        private CharSequence s;

        public UnknownToPrimitiveTest(Comparable<?> o, CharSequence s) {
            this.o = o;
            this.s = s;
        }

        public Object getO() {
            return o;
        }

        public CharSequence getS() {
            return s;
        }
    }
}