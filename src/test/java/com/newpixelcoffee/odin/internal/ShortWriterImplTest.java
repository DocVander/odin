package com.newpixelcoffee.odin.internal;

import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.elements.OdinArray;
import com.newpixelcoffee.odin.elements.OdinNode;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.TypeManager;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author DocVander
 */
class ShortWriterImplTest {

    @Test
    void writeNode_Boolean() throws IOException {
        OdinNode node = new OdinNode().run(n -> n.add("test", true));

        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeNode(node);
        writer.close();

        assertEquals("{test=true}", s.toString());
    }

    @Test
    void writeNode_SubNode() throws IOException {
        OdinNode node = new OdinNode().run(n -> {
            n.addNode("test").run(n2 -> n2.add("value", 10));
        });


        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeNode(node);
        writer.close();

        assertEquals("{test={value=10}}", s.toString());
    }

    @Test
    void writeNode_SubArray() throws IOException {
        OdinNode node = new OdinNode().run(n -> {
            n.addArray("test").run(a -> a.add(10));
        });

        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeNode(node);
        writer.close();

        assertEquals("{test=[10]}", s.toString());
    }

    @Test
    void writeNode_multiple() throws IOException {
        OdinNode node = new OdinNode().run(n -> {
            n.add("test", true);
            n.add("test 2", 10);
        });

        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeNode(node);
        writer.close();

        assertEquals("{test=true,test 2=10}", s.toString());
    }

    @Test
    void writeNode_null() throws IOException {
        OdinNode node = new OdinNode().run(n -> n.add("test", null));

        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeNode(node);
        writer.close();

        assertEquals("{test=null}", s.toString());
    }

    @Test
    void writeArray_Boolean() throws IOException {
        OdinArray array = new OdinArray().run(a -> a.add(true));

        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeArray(array);
        writer.close();

        assertEquals("[true]", s.toString());
    }

    @Test
    void writeArray_SubNode() throws IOException {
        OdinArray array = new OdinArray().run(a -> {
            a.addNode().run(n -> n.add("value", 10));
        });

        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeArray(array);
        writer.close();

        assertEquals("[{value=10}]", s.toString());
    }

    @Test
    void writeArray_SubArray() throws IOException {
        OdinArray array = new OdinArray().run(a -> {
            a.addArray().run(a2 -> a2.add(10));
        });

        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeArray(array);
        writer.close();

        assertEquals("[[10]]", s.toString());
    }

    @Test
    void writeArray_multiple() throws IOException {
        OdinArray array = new OdinArray().run(a -> {
            a.add(true);
            a.add(10);
        });

        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeArray(array);
        writer.close();

        assertEquals("[true,10]", s.toString());
    }

    @Test
    void writeArray_null() throws IOException {
        OdinArray array = new OdinArray().run(a -> a.add(null));

        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeArray(array);
        writer.close();

        assertEquals("[null]", s.toString());
    }

    @Test
    void writeRecursiveNode_0() throws IOException {
        OdinNode node = new OdinNode();
        node.add("this", node);

        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeNode(node);
        writer.close();

        assertEquals("{this=(0)}", s.toString());
    }

    @Test
    void writeRecursiveNode_1() throws IOException {
        OdinNode node = new OdinNode();
        node.addNode("test").run(n -> n.add("parent", node));

        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeNode(node);
        writer.close();

        assertEquals("{test={parent=(1)}}", s.toString());
    }

    @Test
    void writeUuid() throws IOException {
        UUID id = UUID.randomUUID();

        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.write(id);
        writer.close();

        assertEquals("<uuid>\""+id.toString()+"\"", s.toString());
    }

    @Test
    void writeEntries() throws IOException {
        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeField("t").withInt(10);
        writer.writeField("a").withBoolean(true);
        writer.writeField("b").withInt(5);
        writer.close();

        assertEquals("t=10,a=true,b=5", s.toString());
    }

    @Test
    void writeRow() throws IOException {
        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeRow().addBoolean(true).addInt(50).addInt(5);
        writer.close();

        assertEquals("true:50:5", s.toString());
    }

    @Test
    void writeSub_object() throws IOException {
        StringWriter s = new StringWriter();
        OdinWriter writer = new ShortWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeField("content").withSubContent((OdinWriter sub) -> {
            sub.writeField("age").withInt(15);
            sub.writeField("value").withInt(65464);
        });

        assertEquals("content={age=15,value=65464}", s.toString());
    }
}