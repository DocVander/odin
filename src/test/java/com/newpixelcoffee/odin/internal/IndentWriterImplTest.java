package com.newpixelcoffee.odin.internal;

import com.newpixelcoffee.odin.*;
import com.newpixelcoffee.odin.adapters.OdinObjectAdapter;
import com.newpixelcoffee.odin.elements.OdinNode;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.TypeManager;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author DocVander
 */
class IndentWriterImplTest {

    private String newLine = System.getProperty("line.separator");

    @Test
    void writeNode_commentsBefore() throws IOException {
        OdinNode node = new OdinNode();
        node.getExtra().addComment("test");
        node.getExtra().addComment("test 2");
        node.getExtra().addSpace();
        node.getExtra().addComment("test 3");

        StringWriter s = new StringWriter();
        OdinWriter writer = new IndentWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeNode(node);
        writer.close();

        assertEquals("# test"+newLine+"# test 2"+newLine+newLine+"# test 3"+newLine+"{"+newLine+"}", s.toString());
    }

    @Test
    void writeNode_commentContent() throws IOException {
        OdinNode node = new OdinNode();
        node.getExtra().addComment("test");
        node.getContentExtra().addComment("test");
        node.getContentExtra().addSpace();
        node.getContentExtra().addComment("test 2");

        StringWriter s = new StringWriter();
        OdinWriter writer = new IndentWriterImpl(new TypeManager(), new StreamWriter(s));

        writer.writeNode(node);
        writer.close();

        assertEquals("# test"+newLine+"{"+newLine+"\t# test"+newLine+"\t"+newLine+"\t# test 2"+newLine+"}", s.toString());
    }

    @Test
    void changeSubObject_Format() throws IOException {
        StringWriter s = new StringWriter();
        Odin odin = new Odin().indentOutput();
        odin.register(new OdinObjectAdapter<Test1>() {
            @Override
            public void read(OdinReader reader, ObjectType<? extends Test1> type, Test1 o) {
                // nothing
            }

            @Override
            public void write(OdinWriter writer, ObjectType<? extends Test1> type, Test1 o) throws IOException {
                writer.compressSubObjects();
                for (Test5 t : o.getPositions())
                    writer.writeTyped(t, Test5.class);
            }
        });

        Test1 t = new Test1();
        List<Test5> p = new ArrayList<>();
        p.add(new Test5(5, 7, 9));
        p.add(new Test5(6, 7, 1));
        p.add(new Test5(3, 9, 1));
        t.setPositions(p);

        try (OdinWriter writer = odin.writer(s)) {
            writer.writeTyped(t, Test1.class);
        }

        assertEquals("{"+newLine+"\t{ x = 5, y = 7, z = 9 }"+newLine+"\t{ x = 6, y = 7, z = 1 }"+newLine+"\t{ x = 3, y = 9, z = 1 }"+newLine+"}", s.toString());
    }

    @Test
    void changeFirstSubObject_Format() throws IOException {
        StringWriter s = new StringWriter();
        Odin odin = new Odin().indentOutput();
        odin.register(new OdinObjectAdapter<Test1>() {
            @Override
            public void read(OdinReader reader, ObjectType<? extends Test1> type, Test1 o) {
                // nothing
            }

            @Override
            public void write(OdinWriter writer, ObjectType<? extends Test1> type, Test1 o) throws IOException {
                writer.compressSubObjects();
                for (Test5 t : o.getPositions()) {
                    writer.writeTyped(t, Test5.class);
                    writer.indentSubObjects();
                }
            }
        });

        Test1 t = new Test1();
        List<Test5> p = new ArrayList<>();
        p.add(new Test5(5, 7, 9));
        p.add(new Test5(6, 7, 1));
        p.add(new Test5(3, 9, 1));
        t.setPositions(p);

        try (OdinWriter writer = odin.writer(s)) {
            writer.writeTyped(t, Test1.class);
        }

        assertEquals("{"+newLine+"\t{ x = 5, y = 7, z = 9 }"+newLine+"\t{"+newLine+"\t\tx = 6"+newLine+"\t\ty = 7"+newLine+"\t\tz = 1"+newLine+"\t}"+newLine+"\t{"+newLine+"\t\tx = 3"+newLine+"\t\ty" +
                " = 9"+ newLine+ "\t\tz = 1"+newLine+"\t}"+newLine+"}", s.toString());
    }

    @Test
    void writeNode_comments() throws IOException {
        StringWriter s = new StringWriter();
        OdinWriter writer = new IndentWriterImpl(new TypeManager(), new StreamWriter(s));

        OdinNode node = new OdinNode();
        node.add("value", 10).getExtra().addComment("hello, test of comment\nwith escape");

        writer.writeNode(node);
        writer.close();

        assertEquals("{"+newLine+"\t# hello, test of comment\\nwith escape"+newLine+"\tvalue = 10"+newLine+"}", s.toString());
    }
}