package com.newpixelcoffee.odin.internal.collections;

import com.newpixelcoffee.odin.Odin;
import com.newpixelcoffee.odin.Test5;
import com.newpixelcoffee.odin.annotations.OdinMapAsList;
import com.newpixelcoffee.odin.exceptions.OdinAdapterException;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author DocVander
 */
class MapValuesAdapterTest {

    @Test
    void read_generic() {
        MapValuesAdapterTestFoo foo = new Odin().fromOdn("{values=<hash map>{{x=1,y=2,z=3}}}", MapValuesAdapterTestFoo.class);
        assertEquals(1, foo.values.size());
        Test5 test = foo.values.get(1);
        assertNotNull(test);
        assertEquals(1, test.getX());
        assertEquals(2, test.getY());
        assertEquals(3, test.getZ());
    }

    @Test
    void write_generic() {
        Map<Integer, Test5> map = new HashMap<>();
        map.put(1, new Test5(1, 2, 3));
        assertEquals("{values=<hash map>{{x=1,y=2,z=3}}}", new Odin().toOdn(new MapValuesAdapterTestFoo(map), MapValuesAdapterTestFoo.class));
    }

    @Test
    void read_generic_wrongField_throwError() {
        assertThrows(OdinAdapterException.class, () -> new Odin().fromOdn("{values=<hash map>{{x=1,y=2,z=3}}}", MapValuesAdapterTestFooWrong.class));
    }

    public static class MapValuesAdapterTestFoo {
        @OdinMapAsList(keyField = "x")
        private Map<Integer, Test5> values;

        public MapValuesAdapterTestFoo(Map<Integer, Test5> values) {
            this.values = values;
        }
    }

    public static class MapValuesAdapterTestFooWrong {
        @OdinMapAsList(keyField = "wrong")
        private Map<Integer, Test5> values;

        public MapValuesAdapterTestFooWrong(Map<Integer, Test5> values) {
            this.values = values;
        }
    }
}