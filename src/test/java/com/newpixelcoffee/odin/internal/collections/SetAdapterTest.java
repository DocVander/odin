package com.newpixelcoffee.odin.internal.collections;

import com.newpixelcoffee.odin.Odin;
import com.newpixelcoffee.odin.Test5;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author DocVander
 */
class SetAdapterTest {

    @Test
    void read() {
        Set<String> set = new Odin().fromOdn("{\"a\",\"b\"}", HashSet.class);
        assertEquals(2, set.size());
        Iterator<String> i = set.iterator();
        assertEquals("a", i.next());
        assertEquals("b", i.next());
    }

    @Test
    void write() {
        Set<String> map = new HashSet<>();
        map.add("a");
        map.add("b");
        assertEquals("{\"a\",\"b\"}", new Odin().toOdn(map, HashSet.class));
    }

    @Test
    void read_generic() {
        SetAdapterTestFoo foo = new Odin().fromOdn("{values=<hash set>{{x=1,y=2,z=3}}}", SetAdapterTestFoo.class);
        assertEquals(1, foo.values.size());
        Test5 test = foo.values.iterator().next();
        assertNotNull(test);
        assertEquals(1, test.getX());
        assertEquals(2, test.getY());
        assertEquals(3, test.getZ());
    }

    @Test
    void write_generic() {
        Set<Test5> set = new HashSet<>();
        set.add(new Test5(1, 2, 3));
        assertEquals("{values=<hash set>{{x=1,y=2,z=3}}}", new Odin().toOdn(new SetAdapterTestFoo(set), SetAdapterTestFoo.class));
    }

    public static class SetAdapterTestFoo {
        private Set<Test5> values;

        public SetAdapterTestFoo(Set<Test5> values) {
            this.values = values;
        }
    }
}