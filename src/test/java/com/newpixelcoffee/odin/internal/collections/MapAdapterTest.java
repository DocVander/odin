package com.newpixelcoffee.odin.internal.collections;

import com.newpixelcoffee.odin.Odin;
import com.newpixelcoffee.odin.Test2;
import com.newpixelcoffee.odin.Test5;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author DocVander
 */
class MapAdapterTest {

    @Test
    void read() {
        Map<String, Integer> map = new Odin().fromOdn("{\"test1\":1,\"test2\":2}", HashMap.class);
        assertEquals(2, map.size());
        assertEquals(1, map.get("test1"));
        assertEquals(2, map.get("test2"));
    }

    @Test
    void write() {
        Map<String, Integer> map = new HashMap<>();
        map.put("a", 1);
        map.put("b", 2);
        assertEquals("{\"a\":1,\"b\":2}", new Odin().toOdn(map, HashMap.class));
    }

    @Test
    void read_generic() {
        MapAdapterTestFoo foo = new Odin().fromOdn("{values=<hash map>{\"SUCCESS\":{x=1,y=2,z=3}}}", MapAdapterTestFoo.class);
        assertEquals(1, foo.values.size());
        Test5 test = foo.values.get(Test2.SUCCESS);
        assertNotNull(test);
        assertEquals(1, test.getX());
        assertEquals(2, test.getY());
        assertEquals(3, test.getZ());
    }

    @Test
    void write_generic() {
        Map<Test2, Test5> map = new HashMap<>();
        map.put(Test2.SUCCESS, new Test5(1, 2, 3));
        assertEquals("{values=<hash map>{\"SUCCESS\":{x=1,y=2,z=3}}}", new Odin().toOdn(new MapAdapterTestFoo(map), MapAdapterTestFoo.class));
    }

    public static class MapAdapterTestFoo {
        private Map<Test2, Test5> values;

        public MapAdapterTestFoo(Map<Test2, Test5> values) {
            this.values = values;
        }
    }
}