package com.newpixelcoffee.odin.internal.collections;

import com.newpixelcoffee.odin.Odin;
import com.newpixelcoffee.odin.Test5;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author DocVander
 */
class ListAdapterTest {

    @Test
    void read() {
        List<String> list = new Odin().fromOdn("{\"test1\",\"test2\"}", ArrayList.class);
        assertEquals(2, list.size());
        assertEquals("test1", list.get(0));
        assertEquals("test2", list.get(1));
    }

    @Test
    void write() {
        List<String> list = new ArrayList<>();
        list.add("test1");
        list.add("test2");
        assertEquals("{\"test1\",\"test2\"}", new Odin().toOdn(list, ArrayList.class));
    }

    @Test
    void read_generic() {
        ListAdapterTestFoo foo = new Odin().fromOdn("{values=<array list>{{x=1,y=2,z=3}}}", ListAdapterTestFoo.class);
        assertEquals(1, foo.values.size());
        assertEquals(1, foo.values.get(0).getX());
        assertEquals(2, foo.values.get(0).getY());
        assertEquals(3, foo.values.get(0).getZ());
    }

    @Test
    void write_generic() {
        List<Test5> list = new ArrayList<>();
        list.add(new Test5(1, 2, 3));
        assertEquals("{values=<array list>{{x=1,y=2,z=3}}}", new Odin().toOdn(new ListAdapterTestFoo(list), ListAdapterTestFoo.class));
    }

    @Test
    void read_immutable() {
        List<String> list = new Odin().fromOdn("<java.util.Arrays$ArrayList>{a=<[string>[\"a1\",\"a2\"]}");
        assertEquals(2, list.size());
        assertEquals("a1", list.get(0));
        assertEquals("a2", list.get(1));
    }

    @Test
    void write_immutable() {
        List<String> list = Arrays.asList("a1", "a2");
        assertEquals("<java.util.Arrays$ArrayList>{a=<[string>[\"a1\",\"a2\"]}", new Odin().toOdn(list));
    }

    public static class ListAdapterTestFoo {
        private List<Test5> values;

        public ListAdapterTestFoo(List<Test5> values) {
            this.values = values;
        }
    }
}