package com.newpixelcoffee.odin.internal;

import com.newpixelcoffee.odin.*;
import com.newpixelcoffee.odin.access.OdinRowReader;
import com.newpixelcoffee.odin.adapters.OdinInlineAdapter;
import com.newpixelcoffee.odin.elements.OdinArray;
import com.newpixelcoffee.odin.elements.OdinNode;
import com.newpixelcoffee.odin.elements.OdinObject;
import com.newpixelcoffee.odin.exceptions.OdinAdapterException;
import com.newpixelcoffee.odin.exceptions.OdinFormatException;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.types.TypeManager;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author DocVander
 */
class ReaderImplTest {

    private static final int TU_BUFFER_SIZE = 64;

    @Test
    void readByte() throws IOException {
        try (OdinReader reader = new Odin().reader(new StringReader("5"))) {
            assertEquals((byte) 5, reader.readByte());
        }
    }

    @Test
    void readByte_unknown() throws IOException {
        try (OdinReader reader = new Odin().reader(new StringReader("5"))) {
            assertEquals((byte) 5, reader.<Integer>read().byteValue());
        }
    }

    @Test
    void readShort() throws IOException {
        try (OdinReader reader = new Odin().reader(new StringReader("5"))) {
            assertEquals((short) 5, reader.readShort());
        }
    }

    @Test
    void readInt() throws IOException {
        try (OdinReader reader = new Odin().reader(new StringReader("5"))) {
            assertEquals(5, reader.readInt());
        }
    }

    @Test
    void readInt_unknown() throws IOException {
        try (OdinReader reader = new Odin().reader(new StringReader("5"))) {
            assertEquals(5, reader.<Integer>read());
        }
    }

    @Test
    void readLong() throws IOException {
        try (OdinReader reader = new Odin().reader(new StringReader("5564654644654"))) {
            assertEquals(5564654644654L, reader.readLong());
        }
    }

    @Test
    void readLong_unknown() throws IOException {
        try (OdinReader reader = new Odin().reader(new StringReader("5564654644654"))) {
            assertEquals(5564654644654L, reader.<Long>read());
        }
    }

    @Test
    void readFloat() throws IOException {
        float f = -5.985E4f;
        try (OdinReader reader = new Odin().reader(new StringReader(Float.toString(f)))) {
            assertEquals(f, reader.readFloat());
        }
    }

    @Test
    void readFloat_unknown() throws IOException {
        float f = -5.985E4f;
        try (OdinReader reader = new Odin().reader(new StringReader(Float.toString(f)))) {
            assertEquals(f, reader.<Float>read());
        }
    }

    @Test
    void readDouble() throws IOException {
        double f = -5.985E4f;
        try (OdinReader reader = new Odin().reader(new StringReader(Double.toString(f)))) {
            assertEquals(f, reader.readDouble());
        }
    }

    @Test
    void readDouble_unknown() throws IOException {
        double f = -5.985E4f;
        try (OdinReader reader = new Odin().reader(new StringReader(Double.toString(f)))) {
            assertEquals(f, reader.<Float>read().doubleValue());
        }
    }

    @Test
    void readNode_spacedCr() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("{\r\tkey = \"bonjour le monde\"\r\tvalue = 15.5\r\tb = false\r}"), TU_BUFFER_SIZE));

        OdinNode node = reader.readNode();
        reader.close();

        assertEquals(3, node.size());
        assertFalse(node.get("key").hasExtra());
        assertEquals("bonjour le monde", node.getValue("key"));
        assertEquals(15.5f, (float) node.getValue("value"));
        assertEquals(false, node.getValue("b"));
    }

    @Test
    void readNode_spacedCrLf() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("{\r\n\tkey = \"bonjour le monde\"\r\n\tvalue = 15.5\r\n\tb = false\r\n}"), TU_BUFFER_SIZE));

        OdinNode node = reader.readNode();
        reader.close();

        assertEquals(3, node.size());
        assertFalse(node.get("key").hasExtra());
        assertEquals("bonjour le monde", node.getValue("key"));
        assertEquals(15.5f, (float) node.getValue("value"));
        assertEquals(false, node.getValue("b"));
    }

    @Test
    void readNode_spacedLf() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("{\n\tkey = \"bonjour le monde\"\n\tvalue = 15.5\n\tb = false\n}"), TU_BUFFER_SIZE));

        OdinNode node = reader.readNode();
        reader.close();

        assertEquals(3, node.size());
        assertFalse(node.get("key").hasExtra());
        assertEquals("bonjour le monde", node.getValue("key"));
        assertEquals(15.5f, (float) node.getValue("value"));
        assertEquals(false, node.getValue("b"));
    }

    @Test
    void readNode_short() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("{key=\"bonjour le monde\",value=15.5,b=false}"), TU_BUFFER_SIZE));

        OdinNode node = reader.readNode();
        reader.close();

        assertEquals(3, node.size());
        assertEquals("bonjour le monde", node.getValue("key"));
        assertEquals(15.5f, (float) node.getValue("value"));
        assertEquals(false, node.getValue("b"));
    }

    @Test
    void readArray_spaced() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("[\n\t{\n\t\tc = 't'\n\t}\n\t{\n\t\ta = 565654\n\t}\n\t{\n\t\tb = \"bonjour\"\n\t\tc = " +
                "NaN\n\t}]"), TU_BUFFER_SIZE));

        OdinArray array = reader.readArray();
        reader.close();

        assertEquals(3, array.size());
        assertEquals('t', (char) array.get(0).asNode().getValue("c"));
        assertEquals(565654, (int) array.get(1).asNode().getValue("a"));
        assertEquals("bonjour", array.get(2).asNode().getValue("b"));
        assertEquals(Float.NaN, (float) array.get(2).asNode().getValue("c"));
    }

    @Test
    void readArray_spacedCrLf() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("[\r\n\t{\r\n\t\tc = 't'\r\n\t}\r\n\t{\r\n\t\ta = 565654\r\n\t}\r\n\t{\r\n\t\tb = " +
                "\"bonjour\"\r\n\t\tc = NaN\r\n\t}]"), TU_BUFFER_SIZE));

        OdinArray array = reader.readArray();
        reader.close();

        assertEquals(3, array.size());
        assertEquals('t', (char) array.get(0).asNode().getValue("c"));
        assertEquals(565654, (int) array.get(1).asNode().getValue("a"));
        assertEquals("bonjour", array.get(2).asNode().getValue("b"));
        assertEquals(Float.NaN, (float) array.get(2).asNode().getValue("c"));
    }

    @Test
    void readArray_short() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("[{c='t'},{a=565654},{b=\"bonjour\",c=NaN}]"), TU_BUFFER_SIZE));

        OdinArray array = reader.readArray();
        reader.close();

        assertEquals(3, array.size());
        assertEquals('t', (char) array.get(0).asNode().getValue("c"));
        assertEquals(565654, (int) array.get(1).asNode().getValue("a"));
        assertEquals("bonjour", array.get(2).asNode().getValue("b"));
        assertEquals(Float.NaN, (float) array.get(2).asNode().getValue("c"));
    }

    @Test
    void readInline_date() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("<local date> \"2019-02-17\""), TU_BUFFER_SIZE));

        Object o = reader.read();
        reader.close();

        assertTrue(o instanceof LocalDate);

        LocalDate d = (LocalDate) o;
        assertEquals(2019, d.getYear());
        assertEquals(2, d.getMonthValue());
        assertEquals(17, d.getDayOfMonth());
    }

    @Test
    void readDateArray() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("<[local date> [\"1996-10-21\",\"2019-02-17\"]"), TU_BUFFER_SIZE));

        Object o = reader.read();
        reader.close();

        assertTrue(o instanceof LocalDate[]);

        LocalDate[] d = (LocalDate[]) o;
        assertEquals(1996, d[0].getYear());
        assertEquals(10, d[0].getMonthValue());
        assertEquals(21, d[0].getDayOfMonth());

        assertEquals(2019, d[1].getYear());
        assertEquals(2, d[1].getMonthValue());
        assertEquals(17, d[1].getDayOfMonth());
    }

    @Test
    void readNode_class() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("{value = <class> \""+ OdinObject.class.getName()+"\"}"), TU_BUFFER_SIZE));

        OdinNode node = reader.readNode();
        reader.close();

        assertEquals(1, node.size());
        assertEquals(OdinObject.class, node.getValue("value"));
    }

    @Test
    void readNode_comments_cr() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("# hello\r\r\r# comments with space\r\r{value = 10}"), TU_BUFFER_SIZE));

        OdinNode node = reader.readNode();
        reader.close();

        assertLinesMatch(Arrays.asList("hello", "", "", "comments with space", ""), node.getExtra().getComments());

        assertEquals(1, node.size());
        assertEquals(10, (int) node.getValue("value"));
    }

    @Test
    void readNode_comments_crLf() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("# hello\r\n\r\n\r\n# comments with space\r\n\r\n{value = 10}"), TU_BUFFER_SIZE));

        OdinNode node = reader.readNode();
        reader.close();

        assertLinesMatch(Arrays.asList("hello", "", "", "comments with space", ""), node.getExtra().getComments());

        assertEquals(1, node.size());
        assertEquals(10, (int) node.getValue("value"));
    }

    @Test
    void readNode_comments_lf() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("# hello\n\n\n# comments with space\n\n{value = 10}"), TU_BUFFER_SIZE));

        OdinNode node = reader.readNode();
        reader.close();

        assertLinesMatch(Arrays.asList("hello", "", "", "comments with space", ""), node.getExtra().getComments());

        assertEquals(1, node.size());
        assertEquals(10, (int) node.getValue("value"));
    }

    @Test
    void readNode_commentsSpaced() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("10\n\n\n# comments with space\n\n{value = 10}"), TU_BUFFER_SIZE));

        assertEquals(10, reader.readInt());
        OdinNode node = reader.readNode();
        reader.close();

        assertLinesMatch(Arrays.asList("", "", "comments with space", ""), node.getExtra().getComments());

        assertEquals(1, node.size());
        assertEquals(10, (int) node.getValue("value"));
    }

    @Test
    void readNode_commentsStartSpaced() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("\n\n# comments with space\n\n{value = 10}"), TU_BUFFER_SIZE));

        OdinNode node = reader.readNode();
        reader.close();

        assertLinesMatch(Arrays.asList("", "", "comments with space", ""), node.getExtra().getComments());

        assertEquals(1, node.size());
        assertEquals(10, (int) node.getValue("value"));
    }

    @Test
    void readNode_contentComments() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("# hello\n\n\n# comments with space\n\n{value = 10\n\t# test\n\t\n\t# test 2\n}"), TU_BUFFER_SIZE));

        OdinNode node = reader.readNode();
        reader.close();

        assertLinesMatch(Arrays.asList("hello", "", "", "comments with space", ""), node.getExtra().getComments());

        assertEquals(1, node.size());
        assertEquals(10, (int) node.getValue("value"));

        assertLinesMatch(Arrays.asList("test", "", "test 2"), node.getContentExtra().getComments());
    }

    @Test
    void readNode_complexComments() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader(
                "{value = 10\n\t# test, this is a comment. with separator\\nand escape\n\t\n\tversion = 5\n}"), TU_BUFFER_SIZE));

        OdinNode node = reader.readNode();
        reader.close();

        assertEquals(2, node.size());
        assertEquals(10, (int) node.getValue("value"));
        assertEquals(5, (int) node.getValue("version"));
        assertLinesMatch(Arrays.asList("test, this is a comment. with separator\nand escape", ""), node.get("version").getExtra().getComments());
    }

    @Test
    void readObject_list() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("{types=<array list>{\"SUCCESS\",\"SUCCESS\"}}"), TU_BUFFER_SIZE));

        Test1 t = reader.readTyped(Test1.class);
        reader.close();

        assertEquals(2, t.getTypes().size());
        assertEquals(Test2.SUCCESS, t.getTypes().get(0));
        assertEquals(Test2.SUCCESS, t.getTypes().get(1));
    }

    @Test
    void readObject_extends() throws IOException {
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("{t={id=\"ERROR\",list=<array list>{\""+id1+"\",\""+id2+"\"}}}"), TU_BUFFER_SIZE));

        Test1 t = reader.readTyped(Test1.class);
        reader.close();

        assertEquals(Test2.ERROR, t.getT().id);
        assertEquals(2, t.getT().getList().size());
        assertEquals(id1, t.getT().getList().get(0));
        assertEquals(id2, t.getT().getList().get(1));
    }

    @Test
    void readObject_extendsGeneric() throws IOException {
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("{g=<"+ Test3.class.getName()+">{id=<"+Test2.class.getName()+">\"ERROR\"," +
                "list=<array list>{\""+id1+"\",\""+id2+"\"}}}"), TU_BUFFER_SIZE));

        Test1 t = reader.readTyped(Test1.class);
        reader.close();

        assertTrue(t.getG() instanceof Test3);
        Test3 g = (Test3) t.getG();

        assertEquals(Test2.ERROR, g.id);
        assertEquals(2, g.getList().size());
        assertEquals(id1, g.getList().get(0));
        assertEquals(id2, g.getList().get(1));
    }

    @Test
    void readObject_listWithoutType1() throws IOException {
        UUID id1 = UUID.randomUUID();
        UUID id2 = UUID.randomUUID();
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("{l=<array list>{<uuid>\""+id1+"\",<uuid>\""+id2+"\"}}"), TU_BUFFER_SIZE));

        Test1 t = reader.readTyped(Test1.class);
        reader.close();

        assertEquals(2, t.getL().size());
        assertEquals(id1, t.getL().get(0));
        assertEquals(id2, t.getL().get(1));
    }

    @Test
    void readObject_listWithoutType2() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("{l=<array list>{5.1,20.5,35.4,NaN}}"), TU_BUFFER_SIZE));

        Test1 t = reader.readTyped(Test1.class);
        reader.close();

        assertEquals(4, t.getL().size());
        assertEquals(5.1f, t.getL().get(0));
        assertEquals(20.5f, t.getL().get(1));
        assertEquals(35.4f, t.getL().get(2));
        assertEquals(Float.NaN, t.getL().get(3));
    }

    @Test
    void readObject_skip() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("{unknown=true,unknown 2=<object>{a=false},l=<array list>{5.1,20.5,35.4,NaN}}"),
                TU_BUFFER_SIZE));

        Test1 t = reader.readTyped(Test1.class);
        reader.close();

        assertEquals(4, t.getL().size());
        assertEquals(5.1f, t.getL().get(0));
        assertEquals(20.5f, t.getL().get(1));
        assertEquals(35.4f, t.getL().get(2));
        assertEquals(Float.NaN, t.getL().get(3));
    }

    @Test
    void readTo() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("{t={id=\"SUCCESS\"}}"),
                TU_BUFFER_SIZE));

        Test1 to = new Test1();
        Test1 t = reader.readTo(to);
        reader.close();

        assertSame(to, t);
        assertEquals(Test2.SUCCESS, t.getT().id);
    }

    @Test
    void readEntries() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("t=10,a=true,b=5"),
                TU_BUFFER_SIZE));

        Object[] o = new Object[3];
        reader.readField("a").listenBoolean((Boolean i) -> o[1] = i);
        reader.readField("b").listenInt((Integer i) -> o[2] = i);
        reader.readField("t").listenInt((Integer i) -> o[0] = i);
        reader.close();

        assertEquals(10, o[0]);
        assertEquals(true, o[1]);
        assertEquals(5, o[2]);
    }

    @Test
    void readRow() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("true:50:5"),
                TU_BUFFER_SIZE));

        Object[] o = new Object[3];
        OdinRowReader row = reader.readRow();
        o[0] = row.takeBoolean();
        o[1] = row.takeInt();
        o[2] = row.takeInt();
        reader.close();

        assertEquals(true, o[0]);
        assertEquals(50, o[1]);
        assertEquals(5, o[2]);
    }

    @Test
    void readRow_overflow() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("true:50:5,false:10"),
                TU_BUFFER_SIZE));

        OdinRowReader row = reader.readRow();
        assertTrue(row.takeBoolean());
        assertEquals(50, row.takeInt());
        assertEquals(5, row.takeInt());
        assertThrows(OdinFormatException.class, row::takeBoolean);
        reader.close();
    }

    @Test
    void readRow_unknown() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("50 : 5"),
                TU_BUFFER_SIZE));

        Object[] o = new Object[2];
        OdinRowReader row = reader.readRow();
        o[0] = row.take();
        o[1] = row.take();
        reader.close();

        assertEquals(50, o[0]);
        assertEquals(5, o[1]);
        assertEquals(Integer.class, o[0].getClass());
    }

    @Test
    void readHasNext_empty() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader(""),
                TU_BUFFER_SIZE));

        assertFalse(reader.hasNext());
        reader.close();
    }

    @Test
    void readHasNext_entry() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("10,5"),
                TU_BUFFER_SIZE));

        assertEquals(10, reader.readInt());
        assertTrue(reader.hasNext());
        assertEquals(5, reader.readInt());
        reader.close();
    }

    @Test
    void readHasNext_overflow() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("10},15"), TU_BUFFER_SIZE));

        assertEquals(10, reader.readInt());
        assertThrows(OdinFormatException.class, reader::readInt);
        reader.close();
    }

    @Test
    void readNode_noSeparator() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("{\n\tkey {\n\t\tversion = 5\n\t}\n\tvalue = 15.5}"), TU_BUFFER_SIZE));

        OdinNode node = reader.readNode();
        reader.close();

        assertEquals(2, node.size());
        assertEquals(5, (int) node.get("key").asNode().getValue("version"));
        assertEquals(15.5f, (float) node.getValue("value"));
    }

    @Test
    void readEntries_skipValue() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("10,5,key=true"), TU_BUFFER_SIZE));

        final boolean[] i = {false};
        reader.readField("key").listenBoolean(b -> i[0] = b);
        reader.close();

        assertTrue(i[0]);
    }

    @Test
    void readEntries_noEof() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("key=true"), TU_BUFFER_SIZE));

        final boolean[] i = {false, false};
        reader.readField("key").listenBoolean(b -> i[0] = b);
        reader.readField("key 2").listenBoolean(b -> i[1] = b);
        reader.close();

        assertTrue(i[0]);
    }

    @Test
    void readEntries_adapterError() throws IOException {
        Odin odin = new Odin();
        odin.register("test", new OdinInlineAdapter<Test5, String>() {
            @Override
            public Test5 read(ObjectType<? extends Test5> type, String o) {
                throw new IllegalArgumentException("Unable to find " + o);
            }

            @Override
            public String write(ObjectType<? extends Test5> type, Test5 o) {
                return o.getX() + " " + o.getY() + " " + o.getZ();
            }
        });
        Test4<Test7> test = new Test4<>();
        try (OdinReader reader = odin.reader(new StringReader("{list=<array list>{{name=\"t1\",value=\"10 5 1\"},{name=\"t2\",value=\"11 5 1\"},{name=\"t3\",value=\"12 5 1\"}}}"))) {
            assertThrows(OdinAdapterException.class, () -> reader.readTo(test, new TypeBuilder<Test4<Test7>>() {}.type));
        }
    }

    @Test
    void readEntries_adapterError2() throws IOException {
        Odin odin = new Odin();
        Test7 test = new Test7("l", null);
        try (OdinReader reader = odin.reader(new StringReader("{name=\"t1\",value={x=10,y=5,z=}}"))) {
            assertThrows(OdinAdapterException.class, () -> reader.readTo(test));
        }
    }

    @Test
    void readInts() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36," +
                "37,38,39,40,41,42"), TU_BUFFER_SIZE));

        int i = 1;
        int j = 0;
        while (reader.hasNext()) {
            j = reader.readInt();
            assertEquals(i++, j);
        }

        assertEquals(j, 42);
        reader.close();
    }

    @Test
    void readKey() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("# test\nkey=true"), 128));

        assertEquals("key", reader.readKey());
        assertTrue(reader.readBoolean());
    }

    @Test
    void readNextKey() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("# test\nkey=true"), 128));

        assertEquals("key", reader.readNextKey());
        assertTrue(reader.readBoolean());
    }

    @Test
    void readNextKey_skipValues() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("10,8,key=true"), 128));

        assertEquals("key", reader.readNextKey());
        assertTrue(reader.readBoolean());
    }

    @Test
    void readNextKey_skipValues2() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("10\n8\r\nkey=true"), 128));

        assertEquals("key", reader.readNextKey());
        assertTrue(reader.readBoolean());
    }

    @Test
    void readNextKey_skipRow() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("# test\ntrue:false,key=true"), 128));

        assertEquals("key", reader.readNextKey());
        assertTrue(reader.readBoolean());
    }

    @Test
    void readNextKey_noMore() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("# test\ntrue:false"), 128));
        assertNull(reader.readNextKey());
    }

    @Test
    void readSub_object() throws IOException {
        OdinReader reader = new ReaderImpl(new TypeManager(), new StreamReader(new StringReader("content={age=15,value=65464}"), 128));
        boolean[] test = { false };
        reader.readField("content").listenSubContent((OdinReader sub) -> {
            assertEquals("age", sub.readKey());
            assertEquals(15, sub.readInt());
            assertEquals("value", sub.readKey());
            assertEquals(65464, sub.readInt());
            test[0] = true;
        });
        reader.close();
        assertTrue(test[0]);
    }
}