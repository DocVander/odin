package com.newpixelcoffee.odin;

import java.util.List;
import java.util.UUID;

/**
 * @author DocVander
 */
public class Test1 {
    private List<Test2> types;
    private Test3<Test2> t;
    private Test4<UUID> g;
    private List l;

    private List<Test5> positions;

    public List<Test2> getTypes() {
        return types;
    }

    public void setTypes(List<Test2> types) {
        this.types = types;
    }

    public Test3<Test2> getT() {
        return t;
    }

    public void setT(Test3<Test2> t) {
        this.t = t;
    }

    public Test4<UUID> getG() {
        return g;
    }

    public void setG(Test4<UUID> g) {
        this.g = g;
    }

    public List getL() {
        return l;
    }

    public void setL(List l) {
        this.l = l;
    }

    public List<Test5> getPositions() {
        return positions;
    }

    public void setPositions(List<Test5> positions) {
        this.positions = positions;
    }
}
