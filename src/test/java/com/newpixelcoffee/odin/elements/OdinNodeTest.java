package com.newpixelcoffee.odin.elements;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class OdinNodeTest {

    @Test
    void node_shouldBeOk() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.addNode("sub1");

        // Then
        assertEquals(1, node.size);
        assertEquals("sub1", node.getName(0));
    }

    @Test
    void node_shouldBeOk_whenNodeExist() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.addNode("sub1");
        node.addNode("sub1");

        // Then
        assertEquals(1, node.size);
        assertEquals("sub1", node.getName(0));
    }

    @Test
    void array_shouldBeOk() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.addArray("sub1");

        // Then
        assertEquals(1, node.size);
        assertEquals("sub1", node.getName(0));
    }

    @Test
    void array_shouldBeOk_whenArrayExist() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.addArray("sub1");
        node.addArray("sub1");

        // Then
        assertEquals(1, node.size);
        assertEquals("sub1", node.getName(0));
    }

    @Test
    void object_shouldBeOk() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.add("sub1", 1);

        // Then
        assertEquals(1, node.size);
        assertEquals("sub1", node.getName(0));
        assertEquals(1, (int) node.getValue(0));
    }

    @Test
    void object_shouldBeOk_whenObjectExist() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.add("sub1", 1);
        node.add("sub1", 2);

        // Then
        assertEquals(1, node.size);
        assertEquals("sub1", node.getName(0));
        assertEquals(2, (int) node.getValue(0));
    }

    @Test
    void addNode_shouldBeOk() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.addNode("sub1");

        // Then
        assertEquals(1, node.size);
        assertEquals("sub1", node.getName(0));
    }

    @Test
    void addNode_shouldBeOk_whenNodeExist() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.addNode("sub1");
        node.addNode("sub1");

        // Then
        assertEquals(1, node.size);
        assertEquals("sub1", node.getName(0));
    }

    @Test
    void setNode_shouldBeOk() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.addArray("sub1");

        // Then
        assertEquals(1, node.size);
        assertEquals("sub1", node.getName(0));
    }

    @Test
    void addArray_shouldBeOk() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.addNode("sub1");

        // Then
        assertEquals(1, node.size);
        assertEquals("sub1", node.getName(0));
    }

    @Test
    void addArray_shouldBeOk_whenArrayExist() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.addArray("sub1");
        node.addArray("sub1");

        // Then
        assertEquals(1, node.size);
        assertEquals("sub1", node.getName(0));
    }

    @Test
    void setArray_shouldBeOk() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.addArray("sub1");

        // Then
        assertEquals(1, node.size);
        assertEquals("sub1", node.getName(0));
    }

    @Test
    void setIfAbsent() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.add("value", 10);
        node.addIfAbsent("t", true);
        node.addIfAbsent("value", 0);

        // Then
        assertEquals(2, node.size);
        assertEquals(10, (int) node.getValue("value"));
        assertTrue((boolean) node.getValue("t"));
    }

    @Test
    void contains_shouldBeOk() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.add("value", 10);

        // Then
        assertEquals(1, node.size);
        assertTrue(node.contains("value"));
    }

    @Test
    void remove() {
        // Given
        OdinNode node = new OdinNode();

        // When
        node.add("value", 10);
        node.add("t", true);
        node.add("a", 5);
        node.add("c", 'c');

        // Then
        assertEquals(4, node.size);
        assertEquals(true, node.remove("t").asObject().value);
        assertEquals(3, node.size);
        assertFalse(node.contains("t"));
    }

    @Test
    void findNode_SubArray() {
        OdinNode node = new OdinNode().run(n -> {
            n.addArray("test").run(a -> {
                a.add(10);
                a.addNode().run(n2 -> n2.add("foo", true));
            });
        });

        assertEquals(10, node.<Integer>findValue("test[0]"));
        assertEquals(true, node.findValue("test[1].foo"));
    }
}