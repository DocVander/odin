package com.newpixelcoffee.odin;

import java.util.List;

/**
 * @author DocVander
 */
public class Test4<T> {
    private List<T> list;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
