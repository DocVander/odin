package com.newpixelcoffee.odin;

/**
 * @author DocVander
 */
public enum Test2 {
    SUCCESS {
        @Override
        public String id() {
            return "ok";
        }
    },
    ERROR {
        @Override
        public String id() {
            return "ko";
        }
    };

    public abstract String id();
}
