package com.newpixelcoffee.odin;

/**
 * @author DocVander
 */
public class Test7 {
    private String name;
    private Test5 value;

    public Test7(String name, Test5 value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Test5 getValue() {
        return value;
    }
}
