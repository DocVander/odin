package com.newpixelcoffee.odin.adapters;

import com.newpixelcoffee.odin.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * @author DocVander
 */
class OdinObjectAdapterTest {

    private static final OdinObjectAdapter<Test1> test1 = new OdinObjectAdapter<Test1>() {
        @Override
        public void read(OdinReader reader, ObjectType<? extends Test1> type, Test1 o) throws IOException {
            Test3<Test2> t = new Test3<>();
            t.id = reader.readTyped(type.getField("t").getType().asObject().<Test2>getField("id").getType());
            o.setT(t);
        }

        @Override
        public void write(OdinWriter writer, ObjectType<? extends Test1> type, Test1 o) throws IOException {
            writer.writeTyped(o.getT().id, type.getField("t").getType().asObject().<Test2>getField("id").getType());
        }
    };

    @Test
    void writeSubFieldValue() throws IOException {
        StringWriter s = new StringWriter();

        Test1 t = new Test1();
        t.setT(new Test3<>());
        t.getT().id = Test2.SUCCESS;

        Odin odin = new Odin().compressOutput();
        odin.register("test", test1);

        OdinWriter writer = odin.writer(s);
        writer.write(t);
        writer.close();

        assertEquals("<test>{\"SUCCESS\"}", s.toString());
    }

    @Test
    void readSubFieldValue() throws IOException {
        StringReader s = new StringReader("<test>{\"SUCCESS\"}");

        Odin odin = new Odin().compressOutput();
        odin.register("test", test1);

        OdinReader reader = odin.reader(s);
        Test1 t = reader.read();
        reader.close();

        assertNotNull(t.getT());
        assertEquals(Test2.SUCCESS, t.getT().id);
    }
}