package com.newpixelcoffee.odin.elements;

import java.util.function.Consumer;
import java.util.function.Function;

public class OdinObject<T> extends OdinElement {

    public T value;

    public OdinObject(T value) {
        this.value = value;
    }

    /**
     * Run a consumer with this object as parameter.
     * @param consumer consumer to use
     * @return this object
     */
    public OdinObject<T> run(Consumer<OdinObject<T>> consumer) {
        consumer.accept(this);
        return this;
    }

    /**
     * Run a function with this object as parameter.
     * @param function consumer to use
     * @param <V> the type of the returned value
     * @return the value returned by the function
     */
    public <V> V compute(Function<OdinObject<T>, V> function) {
        return function.apply(this);
    }

    @Override
    public boolean isObject() {
        return true;
    }

    @Override
    public String toString() {
        if (value == null)
            return "null";
        else
            return value.toString();
    }

    @Override
    OdinElement getChildren(String name) {
        return null;
    }
}
