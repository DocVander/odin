package com.newpixelcoffee.odin.elements;

import java.util.function.Consumer;

public abstract class AbstractContainer extends OdinElement {

    private OdinExtra contentExtra;

    protected int size;

    /**
     * Get if content extra is defined for this element.<br>
     * Content extra contains comments and empty lines that appear after the last child of this element in odn.<br>
     * If this element has no children, this content extra contains all comments and empty lines.
     * @return true if extra is not null.
     */
    public boolean hasContentExtra() {
        return contentExtra != null;
    }

    /**
     * Get content extra of this element.<br>
     * Content extra contains comments and empty lines that appear after the last child of this element in odn.<br>
     * If this element has no children, this content extra contains all comments and empty lines.
     * If null, new Extra will be created.
     * @return extra
     */
    public OdinExtra getContentExtra() {
        if (contentExtra == null)
            contentExtra = new OdinExtra();
        return contentExtra;
    }

    /**
     * Set the content extra of this element.<br>
     * Content extra contains comments and empty lines that appear after the last child of this element in odn.<br>
     * If this element has no children, this content extra contains all comments and empty lines.
     * @param contentExtra extra to use
     */
    public void setContentExtra(OdinExtra contentExtra) {
        this.contentExtra = contentExtra;
    }

    /**
     * Build the element content extra by using a consumer.<br>
     * If this element already contains content extra, the consumer is called with it, else new extra is created.<br>
     * Content extra contains comments and empty lines that appear after the last child of this element in odn.<br>
     * If this element has no children, this content extra contains all comments and empty lines.
     * @see #getContentExtra()
     * @param consumer extra consumer
     */
    public void buildContentExtra(Consumer<OdinExtra> consumer) {
        consumer.accept(getContentExtra());
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public abstract void clear();

    /**
     * Get the {@link OdinElement} present at the given index.
     * @param index element position
     * @return element, or null if index is out of bounds
     */
    public abstract OdinElement get(int index);

    /**
     * Get the object value present at the given index.
     * @param index element position
     * @param <T> the type of the returned object
     * @return object value, or null if index is out of bounds or element not an object
     */
    public abstract <T> T getValue(int index);

    /**
     * Remove the {@link OdinElement} present at the given index.
     * @param index element position
     * @return removed element, or null if index is out of bounds
     */
    public abstract OdinElement removeFrom(int index);

    /**
     * Find element in elements hierarchy. Each depth is separated with '.' or with '[]'.<br>
     * Examples : "foo.data.0.id" or "foo[data][0].id"
     * @param name element path
     * @return element or null if element or a parent does not exist
     */
    public OdinElement find(String name) {
        NodeKey key = new NodeKey(name);
        OdinElement e = this;
        while (e != null && key.hasNext())
            e = e.getChildren(key.next());
        return e;
    }

    /**
     * Find object value in elements hierarchy. Each depth is separated with '.' or with '[]'.<br>
     * Examples : "foo.data.0.id" or "foo[data][0].id"
     * @param name object name
     * @param <T> returned object type
     * @return object value or null if element or a parent does not exist
     */
    public <T> T findValue(String name) {
        NodeKey key = new NodeKey(name);
        OdinElement e = this;
        while (e != null && key.hasNext())
            e = e.getChildren(key.next());
        if (e != null && e.isObject())
            return e.<T>asObject().value;
        return null;
    }
}
