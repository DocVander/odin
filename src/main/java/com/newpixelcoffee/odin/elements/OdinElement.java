package com.newpixelcoffee.odin.elements;

import java.util.function.Consumer;

public abstract class OdinElement {

    private OdinExtra extra;

    /**
     * Get if extra is defined for this element.<br>
     * Extra contains comments and empty lines that appear before this element if the odn output.
     * @return true if extra is not null.
     */
    public boolean hasExtra() {
        return extra != null;
    }

    /**
     * Get extra defined for this element.<br>
     * Extra contains comments and empty lines that appear before this element if the odn output.
     * If no extra is present, a new extra will be created.
     * @return extra
     */
    public OdinExtra getExtra() {
        if (extra == null)
            extra = new OdinExtra();
        return extra;
    }

    /**
     * Set the extra of this element.<br>
     * Extra contains comments and empty lines that appear before this element if the odn output.
     * @param extra extra to set
     */
    public void setExtra(OdinExtra extra) {
        this.extra = extra;
    }

    public boolean isNode() {
        return false;
    }

    public boolean isArray() {
        return false;
    }

    public boolean isObject() {
        return false;
    }

    public OdinNode asNode() {
        return (OdinNode) this;
    }

    public OdinArray asArray() {
        return (OdinArray) this;
    }

    public <T> OdinObject<T> asObject() {
        return (OdinObject<T>) this;
    }

    /**
     * Build the element extra by using a consumer.<br>
     * If this element already contains extra, the consumer is called with it, else new extra data is created.
     * @see #getExtra()
     * @param consumer extra consumer
     */
    public void buildExtra(Consumer<OdinExtra> consumer) {
        consumer.accept(getExtra());
    }

    abstract OdinElement getChildren(String name);
}
