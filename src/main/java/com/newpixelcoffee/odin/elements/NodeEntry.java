package com.newpixelcoffee.odin.elements;

public class NodeEntry {

    String name;
    OdinElement element;

    NodeEntry(String name, OdinElement element) {
        this.name = name;
        this.element = element;
    }

    public String getName() {
        return name;
    }

    public OdinElement getElement() {
        return element;
    }
}
