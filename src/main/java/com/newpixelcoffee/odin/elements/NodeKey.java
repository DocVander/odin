package com.newpixelcoffee.odin.elements;

import java.util.Iterator;

class NodeKey implements Iterator<String> {

    private String key;
    private int next = 0;

    NodeKey(String name) {
        this.key = name;
    }

    @Override
    public boolean hasNext() {
        return next < key.length();
    }

    @Override
    public String next() {
        int l = key.length();
        int n = next;
        for (int i = next; i < l; i++) {
            switch (key.charAt(i)) {
                case '.':
                case '[':
                    next = i+1;
                    return key.substring(n, i);
                case ']':
                    next = i+2;
                    return key.substring(n, i);
            }
        }
        next = l;
        return key.substring(n);
    }
}
