package com.newpixelcoffee.odin.elements;

import java.util.Collection;
import java.util.Iterator;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

public class OdinArray extends AbstractContainer implements Iterable<OdinElement> {

    private OdinElement[] elements = new OdinElement[8];

    /**
     * Add a new {@link OdinNode} to this array
     * @return the newly created node
     */
    public OdinNode addNode() {
        return insert(new OdinNode());
    }

    /**
     * Add a new {@link OdinArray} to this array
     * @return the newly created node
     */
    public OdinArray addArray() {
        return insert(new OdinArray());
    }

    /**
     * Create and add a {@link OdinObject} with the given value to this array
     * @param value object value
     * @param <T> object value type
     * @return the newly created object
     */
    public <T> OdinObject<T> add(T value) {
        return insert(new OdinObject<>(value));
    }

    /**
     * Add a {@link OdinElement} to this array.
     * @param element element to add
     * @param <T> element type
     * @return the added element
     */
    public <T extends OdinElement> T addElement(T element) {
        return insert(element);
    }

    /**
     * Add a new {@link OdinObject} with the given value to the specified position.
     * @param index object position in this array
     * @param value object value
     * @param <T> object value type
     * @return the newly created object
     */
    public <T> OdinObject<T> addTo(int index, T value) {
        return insert(index, new OdinObject<>(value));
    }

    /**
     * Add a {@link OdinElement} to the specified position.
     * @param index element position in this array
     * @param element element to add
     * @param <T> element type
     * @return the added element
     */
    public <T extends OdinElement> T addElementTo(int index, T element) {
        return insert(index, element);
    }

    public void addAll(Collection<?> objects) {
        for (Object value : objects)
            add(value);
    }

    /**
     * Replace the element present at specified position by a new {@link OdinObject} with the given value.<br>
     * If index is out of bounds, this method call {@link #add(Object)}.
     * @param index object position in this array
     * @param value new object value
     * @param <T> object value type
     * @return the newly created object
     */
    public <T> OdinObject<T> set(int index, T value) {
        if (index >= 0 && index < size) {
            OdinObject<T> element = new OdinObject<>(value);
            elements[index] = element;
            return element;
        } else
            return add(value);
    }

    /**
     * Replace the element present at specified position by the given element.<br>
     * If index is out of bounds, this method call {@link #addElement(OdinElement)}.
     * @param index element position in this array
     * @param element element to add
     * @param <T> element type
     * @return the newly created object
     */
    public <T extends OdinElement> T set(int index, T element) {
        if (index >= 0 && index < size) {
            elements[index] = element;
            return element;
        } else
            return addElement(element);
    }

    public <T> boolean contains(T value) {
        for (int i = 0; i < size; i++) {
            OdinElement e = elements[i];
            if (e.isObject() && Objects.equals(e.asObject().value, value))
                return true;
        }
        return false;
    }

    /**
     * Get the position in this array of the {@link OdinObject} containing the given value.
     * @param value element value
     * @param <T> value type
     * @return element position, or -1 if no element was found with this value
     */
    public <T> int indexOf(T value) {
        for (int i = 0; i < size; i++) {
            OdinElement e = elements[i];
            if (e.isObject() && Objects.equals(e.asObject().value, value))
                return i;
        }
        return -1;
    }

    @Override
    public OdinElement get(int index) {
        OdinElement e = null;
        if (index >= 0 && index < size)
            e = elements[index];
        return e;
    }

    @Override
    public <T> T getValue(int index) {
        if (index >= 0 && index < size) {
            OdinElement e = elements[index];
            if (e.isObject())
                return e.<T>asObject().value;
        }
        return null;
    }

    @Override
    public void clear() {
        elements = new OdinElement[8];
        size = 0;
    }

    /**
     * Remove the {@link OdinObject} containing the given value.
     * @param value element value
     * @param <T> value type
     * @return the removed element, or null if no element was found with this name
     */
    public <T> OdinObject<T> remove(T value) {
        for (int i = 0; i < size; i++) {
            OdinElement e = elements[i];
            if (e.isObject() && Objects.equals(e.asObject().value, value))
                return removeFrom(i).asObject();
        }
        return null;
    }

    public void removeAll(Collection<?> values) {
        for (Object value : values)
            remove(value);
    }

    @Override
    public OdinElement removeFrom(int index) {
        OdinElement e = null;
        if (index >= 0 && index < size) {
            e = elements[index];
            size--;
            if (index < size)
                System.arraycopy(elements, index + 1, elements, index, size - index);
            elements[size] = null;
        }
        return e;
    }

    /**
     * Run a consumer with this array as parameter. This can be used to build children.
     * @param consumer consumer to use
     * @return this array
     */
    public OdinArray run(Consumer<OdinArray> consumer) {
        consumer.accept(this);
        return this;
    }

    /**
     * Run a function with this array as parameter. This can be used to build children or get a value.
     * @param function consumer to use
     * @param <V> the type of the returned value
     * @return the value returned by the function
     */
    public <V> V compute(Function<OdinArray, V> function) {
        return function.apply(this);
    }

    private <T extends OdinElement> T insert(T e) {
        return insert(size, e);
    }

    private <T extends OdinElement> T insert(int index, T e) {
        int i = index;
        if (i >= size)
            i = size;

        if (size == elements.length) {
            int j = size * 2;

            OdinElement[] es = new OdinElement[j];
            System.arraycopy(elements, 0, es, 0, i);
            if (i < size)
                System.arraycopy(elements, i, es, i + 1, size - 1 - i);
            elements = es;
        }
        else if (i < size)
            System.arraycopy(elements, i, elements, i + 1, size - 1 - i);
        elements[i] = e;
        size++;
        return e;
    }

    @Override
    public boolean isArray() {
        return true;
    }

    @Override
    public Iterator<OdinElement> iterator() {
        return new Iterator<OdinElement>() {
            int i = 0;
            @Override
            public boolean hasNext() {
                return i < size;
            }

            @Override
            public OdinElement next() {
                return get(i++);
            }
        };
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (int i = 0; i < size; i++) {
            if (i > 0)
                sb.append(',');
            sb.append(elements[i]);
        }
        sb.append(']');
        return sb.toString();
    }

    @Override
    OdinElement getChildren(String name) {
        return get(Integer.parseInt(name));
    }
}
