package com.newpixelcoffee.odin.elements;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

public class OdinNode extends AbstractContainer implements Iterable<NodeEntry> {

    private NodeEntry[] elements = new NodeEntry[8];

    /**
     * Add a new {@link OdinNode} with the given name to this node
     * @param name node name
     * @return the newly created node
     */
    public OdinNode addNode(String name) {
        remove(name);
        return insert(name, new OdinNode());
    }

    /**
     * Add a new {@link OdinArray} with the given name to this node
     * @param name array name
     * @return the newly created array
     */
    public OdinArray addArray(String name) {
        remove(name);
        return insert(name, new OdinArray());
    }

    /**
     * Create and add a {@link OdinObject} with the given name and value to this node
     * @param name object name
     * @param value object value
     * @param <T> object value type
     * @return the newly created object
     */
    public <T> OdinObject<T> add(String name, T value) {
        remove(name);
        return insert(name, new OdinObject<>(value));
    }

    /**
     * Add a {@link OdinElement} with the given name to this node
     * @param name element name
     * @param element element to add
     * @param <T> element type
     * @return the added element
     */
    public <T extends OdinElement> T addElement(String name, T element) {
        remove(name);
        return insert(name, element);
    }

    /**
     * Create and add a {@link OdinObject} with the given name and value to this node at the specified index
     * @param index object index
     * @param name object name
     * @param value object value
     * @param <T> object value type
     * @return the newly created object
     */
    public <T> OdinObject<T> addTo(int index, String name, T value) {
        remove(name);
        return insert(index, name, new OdinObject<>(value));
    }

    /**
     * Add a {@link OdinElement} with the given name to this node at the specified index
     * @param index object index
     * @param name element name
     * @param element element to add
     * @param <T> element type
     * @return the added element
     */
    public <T extends OdinElement> T addElementTo(int index, String name, T element) {
        remove(name);
        return insert(index, name, element);
    }

    /**
     * If no elements with the given name is found it will be added with the given value.<br>
     * Else if an element is found, this method return it without any modifications.
     * @param name object name
     * @param value object value
     * @param <T> object value type
     * @return the newly created object or the existing element
     */
    public <T> OdinElement addIfAbsent(String name, T value) {
        NodeEntry e = findEntry(name);
        if (e == null)
            return insert(name, new OdinObject<>(value));
        else
            return e.element;
    }

    /**
     * If no elements with the given name is found, the given element will be added.<br>
     * Else if an element is found, this method return it without any modifications.
     * @param name element name
     * @param element element to add
     * @param <T> element type
     * @return the added or existing element
     */
    public <T extends OdinElement> OdinElement addElementIfAbsent(String name, T element) {
        NodeEntry e = findEntry(name);
        if (e == null)
            return insert(name, element);
        else
            return e.element;
    }

    public void addAll(Map<String, ?> objects) {
        for (Map.Entry<String, ?> e : objects.entrySet())
            add(e.getKey(), e.getValue());
    }

    public boolean contains(String name) {
        return findEntry(name) != null;
    }

    /**
     * Get the position in this node of the {@link OdinElement} defined with the given name.
     * @param name element name
     * @return element position, or -1 if no element was found with this name
     */
    public int indexOf(String name) {
        for (int i = 0; i < size; i++) {
            NodeEntry e = elements[i];
            if (name.equals(e.name))
                return i;
        }
        return -1;
    }

    /**
     * Get the {@link OdinElement} defined with this name
     * @param name element name
     * @return the element with this name
     */
    public OdinElement get(String name) {
        NodeEntry e = findEntry(name);
        if (e != null)
            return e.element;
        return null;
    }

    /**
     * Get the value of the {@link OdinObject} defined with this name.
     * @param name element name
     * @param <T> value type
     * @return the value, or null if no element was found with this name or is not a {@link OdinObject}
     */
    public <T> T getValue(String name) {
        NodeEntry e = findEntry(name);
        if (e != null && e.element.isObject())
            return e.element.<T>asObject().value;
        return null;
    }

    /**
     * Get the name of the {@link OdinElement} present at the given index.
     * @param index element position
     * @return element name, or null if index is out of bounds
     */
    public String getName(int index) {
        if (index >= 0 && index < size)
            return elements[index].name;
        return null;
    }

    @Override
    public OdinElement get(int index) {
        if (index >= 0 && index < size)
            return elements[index].element;
        return null;
    }

    @Override
    public <T> T getValue(int index) {
        if (index >= 0 && index < size) {
            NodeEntry e = elements[index];
            if (e.element.isObject())
                return e.element.<T>asObject().value;
        }
        return null;
    }

    @Override
    public void clear() {
        elements = new NodeEntry[8];
        size = 0;
    }

    /**
     * Remove the {@link OdinElement} defined with this name.
     * @param name element name
     * @return the removed element, or null if no element was found with this name
     */
    public OdinElement remove(String name) {
        for (int i = 0; i < size; i++) {
            NodeEntry e = elements[i];
            if (name.equals(e.name))
                return removeFrom(i);
        }
        return null;
    }

    public void removeAll(Collection<String> names) {
        for (String name : names)
            remove(name);
    }

    @Override
    public OdinElement removeFrom(int index) {
        OdinElement e = null;
        if (index >= 0 && index < size) {
            e = elements[index].element;
            size--;
            if (index < size)
                System.arraycopy(elements, index + 1, elements, index, size - index);
            elements[size] = null;
            if (size > 7 && size < elements.length / 2) {
                NodeEntry[] es = new NodeEntry[size];
                System.arraycopy(elements, 0, es, 0, size);
                elements = es;
            }
        }
        return e;
    }

    /**
     * Run a consumer with this node as parameter. This can be used to build children.
     * @param consumer consumer to use
     * @return this node
     */
    public OdinNode run(Consumer<OdinNode> consumer) {
        consumer.accept(this);
        return this;
    }

    /**
     * Run a function with this node as parameter. This can be used to build children or get a value.
     * @param function consumer to use
     * @param <V> the type of the returned value
     * @return the value returned by the function
     */
    public <V> V compute(Function<OdinNode, V> function) {
        return function.apply(this);
    }

    private <T extends OdinElement> T insert(String name, T element) {
        return insert(size, name, element);
    }

    private <T extends OdinElement> T insert(int index, String name, T element) {
        int i = index;
        if (i >= size)
            i = size;
        if (size == elements.length) {
            int j = size * 2;
            NodeEntry[] es = new NodeEntry[j];
            System.arraycopy(elements, 0, es, 0, i);
            if (i < size)
                System.arraycopy(elements, i, es, i + 1, size - 1 - i);
            elements = es;
        }
        else if (i < size)
            System.arraycopy(elements, i, elements, i + 1, size - 1 - i);
        elements[i] = new NodeEntry(name, element);
        size++;
        return element;
    }

    private NodeEntry findEntry(String key) {
        for (int i = 0; i < size; i++) {
            NodeEntry e = elements[i];
            if (key.equals(e.name))
                return e;
        }
        return null;
    }

    @Override
    public boolean isNode() {
        return true;
    }

    @Override
    public Iterator<NodeEntry> iterator() {
        return new Iterator<NodeEntry>() {
            int i = 0;
            @Override
            public boolean hasNext() {
                return i < size;
            }

            @Override
            public NodeEntry next() {
                return elements[i++];
            }
        };
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        for (int i = 0; i < size; i++) {
            if (i > 0)
                sb.append(',');
            NodeEntry e = elements[i];
            sb.append(e.name).append('=');
            sb.append(e.element);
        }
        sb.append('}');
        return sb.toString();
    }

    @Override
    OdinElement getChildren(String name) {
        return get(name);
    }
}
