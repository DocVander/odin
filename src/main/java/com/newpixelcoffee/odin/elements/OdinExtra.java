package com.newpixelcoffee.odin.elements;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class OdinExtra {

    private List<String> extras = new ArrayList<>();

    /**
     * Add an empty line to this extra.
     */
    public void addSpace() {
        extras.add("");
    }

    /**
     * Add multiple empty lines to this extra.
     * @param lines number of empty lines
     */
    public void addSpace(int lines) {
        for (int i = 0; i < lines; i++)
            extras.add("");
    }

    /**
     * Add comment to this extra.<br>
     * Empty lines are represented by empty string.
     * @param comment comment to add
     */
    public void addComment(String comment) {
        extras.add(comment);
    }

    /**
     * Add comments to this extra.<br>
     * Empty lines are represented by empty string.
     * @param comments comments to add
     */
    public void addComments(String[] comments) {
        Collections.addAll(extras, comments);
    }

    /**
     * Add comments to this extra.<br>
     * Empty lines are represented by empty string.
     * @param comments comments to add
     */
    public void addComments(Collection<String> comments) {
        extras.addAll(comments);
    }

    /**
     * Get comments and empty lines of this extra.<br>
     * Empty lines are represented by empty string.
     * @return list of the extra
     */
    public List<String> getComments() {
        return extras;
    }
}
