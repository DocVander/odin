package com.newpixelcoffee.odin.adapters;

import com.newpixelcoffee.odin.ObjectType;

/**
 * You can use this adapter to convert an object to another object, like Date to a String
 * @param <T> the type to convert
 * @param <R> the result type
 */
public interface OdinInlineAdapter<T, R> extends OdinAdapter<T> {
    T read(ObjectType<? extends T> type, R o);
    R write(ObjectType<? extends T> type, T o);
}
