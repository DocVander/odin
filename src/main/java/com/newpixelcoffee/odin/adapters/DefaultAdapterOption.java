package com.newpixelcoffee.odin.adapters;

import com.newpixelcoffee.odin.exceptions.ExceptionStrategy;

/**
 * Manage option of the default object adapter
 * @author DocVander
 */
public interface DefaultAdapterOption {

    /**
     * <p>Get the strategy to use when a field value can not be read and throw an {@link IllegalAccessException}.
     * Can appear when writing the object or when reading a field with readTo parameter.
     * <p>By default, the strategy is to forward the exception.
     * @return the exception strategy
     */
    ExceptionStrategy getFieldGetExceptionStrategy();

    /**
     * <p>Set the strategy to use when a field value can not be read and throw an {@link IllegalAccessException}.
     * Can appear when writing the object or when reading a field with readTo parameter.
     * <p>By default, the strategy is to forward the exception.
     * @param strategy the exception strategy
     */
    void setFieldGetExceptionStrategy(ExceptionStrategy strategy);

    /**
     * <p>Get the strategy to use when a {@link RuntimeException} occur while reading a field value. The cause can be:
     * <pre>
     * -{@link com.newpixelcoffee.odin.exceptions.OdinFormatException}
     *      if the the odn format contains an error or an object is read with the wrong type.
     * -{@link com.newpixelcoffee.odin.exceptions.OdinTypeException}
     *      if an error occur when Odin build the fields type.
     * -{@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     *      if the error has already been forwarded by a field adapter.
     * -Any other {@link RuntimeException} if the error come from a registered adapter.
     * </pre>
     * <p>By default, the strategy is to forward the exception.
     * @return the exception strategy
     */
    ExceptionStrategy getReaderExceptionStrategy();

    /**
     * <p>Set the strategy to use when a {@link RuntimeException} occur while reading a field value.
     * <pre>
     * -{@link com.newpixelcoffee.odin.exceptions.OdinFormatException}
     *      if the the odn format contains an error or an object is read with the wrong type.
     * -{@link com.newpixelcoffee.odin.exceptions.OdinTypeException}
     *      if an error occur when Odin build the fields type.
     * -{@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     *      if the error has already been forwarded by a field adapter.
     * -Any other {@link RuntimeException} if the error come from a registered adapter.
     * </pre>
     * <p>By default, the strategy is to forward the exception.
     * @param strategy the exception strategy
     */
    void setReaderExceptionStrategy(ExceptionStrategy strategy);

    /**
     * <p>Get the strategy to use when a {@link RuntimeException} occur while writing a field value.
     * <pre>
     * -{@link com.newpixelcoffee.odin.exceptions.OdinFormatException}
     *      if an invalid field key is write by this adapter ( key can not contains <b>,:={}[]&lt;&gt;'"</b> )
     * -{@link com.newpixelcoffee.odin.exceptions.OdinTypeException}
     *      if an error occur when Odin build the fields type.
     * -{@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     *      if the error has already been forwarded by a field adapter.
     * -Any other {@link RuntimeException} if the error come from a registered adapter.
     * </pre>
     * <p>By default, the strategy is to forward the exception.
     * @return the exception strategy
     */
    ExceptionStrategy getWriterExceptionStrategy();

    /**
     * <p>Set the strategy to use when a {@link RuntimeException} occur while writing a field value.
     * <pre>
     * -{@link com.newpixelcoffee.odin.exceptions.OdinFormatException}
     *      if an invalid field key is write by this adapter ( key can not contains <b>,:={}[]&lt;&gt;'"</b> )
     * -{@link com.newpixelcoffee.odin.exceptions.OdinTypeException}
     *      if an error occur when Odin build the fields type.
     * -{@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     *      if the error has already been forwarded by a field adapter.
     * -Any other {@link RuntimeException} if the error come from a registered adapter.
     * </pre>
     * <p>By default, the strategy is to forward the exception.
     * @param strategy the exception strategy
     */
    void setWriterExceptionStrategy(ExceptionStrategy strategy);

    /**
     * Get if the default object adapter write entries containing null as value
     * @return true if entries with a null value are skipped
     */
    boolean isSkippingNull();

    /**
     * Set if the default object adapter write entries containing null as value
     * @param skipNull true to skip entries with a null value
     */
    void setSkippingNull(boolean skipNull);
}
