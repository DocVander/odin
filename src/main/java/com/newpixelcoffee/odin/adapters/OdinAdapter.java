package com.newpixelcoffee.odin.adapters;

/**
 * The root interface for Odin Adapter, You have to implement {@link OdinInlineAdapter}, {@link OdinObjectAdapter} or {@link OdinImmutableAdapter} to customize the serialization of an object
 * @param <T> the type of the object
 */
public interface OdinAdapter<T> {}
