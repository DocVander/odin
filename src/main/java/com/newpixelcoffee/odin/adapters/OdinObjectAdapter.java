package com.newpixelcoffee.odin.adapters;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;

import java.io.IOException;

/**
 * You can use this adapter if you need to customize the serialization and the deserialization of an object
 * @param <T> the type of the object
 */
public interface OdinObjectAdapter<T> extends OdinAdapter<T> {
    void read(OdinReader reader, ObjectType<? extends T> type, T o) throws IOException;
    void write(OdinWriter writer, ObjectType<? extends T> type, T o) throws IOException;
}
