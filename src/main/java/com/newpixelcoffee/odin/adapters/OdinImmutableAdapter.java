package com.newpixelcoffee.odin.adapters;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;

import java.io.IOException;

/**
 * <p>This adapter can be used if you need to pass some content to the object constructor when you deserialize it.
 * <p>If your object contains an empty constructor or can use null as default value for the object fields, use {@link OdinObjectAdapter}.
 * @param <T> the type of the object
 */
public interface OdinImmutableAdapter<T> extends OdinAdapter<T> {
    T read(OdinReader reader, ObjectType<? extends T> type) throws IOException;
    void write(OdinWriter writer, ObjectType<? extends T> type, T o) throws IOException;
}
