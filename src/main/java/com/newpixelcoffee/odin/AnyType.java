package com.newpixelcoffee.odin;

import java.lang.reflect.Type;

/**
 * This object is the parent of every odin types, this type can be primitive, array, unknown type, or {@link ObjectType}.
 * @param <T> genetic type
 * @author DocVander
 */
public interface AnyType<T> {

    /**
     * Get the name of this type, name is used to know the type on the deserialization.<br>
     * If a field is defined with Object type, and value is a 'Foo' instance, this name is write before the object in the writer.
     * @return type name
     */
    String getName();

    /**
     * Get class for this type
     * @return class
     */
    Class<T> getType();

    /**
     * This method return same value than {@link #getType()} except for generic types using {@link java.lang.reflect.ParameterizedType} or any other reflect generic type.<br>
     * So for {@literal List<String>}, {@link #getType()} return {@literal Class<List>} and this method return {@link java.lang.reflect.ParameterizedType} '{@literal List<String>}'
     * method.
     * @return generic type
     */
    Type getGenericType();

    /**
     * Get if this type instance is a implementation of {@link ObjectType}, if false, this type can be a primitive or an array. Note that unknown type use {@link ObjectType}.<br>
     * {@link ObjectType} is used for all standard class to get fields or arguments.
     * @return true if implementation of {@link ObjectType}
     */
    boolean isObject();

    ObjectType<T> asObject();
}
