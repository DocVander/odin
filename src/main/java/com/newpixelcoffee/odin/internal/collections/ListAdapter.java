package com.newpixelcoffee.odin.internal.collections;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.adapters.OdinObjectAdapter;

import java.io.IOException;
import java.util.List;

public class ListAdapter implements OdinObjectAdapter<List<Object>> {

    @Override
    public void read(OdinReader reader, ObjectType<? extends List<Object>> type, List<Object> o) throws IOException {
        while (reader.hasNext())
            o.add(reader.readTyped(type.getAdapterArgument(0)));
    }

    @Override
    public void write(OdinWriter writer, ObjectType<? extends List<Object>> type, List<Object> o) throws IOException {
        for (Object e : o)
            writer.writeTyped(e, type.getAdapterArgument(0));
    }
}
