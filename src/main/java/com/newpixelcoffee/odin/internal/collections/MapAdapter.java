package com.newpixelcoffee.odin.internal.collections;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.access.OdinRowReader;
import com.newpixelcoffee.odin.adapters.OdinObjectAdapter;

import java.io.IOException;
import java.util.Map;

public class MapAdapter implements OdinObjectAdapter<Map<Object, Object>> {

    @Override
    public void read(OdinReader reader, ObjectType<? extends Map<Object, Object>> type, Map<Object, Object> o) throws IOException {
        while (reader.hasNext()) {
            OdinRowReader r = reader.readRow();
            Object k = r.takeTyped(type.getAdapterArgument(0));
            Object v = r.takeTyped(type.getAdapterArgument(1));
            o.put(k, v);
        }
    }

    @Override
    public void write(OdinWriter writer, ObjectType<? extends Map<Object, Object>> type, Map<Object, Object> o) throws IOException {
        for (Map.Entry<Object, Object> e : o.entrySet())
            writer.writeRow().addTyped(e.getKey(), type.getAdapterArgument(0)).addTyped(e.getValue(), type.getAdapterArgument(1));
    }
}
