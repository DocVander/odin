package com.newpixelcoffee.odin.internal.collections;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.adapters.OdinObjectAdapter;

import java.io.IOException;
import java.util.Set;

public class SetAdapter implements OdinObjectAdapter<Set<Object>> {

    @Override
    public void read(OdinReader reader, ObjectType<? extends Set<Object>> type, Set<Object> o) throws IOException {
        while (reader.hasNext()) {
            Object value = reader.readTyped(type.getAdapterArgument(0));
            o.add(value);
        }
    }

    @Override
    public void write(OdinWriter writer, ObjectType<? extends Set<Object>> type, Set<Object> o) throws IOException {
        for (Object a : o)
            writer.writeTyped(a, type.getAdapterArgument(0));
    }
}
