package com.newpixelcoffee.odin.internal.collections;

import com.newpixelcoffee.odin.*;
import com.newpixelcoffee.odin.adapters.OdinObjectAdapter;
import com.newpixelcoffee.odin.exceptions.OdinTypeException;

import java.io.IOException;
import java.util.Map;

public class MapValuesAdapter implements OdinObjectAdapter<Map<Object, Object>> {

    private String key;

    public MapValuesAdapter(String key) {
        this.key = key;
    }

    @Override
    public void read(OdinReader reader, ObjectType<? extends Map<Object, Object>> type, Map<Object, Object> o) throws IOException {
        AnyType value = type.getAdapterArgument(1);
        FieldType field = null;
        if (value instanceof ObjectType)
            field = ((ObjectType) value).findField(key);
        if (field == null)
            throw new OdinTypeException("Unable to find Map key '" + key + "' in generic values type");
        while (reader.hasNext()) {
            try {
                Object v = reader.readTyped(type.getAdapterArgument(1));
                Object k = field.getField().get(v);
                o.put(k, v);
            } catch (IllegalAccessException e) {
                throw new OdinTypeException("Unable to access to key field from map values", e);
            }
        }
    }

    @Override
    public void write(OdinWriter writer, ObjectType<? extends Map<Object, Object>> type, Map<Object, Object> o) throws IOException {
        for (Map.Entry<Object, Object> e : o.entrySet())
            writer.writeTyped(e.getValue(), type.getAdapterArgument(1));
    }
}
