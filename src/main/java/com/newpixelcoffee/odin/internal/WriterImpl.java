package com.newpixelcoffee.odin.internal;

import com.newpixelcoffee.odin.AnyType;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.access.IOConsumer;
import com.newpixelcoffee.odin.access.OdinFieldWriter;
import com.newpixelcoffee.odin.access.OdinRowWriter;
import com.newpixelcoffee.odin.elements.*;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.TypeManager;

import java.io.IOException;
import java.lang.reflect.Type;

public abstract class WriterImpl implements OdinWriter, OdinFieldWriter, OdinRowWriter {

    protected StreamWriter stream;
    protected TypeManager types;

    protected WriterImpl parent;

    protected Object content;
    protected boolean firstNode = true;
    protected boolean firstRow = true;

    public WriterImpl(WriterImpl parent, TypeManager types, StreamWriter stream) {
        this.parent = parent;
        this.types = types;
        this.stream = stream;
    }

    @Override
    public void close() throws IOException {
        stream.close();
    }

    public WriterImpl openSubWriter(char open, Object object) throws IOException {
        content = object;
        stream.feed(open);
        return getSubWriter().openObject();
    }

    public abstract WriterImpl getSubWriter();

    protected abstract WriterImpl openObject();

    public abstract void closeObject(char end) throws IOException;

    public boolean isNotWriteRecursive(Object o) throws IOException {
        WriterImpl w = parent;
        int i = 0;
        while (w != null) {
            if (w.content == o) {
                stream.feed('(');
                stream.writeInt(i);
                stream.feed(')');
                return false;
            }
            w = w.parent;
            i++;
        }
        return true;
    }

    public TypeManager getTypeManager() {
        return types;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------- ELEMENTS

    protected abstract void writeExtra(OdinExtra extra) throws IOException;

    @Override
    public void writeNode(OdinNode node) throws IOException {
        if (node.hasExtra())
            writeExtra(node.getExtra());
        newEntry();
        writeNodeContent(node);
    }

    private void writeNodeContent(OdinNode node) throws IOException {
        WriterImpl sub = openSubWriter('{', node);
        for (NodeEntry e : node) {
            OdinElement o = e.getElement();
            if (o.hasExtra())
                sub.writeExtra(o.getExtra());
            sub.newEntry(e.getName());
            sub.writeElement(e.getElement());
        }
        if (node.hasContentExtra())
            sub.writeExtra(node.getContentExtra());
        sub.closeObject('}');
    }

    @Override
    public void writeArray(OdinArray array) throws IOException {
        if (array.hasExtra())
            writeExtra(array.getExtra());
        newEntry();
        writeArrayContent(array);
    }

    private void writeArrayContent(OdinArray array) throws IOException {
        WriterImpl sub = openSubWriter('[', array);
        for (int i = 0; i < array.size(); i++) {
            OdinElement e = array.get(i);
            if (e.hasExtra())
                sub.writeExtra(e.getExtra());
            sub.newEntry();
            sub.writeElement(e);
        }
        if (array.hasContentExtra())
            sub.writeExtra(array.getContentExtra());
        sub.closeObject(']');
    }

    @Override
    public void writeObject(OdinObject object) throws IOException {
        if (object.hasExtra())
            writeExtra(object.getExtra());
        types.UNKNOWN_TYPE.write(this, stream, object.value);
    }

    private void writeElement(OdinElement e) throws IOException {
        if (e.isNode())
            writeNodeContent(e.asNode());
        else if (e.isArray())
            writeArrayContent(e.asArray());
        else
            types.UNKNOWN_TYPE.write(this, stream, e.asObject().value);
    }

    // ------------------------------------------------------------------------------------------------------------------------------------------------- VALUES

    @Override
    public void writeBoolean(boolean b) throws IOException {
        newEntry();
        stream.writeBoolean(b);
    }

    @Override
    public void writeByte(byte b) throws IOException {
        newEntry();
        stream.writeInt(b);
    }

    @Override
    public void writeShort(short s) throws IOException {
        newEntry();
        stream.writeInt(s);
    }

    @Override
    public void writeInt(int i) throws IOException {
        newEntry();
        stream.writeInt(i);
    }

    @Override
    public void writeLong(long l) throws IOException {
        newEntry();
        stream.writeLong(l);
    }

    @Override
    public void writeFloat(float f) throws IOException {
        newEntry();
        stream.writeFloat(f);
    }

    @Override
    public void writeDouble(double d) throws IOException {
        newEntry();
        stream.writeDouble(d);
    }

    @Override
    public void writeChar(char c) throws IOException {
        newEntry();
        stream.writeChar(c);
    }

    @Override
    public void writeString(String s) throws IOException {
        newEntry();
        if (s == null)
            stream.writeNull();
        else
            stream.writeString(s);
    }

    @Override
    public void write(Object o) throws IOException {
        newEntry();
        types.UNKNOWN_TYPE.write(this, stream, o);
    }

    @Override
    public <T> void writeTyped(T o, AnyType<T> type) throws IOException {
        newEntry();
        ((AnyTypeImpl<T>) type).write(this, stream, o);
    }

    @Override
    public void writeTyped(Object o, Type type) throws IOException {
        newEntry();
        types.getType(type).write(this, stream, o);
    }

    @Override
    public void writeSubContent(IOConsumer<OdinWriter> subWriter) throws IOException {
        newEntry();
        WriterImpl sub = openSubWriter('{', null);
        subWriter.accept(sub);
        sub.closeObject('}');
    }

    @Override
    public OdinFieldWriter writeField(String name) throws IOException {
        newEntry(name);
        return this;
    }

    @Override
    public OdinRowWriter writeRow() {
        firstRow = true;
        return this;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------------------- ENTRY

    @Override
    public void withBoolean(boolean b) throws IOException {
        stream.writeBoolean(b);
    }

    @Override
    public void withByte(byte b) throws IOException {
        stream.writeInt(b);
    }

    @Override
    public void withShort(short s) throws IOException {
        stream.writeInt(s);
    }

    @Override
    public void withInt(int i) throws IOException {
        stream.writeInt(i);
    }

    @Override
    public void withLong(long l) throws IOException {
        stream.writeLong(l);
    }

    @Override
    public void withFloat(float f) throws IOException {
        stream.writeFloat(f);
    }

    @Override
    public void withDouble(double d) throws IOException {
        stream.writeDouble(d);
    }

    @Override
    public void withChar(char c) throws IOException {
        stream.writeChar(c);
    }

    @Override
    public void withString(String s) throws IOException {
        if (s == null)
            stream.writeNull();
        else
            stream.writeString(s);
    }

    @Override
    public void with(Object o) throws IOException {
        types.UNKNOWN_TYPE.write(this, stream, o);
    }

    @Override
    public <T> void withTyped(T o, AnyType<T> type) throws IOException {
        ((AnyTypeImpl<T>) type).write(this, stream, o);
    }

    @Override
    public void withTyped(Object o, Type type) throws IOException {
        types.getType(type).write(this, stream, o);
    }

    @Override
    public void withSubContent(IOConsumer<OdinWriter> subWriter) throws IOException {
        WriterImpl sub = openSubWriter('{', null);
        subWriter.accept(sub);
        sub.closeObject('}');
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------------------- MAP

    @Override
    public OdinRowWriter addBoolean(boolean b) throws IOException {
        newRow();
        stream.writeBoolean(b);
        return this;
    }

    @Override
    public OdinRowWriter addByte(byte b) throws IOException {
        newRow();
        stream.writeInt(b);
        return this;
    }

    @Override
    public OdinRowWriter addShort(short s) throws IOException {
        newRow();
        stream.writeInt(s);
        return this;
    }

    @Override
    public OdinRowWriter addInt(int i) throws IOException {
        newRow();
        stream.writeInt(i);
        return this;
    }

    @Override
    public OdinRowWriter addLong(long l) throws IOException {
        newRow();
        stream.writeLong(l);
        return this;
    }

    @Override
    public OdinRowWriter addFloat(float f) throws IOException {
        newRow();
        stream.writeFloat(f);
        return this;
    }

    @Override
    public OdinRowWriter addDouble(double d) throws IOException {
        newRow();
        stream.writeDouble(d);
        return this;
    }

    @Override
    public OdinRowWriter addChar(char c) throws IOException {
        newRow();
        stream.writeChar(c);
        return this;
    }

    @Override
    public OdinRowWriter addString(String s) throws IOException {
        newRow();
        if (s == null)
            stream.writeNull();
        else
            stream.writeString(s);
        return this;
    }

    @Override
    public OdinRowWriter add(Object o) throws IOException {
        newRow();
        types.UNKNOWN_TYPE.write(this, stream, o);
        return this;
    }

    @Override
    public <T> OdinRowWriter addTyped(T o, AnyType<T> type) throws IOException {
        newRow();
        ((AnyTypeImpl<T>) type).write(this, stream, o);
        return this;
    }

    @Override
    public OdinRowWriter addTyped(Object o, Type type) throws IOException {
        newRow();
        types.getType(type).write(this, stream, o);
        return this;
    }

    @Override
    public OdinRowWriter addSubContent(IOConsumer<OdinWriter> subWriter) throws IOException {
        newRow();
        WriterImpl sub = openSubWriter('{', null);
        subWriter.accept(sub);
        sub.closeObject('}');
        return this;
    }

    // ------------------------------------------------------------------------------------------------------------------------------------------------ PRIVATE

    protected abstract void newEntry() throws IOException;

    protected abstract void newEntry(String name) throws IOException;

    protected abstract void newRow() throws IOException;
}
