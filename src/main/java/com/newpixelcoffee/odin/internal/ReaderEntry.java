package com.newpixelcoffee.odin.internal;

import com.newpixelcoffee.odin.AnyType;
import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.access.IOConsumer;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;

import java.io.IOException;
import java.util.function.Consumer;

class ReaderEntry<T> {

    FieldReader reader;
    Runnable notFound;

    void define(Consumer<T> listener, AnyType<T> type) {
        this.reader = new TypeReader<>(listener, type, null);
    }

    void define(Consumer<T> listener, AnyType<T> type, T defaultValue) {
        this.reader = new TypeReader<>(listener, type, defaultValue);
    }

    void define(IOConsumer<OdinReader> listener) {
        this.reader = new SubReader(listener);
    }

    void notFound() {
        if (notFound != null)
            notFound.run();
    }

    interface FieldReader {
        void read(ReaderImpl reader, StreamReader stream) throws IOException;
    }

    private static class TypeReader<T> implements FieldReader {

        private Consumer<T> listener;
        private AnyTypeImpl<T> type;
        private T defaultValue;

        private TypeReader(Consumer<T> listener, AnyType<T> type, T defaultValue) {
            this.listener = listener;
            this.type = (AnyTypeImpl<T>) type;
            this.defaultValue = defaultValue;
        }

        @Override
        public void read(ReaderImpl reader, StreamReader stream) throws IOException {
            T r = type.read(reader, stream, defaultValue);
            if (listener != null)
                listener.accept(r);
        }
    }

    private static class SubReader implements FieldReader {

        private IOConsumer<OdinReader> listener;

        private SubReader(IOConsumer<OdinReader> listener) {
            this.listener = listener;
        }

        @Override
        public void read(ReaderImpl reader, StreamReader stream) throws IOException {
            if (stream.eat('{')) {
                ReaderImpl sub = reader.openSubReader(null);
                listener.accept(sub);
                sub.readEntries(null);
                if (stream.eat('}'))
                    stream.eatEnd();
                else
                    throw stream.getUnwantedCharException("Unable to get object ending.", '}');
            } else
                throw stream.getUnwantedCharException("Unable to get object beginning.", '{');
        }
    }
}
