package com.newpixelcoffee.odin.internal.streams;

import com.newpixelcoffee.odin.exceptions.OdinFormatException;

import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;

public class StreamReader {

    private static final char[] NULL_L  = new char[] { 'n', 'u', 'l', 'l' };
    private static final char[] NULL_U  = new char[] { 'N', 'U', 'L', 'L' };

    private static final char[] TRUE_L  = new char[] { 't', 'r', 'u', 'e' };
    private static final char[] TRUE_U  = new char[] { 'T', 'R', 'U', 'E' };
    private static final char[] FALSE_L = new char[] { 'f', 'a', 'l', 's', 'e' };
    private static final char[] FALSE_U = new char[] { 'F', 'A', 'L', 'S', 'E' };

    private static final char[] NAN_L = new char[] { 'n', 'a', 'n' };
    private static final char[] NAN_U = new char[] { 'N', 'A', 'N' };
    private static final char[] N_INFINITY_L = new char[] { '-', 'i', 'n', 'f', 'i', 'n', 'i', 't', 'y' };
    private static final char[] N_INFINITY_U = new char[] { '-', 'I', 'N', 'F', 'I', 'N', 'I', 'T', 'Y' };
    private static final char[] P_INFINITY_L = new char[] { 'i', 'n', 'f', 'i', 'n', 'i', 't', 'y' };
    private static final char[] P_INFINITY_U = new char[] { 'I', 'N', 'F', 'I', 'N', 'I', 'T', 'Y' };

    private Reader reader;

    private char[] buffer;
    private int index = 0;
    private int length;

    public StreamReader(Reader reader) {
        this(reader, 4096);
    }

    public StreamReader(Reader reader, int bufferSize) {
        this.reader = reader;
        if (bufferSize < 64)
            throw new IllegalArgumentException("Buffer size must be at least of 64.");
        buffer = new char[bufferSize];
    }

    public void close() throws IOException {
        reader.close();
    }

    // ---------------------------------------------------- PRIMITIVES

    public boolean readNull() throws IOException {
        loadBuffer(4);
        return readValue(NULL_L, NULL_U);
    }

    public boolean readBoolean() throws IOException {
        loadBuffer(8);
        if (readValue(FALSE_L, FALSE_U))
            return false;
        else if (readValue(TRUE_L, TRUE_U))
            return true;
        throw new OdinFormatException("Illegal boolean value : '" + new String(buffer, index, 6) + "'.");
    }

    public char readChar() throws IOException {
        if (eat('\'')) {
            char c = get(true);
            index++;
            if (c == '\\') {
                c = fromEscapeChar(get(true));
                index++;
            }
            if (eat('\'')) {
                tryReadEnd();
                return c;
            } throw getUnwantedCharException("Unable to get char ending.", '\'');
        }
        throw getUnwantedCharException("Unable to get char beginning.", '\'');
    }

    public int readInt() throws IOException {
        loadBuffer(16);
        if (get(true) == '-') {
            index++;
            return -getDigits();
        } else
            return getDigits();
    }

    public long readLong() throws IOException {
        loadBuffer(32);
        if (get(true) == '-') {
            index++;
            return -getLongDigits();
        } else
            return getLongDigits();
    }

    public float readFloat() throws IOException {
        loadBuffer(32);
        if (readValue(NAN_L, NAN_U))
            return Float.NaN;
        else if (readValue(N_INFINITY_L, N_INFINITY_U))
            return Float.NEGATIVE_INFINITY;
        else if (readValue(P_INFINITY_L, P_INFINITY_U))
            return Float.POSITIVE_INFINITY;
        char[] chars = buffer;
        int pos = index;
        parser: while (pos < length) {
            switch (chars[pos]) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '.':
                case 'e':
                case 'E':
                case '-':
                    pos++;
                    break;
                default:
                    break parser;
            }
        }
        if (pos == index)
            throw new OdinFormatException("Unable to get float beginning. read '" + chars[pos] + "'.");
        float f = Float.parseFloat(new String(buffer, index, pos-index));
        index = pos;
        tryReadEnd();
        return f;
    }

    public double readDouble() throws IOException {
        loadBuffer(32);
        if (readValue(NAN_L, NAN_U))
            return Double.NaN;
        else if (readValue(N_INFINITY_L, N_INFINITY_U))
            return Double.NEGATIVE_INFINITY;
        else if (readValue(P_INFINITY_L, P_INFINITY_U))
            return Double.POSITIVE_INFINITY;
        char[] chars = buffer;
        int pos = index;
        parser: while (pos < length) {
            switch (chars[pos]) {
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '.':
                case 'e':
                case 'E':
                case '-':
                    pos++;
                    break;
                default:
                    break parser;
            }
        }
        if (pos == index)
            throw new OdinFormatException("Unable to get double beginning. read '" + chars[pos] + "'.");
        double d = Double.parseDouble(new String(buffer, index, pos-index));
        index = pos;
        tryReadEnd();
        return d;
    }

    private int getDigits() throws IOException {
        char[] chars = buffer;
        int pos = index;
        int value = 0;
        parser: while (pos < length) {
            switch (chars[pos]) {
                case '0':
                    value *= 10;
                    break;
                case '1':
                    value = value * 10 + 1;
                    break;
                case '2':
                    value = value * 10 + 2;
                    break;
                case '3':
                    value = value * 10 + 3;
                    break;
                case '4':
                    value = value * 10 + 4;
                    break;
                case '5':
                    value = value * 10 + 5;
                    break;
                case '6':
                    value = value * 10 + 6;
                    break;
                case '7':
                    value = value * 10 + 7;
                    break;
                case '8':
                    value = value * 10 + 8;
                    break;
                case '9':
                    value = value * 10 + 9;
                    break;
                default:
                    break parser;
            }
            pos++;
        }
        if (pos == index)
            throw new OdinFormatException("Unable to get int beginning. read '" + chars[pos] + "'.");
        index = pos;
        tryReadEnd();
        return value;
    }

    private long getLongDigits() throws IOException {
        char[] chars = buffer;
        int pos = index;
        long value = 0;
        parser: while (pos < length) {
            switch (chars[pos]) {
                case '0':
                    value *= 10;
                    break;
                case '1':
                    value = value * 10 + 1;
                    break;
                case '2':
                    value = value * 10 + 2;
                    break;
                case '3':
                    value = value * 10 + 3;
                    break;
                case '4':
                    value = value * 10 + 4;
                    break;
                case '5':
                    value = value * 10 + 5;
                    break;
                case '6':
                    value = value * 10 + 6;
                    break;
                case '7':
                    value = value * 10 + 7;
                    break;
                case '8':
                    value = value * 10 + 8;
                    break;
                case '9':
                    value = value * 10 + 9;
                    break;
                default:
                    break parser;
            }
            pos++;
        }
        if (pos == index)
            throw new OdinFormatException("Unable to get long beginning. read '" + chars[pos] + "'.");
        index = pos;
        tryReadEnd();
        return value;
    }

    private boolean readValue(char[] lowerCase, char[] upperCase) throws IOException {
        int l = lowerCase.length;
        char[] chars = buffer;
        int pos = index;
        for (int i = 0; i < l; pos++, i++) {
            if (chars[pos] != lowerCase[i] && chars[pos] != upperCase[i])
                return false;
        }
        index = pos;
        tryReadEnd();
        return true;
    }

    public String readString() throws IOException {
        if (!eat('"'))
            throw getUnwantedCharException("Unable to get String beginning.", '"');
        if (get(true) == '\r' || get(true) == '\n')
            skip();
        StringBuilder sb = null;
        char[] chars = buffer;
        int pos = index;

        while (true) {
            while (pos < length) {
                switch (chars[pos]) {
                    case '\u0000':
                        throw new EOFException("Odin reader reached end of stream");
                    case '\\':
                        if (sb == null)
                            sb = new StringBuilder();
                        sb.append(chars, index, pos-index);
                        index = pos + 1;
                        sb.append(fromEscapeChar(get(true)));
                        pos = ++index;
                        break;
                    case '\r':
                        if (sb == null)
                            sb = new StringBuilder();
                        sb.append(chars, index, pos-index);
                        index = pos + 1;
                        char cr = next(true);
                        if (cr == '\n') {
                            index++;
                            if (next(true) == '"') {
                                index++;
                                tryReadEnd();
                                return sb.toString();
                            } else
                                sb.append('\r').append('\n');
                        } else if (cr == '"') {
                            index++;
                            tryReadEnd();
                            return sb.toString();
                        } else
                            sb.append('\r');
                        pos = index;
                        break;
                    case '\n':
                        if (sb == null)
                            sb = new StringBuilder();
                        sb.append(chars, index, pos-index);
                        index = pos + 1;
                        char cn = next(true);
                        if (cn == '"') {
                            index++;
                            tryReadEnd();
                            return sb.toString();
                        } else
                            sb.append('\n');
                        pos = index;
                        break;
                    case '"':
                        String s;
                        if (sb != null)
                            s = sb.append(chars, index, pos-index).toString();
                        else
                            s = new String(chars, index, pos-index);
                        index = pos + 1;
                        tryReadEnd();
                        return s;
                    default:
                        pos++;
                }
            }

            if (sb == null)
                sb = new StringBuilder();
            sb.append(chars, index, pos-index);
            length = reader.read(chars);
            if (length == -1)
                throw new EOFException("Odin reader reached end of stream");
            index = 0;
            pos = 0;
        }
    }

    public String readKey() throws IOException {
        skip();
        StringBuilder sb = null;
        StringBuilder trim = null;
        char[] chars = buffer;
        int pos = index;
        int l = pos;

        while (true) {
            while (pos < length) {
                switch (chars[pos]) {
                    case '\\':
                        if (sb == null)
                            sb = new StringBuilder();
                        if (trim != null) {
                            sb.append(trim);
                            trim = null;
                        }
                        sb.append(chars, index, pos - index);
                        index = pos + 1;
                        sb.append(fromEscapeChar(get(true)));
                        pos = ++index;
                        l = pos;
                        break;
                    case '=':
                    case '{':
                    case '[':
                    case '<':
                        String s;
                        if (sb != null)
                            s = sb.append(chars, index, l - index).toString();
                        else
                            s = new String(chars, index, l - index);
                        index = pos;
                        eatEnd();
                        return s;
                    case '\r':
                    case '\n':
                    case ':':
                    case ',':
                    case '}':
                    case ']':
                    case '>':
                    case '\u0000':
                        index = pos;
                        eatEnd();
                        throw new OdinFormatException("Try to read key but found value delimiter : '" + chars[pos] + "'.");
                    case ' ':
                    case '\t':
                        pos++;
                        break;
                    default:
                        if (trim != null) {
                            if (sb == null)
                                sb = new StringBuilder();
                            sb.append(trim);
                            trim = null;
                        }
                        l = ++pos;
                        break;
                }
            }

            if (index < l) {
                if (sb == null)
                    sb = new StringBuilder();
                sb.append(chars, index, l - index);
            }
            if (l < pos) {
                if (trim == null)
                    trim = new StringBuilder();
                trim.append(chars, l,  pos - l);
            }
            length = reader.read(chars);
            if (length == -1) {
                // allow a last iteration to read null char in switch case
                chars[0] = '\u0000';
                length = 1;
            }
            index = 0;
            pos = 0;
            l = 0;
        }
    }

    public String readValue() throws IOException {
        skip();
        StringBuilder sb = null;
        StringBuilder trim = null;
        char[] chars = buffer;
        int pos = index;
        int l = pos;

        while (true) {
            while (pos < length) {
                switch (chars[pos]) {
                    case '\\':
                        if (sb == null)
                            sb = new StringBuilder();
                        if (trim != null) {
                            sb.append(trim);
                            trim = null;
                        }
                        sb.append(chars, index, pos - index);
                        index = pos + 1;
                        sb.append(fromEscapeChar(get(true)));
                        pos = ++index;
                        l = pos;
                        break;
                    case '\r':
                    case '\n':
                    case ',':
                    case ':':
                    case '}':
                    case ']':
                    case '\u0000':
                        String s;
                        if (sb != null)
                            s = sb.append(chars, index, l-index).toString();
                        else
                            s = new String(chars, index, l-index);
                        index = pos;
                        eatEnd();
                        return s;
                    case ' ':
                    case '\t':
                        pos++;
                        break;
                    case '=':
                        index = pos;
                        eatEnd();
                        throw new OdinFormatException("Try to read value but found key delimiter : '='.");
                    default:
                        if (trim != null) {
                            if (sb == null)
                                sb = new StringBuilder();
                            sb.append(trim);
                            trim = null;
                        }
                        l = ++pos;
                        break;
                }
            }

            if (index < l) {
                if (sb == null)
                    sb = new StringBuilder();
                sb.append(chars, index, l - index);
            }
            if (l < pos) {
                if (trim == null)
                    trim = new StringBuilder();
                trim.append(chars, l,  pos - l);
            }
            length = reader.read(chars);
            if (length == -1) {
                // allow a last iteration to read null char in switch case
                chars[0] = '\u0000';
                length = 1;
            }
            index = 0;
            pos = 0;
            l = 0;
        }
    }

    public String readComment() throws IOException {
        skip();
        StringBuilder sb = null;
        char[] chars = buffer;
        int pos = index;

        while (true) {
            while (pos < length) {
                switch (chars[pos]) {
                    case '\\':
                        if (sb == null)
                            sb = new StringBuilder();
                        sb.append(chars, index, pos-index);
                        index = pos + 1;
                        sb.append(fromEscapeChar(get(true)));
                        pos = ++index;
                        break;
                    case '\r':
                    case '\n':
                    case '\u0000':
                        String s;
                        if (sb != null)
                            s = sb.append(chars, index, pos-index).toString();
                        else
                            s = new String(chars, index, pos-index);
                        index = pos;
                        eatEnd();
                        return s;
                    default:
                        pos++;
                        break;
                }
            }

            if (sb == null)
                sb = new StringBuilder();
            sb.append(chars, index, pos-index);
            length = reader.read(chars);
            if (length == -1) {
                // allow a last iteration to read null char in switch case
                chars[0] = '\u0000';
                length = 1;
            }
            index = 0;
            pos = 0;
        }
    }

    public String readType() throws IOException {
        skip();
        StringBuilder sb = null;
        char[] chars = buffer;
        int pos = index;
        while (true) {
            while (pos < length) {
                if (chars[pos] == '>') {
                    String s;
                    if (sb != null)
                        s = sb.append(chars, index, pos-index).toString();
                    else
                        s = new String(chars, index, pos-index);
                    index = pos + 1;
                    return s;
                } else
                    pos++;
            }

            if (sb == null)
                sb = new StringBuilder();
            sb.append(chars, index, pos - index);
            length = reader.read(chars);
            if (length == -1)
                throw new EOFException("Odin reader reached end of stream");
            index = 0;

            pos = 0;
        }
    }

    public RuntimeException getUnwantedCharException(String error, char expected) throws IOException {
        return new OdinFormatException(error + " read '" + get(false) + "' but expected '" + expected + "'.");
    }

    private void tryReadEnd() throws IOException {
        if (!eatEnd())
            throw new OdinFormatException("Wrong element ending, found '" + get(false) + "' inside value.");
    }

    // ---------------------------------------------------- CHARS

    /**
     * Skip all empty char (\s, \t, \r or \n) and get next char.
     * If next char equals c, next char is eaten
     *
     * @param c char to test
     * @return true if c equals next char
     * @throws IOException if an I/O Exception occur or end of stream reached
     */
    public boolean eat(char c) throws IOException {
        if (skip() == c) {
            index++;
            return true;
        }
        return false;
    }

    /**
     * Skip all empty char (\s or \t) and eat char representing end of value.
     * Eat CRLF, '=', ':' or ','
     *
     * @return true if a elements end is successfully eat
     * @throws IOException if an I/O Exception occur or end of stream reached
     */
    public boolean eatEnd() throws IOException {
        char c = next(false);
        switch (c) {
            case '\r':
                index++;
                if (next(true) == '\n')
                    index++;
                return true;
            case '\n':
            case '=':
            case ',':
                index++;
                return true;
            case ')':
            case '}':
            case ']':
            case ':':
            case '\u0000':
                return true;
        }
        return false;
    }

    /**
     * Skip empty char (\s or \t) and get next char.
     * If next chars equals \r, \r\n or \n, next chars is eaten
     *
     * @return true if CRLF is eaten
     * @throws IOException if an I/O Exception occur or end of stream reached
     */
    public boolean eatCrLf() throws IOException {
        char c = next(false);
        if (c == '\r') {
            index++;
            if (next(false) == '\n')
                index++;
            return true;
        } else if (c == '\n') {
            index++;
            return true;
        } else
            return false;
    }

    /**
     * Skip empty char (\s or \t) and get next char.
     *
     * @param eof true to thrown EOFException when reach the end of stream
     * @return next char
     * @throws IOException if an I/O Exception occur or end of stream reached
     */
    public char next(boolean eof) throws IOException {
        char c = get(eof);
        while (c == ' ' || c == '\t') {
            index++;
            c = get(eof);
        }
        return c;
    }

    /**
     * Skip all empty char (\s, \t, \r and \n) and get next char.
     *
     * @return next char
     * @throws IOException if an I/O Exception occur or end of stream reached
     */
    public char skip() throws IOException {
        char c = get(true);
        while (c == ' ' || c == '\t' || c == '\r' || c == '\n') {
            index++;
            c = get(true);
        }
        return c;
    }

    /**
     * Get char at actual index, if index reach end of stream thrown and EOFException or return '\u0000' if eof is false
     *
     * @param eof true to allow method to throw EOFException
     * @return current char
     * @throws IOException if an I/O Exception occur or end of stream reached
     */
    private char get(boolean eof) throws IOException {
        if (index < length)
            return buffer[index];
        else {
            length = reader.read(buffer);
            index = 0;
            if (length == -1) {
                if (eof)
                    throw new EOFException("Odin reader reached end of stream");
                else
                    return '\u0000';
            }
            return buffer[index];
        }
    }

    /**
     * If ( index + minimum size ) is greater than buffer size, move buffer[index] to 0 and refill buffer.
     * @param size needed chars after index
     * @throws IOException IOException
     */
    private void loadBuffer(int size) throws IOException {
        skip();
        int i = index + size;
        if (i >= length) {
            if (index < length) {
                int l = length - index;
                System.arraycopy(buffer, index, buffer, 0, l);
                length = Math.max(reader.read(buffer, l, length - l), 0) + l;
            } else
                length = reader.read(buffer);
            index = 0;
        }
    }

    private static char fromEscapeChar(char c) {
        switch (c) {
            case 'n':
                return '\n';
            case 't':
                return '\t';
            case 'r':
                return '\r';
            case 'b':
                return '\b';
            case 'f':
                return '\f';
            default:
                return c;
        }
    }
}
