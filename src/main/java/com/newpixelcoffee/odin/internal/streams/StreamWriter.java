package com.newpixelcoffee.odin.internal.streams;

import com.newpixelcoffee.odin.exceptions.OdinFormatException;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Field;

public class StreamWriter {

    private static final char[] NULL  = new char[] { 'n', 'u', 'l', 'l' };
    private static final char[] TRUE  = new char[] { 't', 'r', 'u', 'e' };
    private static final char[] FALSE = new char[] { 'f', 'a', 'l', 's', 'e' };

    private Field STRING_TO_CHARS;

    private Writer writer;
    private String indentation;
    private char[] newLine;

    public StreamWriter(Writer writer) {
        this.writer = writer;
        newLine = toChars(System.getProperty("line.separator"));

        // string reflect
        try {
            // 1.7, 1.8 | 9.x, 10.x, 11.x
            if (System.getProperty("java.version").startsWith("1.")) {
                STRING_TO_CHARS = String.class.getDeclaredField("value");
                STRING_TO_CHARS.setAccessible(true);
            }
        } catch (NoSuchFieldException e) {
            System.err.println("Unable to load reflect optimization for String, writer use standard toCharArray() method instead.");
        }
    }

    public void close() throws IOException {
        writer.close();
    }

    public void changeIndentation(String indentation) {
        this.indentation = indentation;
    }

    // ---------------------------------------------------- PRIMITIVES

    public void writeNull() throws IOException {
        feed(NULL);
    }

    public void writeBoolean(boolean b) throws IOException {
        if (b)
            feed(TRUE);
        else
            feed(FALSE);
    }

    public void writeInt(int i) throws IOException {
        feed(Integer.toString(i));
    }

    public void writeLong(long l) throws IOException {
        feed(Long.toString(l));
    }

    public void writeFloat(float f) throws IOException {
        feed(Float.toString(f));
    }

    public void writeDouble(double d) throws IOException {
        feed(Double.toString(d));
    }

    public void writeChar(char c) throws IOException {
        writer.write('\'');
        switch (c) {
            case '\\':
                writer.write('\\');
                writer.write('\\');
                break;
            case '\'':
                writer.write('\\');
                writer.write('\'');
                break;
            case '\n':
                writer.write('\\');
                writer.write('n');
                break;
            case '\t':
                writer.write('\\');
                writer.write('t');
                break;
            case '\r':
                writer.write('\\');
                writer.write('r');
                break;
            case '\b':
                writer.write('\\');
                writer.write('b');
                break;
            case '\f':
                writer.write('\\');
                writer.write('f');
                break;
            default:
                writer.write(c);
                break;
        }
        writer.write('\'');
    }

    public void writeString(String s) throws IOException {
        writer.write('"');
        char[] array = toChars(s);
        boolean lineReturn = indentation != null && containsLineReturn(array);
        if (lineReturn) {
            feedNewLine();
            writer.write('\t');
        }
        int l = array.length;
        int n = 0;
        for (int i = 0; i < l; i++) {
            char c = array[i];
            switch (c) {
                case '"':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('"');
                    break;
                case '\\':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('\\');
                    break;
                case '\r':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    if (indentation == null) {
                        writer.write('\\');
                        writer.write('r');
                    } else {
                        writer.write('\r');
                        // CRLF
                        if (i+1 < l && array[i+1] == '\n') {
                            writer.write('\n');
                            i++;
                            n++;
                        }
                        writer.write(indentation);
                        writer.write('\t');
                        if (i + 1 < l && array[i + 1] == ' ')
                            writer.write('\\');
                    }
                    break;
                case '\n':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    if (indentation == null) {
                        writer.write('\\');
                        writer.write('n');
                    } else {
                        writer.write('\n');
                        writer.write(indentation);
                        writer.write('\t');
                        if (i + 1 < l && array[i + 1] == ' ')
                            writer.write('\\');
                    }
                    break;
                case '\t':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('t');
                    break;
                case '\b':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('b');
                    break;
                case '\f':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('f');
                    break;
            }
        }
        if (n < l)
            writer.write(array, n, l - n);
        if (lineReturn)
            feedNewLine();
        writer.write('"');
    }

    public void writeKey(String k) throws IOException {
        char[] array = toChars(k);
        int l = array.length;
        int n = 0;
        for (int i = 0; i < l; i++) {
            char c = array[i];
            switch (c) {
                case '\\':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('\\');
                    break;
                case '\n':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('n');
                    break;
                case '\t':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('t');
                    break;
                case '\r':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('r');
                    break;
                case '\b':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('b');
                    break;
                case '\f':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('f');
                    break;
                case ',':
                case ':':
                case '=':
                case '[':
                case ']':
                case '{':
                case '}':
                case '<':
                case '>':
                case '\'':
                case '"':
                    // if change this char list, change exception description in Odin and Writer
                    throw new OdinFormatException("Key can not contains the following delimiter ,:={}[]<>'\"");
            }
        }
        if (n < l)
            writer.write(array, n, l - n);
    }

    public void writeComment(String s) throws IOException {
        char[] array = toChars(s);
        int l = array.length;
        int n = 0;
        for (int i = 0; i < l; i++) {
            char c = array[i];
            switch (c) {
                case '\\':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('\\');
                    break;
                case '\n':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('n');
                    break;
                case '\t':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('t');
                    break;
                case '\r':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('r');
                    break;
                case '\b':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('b');
                    break;
                case '\f':
                    if (n < i)
                        writer.write(array, n, i - n);
                    n = i + 1;
                    writer.write('\\');
                    writer.write('f');
                    break;
            }
        }
        if (n < l)
            writer.write(array, n, l - n);
    }

    public void writeType(String type) throws IOException {
        feed('<');
        feed(type);
        feed('>');
        if (indentation != null)
            feed(' ');
    }

    // ---------------------------------------------------- CHARS

    public void feedNewLine() throws IOException {
        writer.write(newLine);
        writer.write(toChars(indentation));
    }

    public void feed(char c) throws IOException {
        writer.write(c);
    }

    public void feed(char[] c) throws IOException {
        writer.write(c);
    }

    public void feed(String s) throws IOException {
        writer.write(toChars(s));
    }

    private char[] toChars(String s) {
        if (STRING_TO_CHARS != null) {
            try {
                return (char[]) STRING_TO_CHARS.get(s);
            } catch (IllegalAccessException ignored) {}
        }
        return s.toCharArray();
    }

    private boolean containsLineReturn(char[] chars) {
        for (char c : chars) {
            switch (c) {
                case '\r':
                case '\n':
                    return true;
            }
        }
        return false;
    }
}
