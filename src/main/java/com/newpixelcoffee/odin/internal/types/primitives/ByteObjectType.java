package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class ByteObjectType extends AnyTypeImpl<Byte> {

    @Override
    public Class<Byte> getType() {
        return Byte.class;
    }

    @Override
    public Type getGenericType() {
        return Byte.class;
    }

    @Override
    public String getName() {
        return "Byte";
    }

    @Override
    public <R extends Byte> Byte read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return stream.readNull() ? null : (byte) stream.readInt();
    }

    @Override
    public <R extends Byte> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        if (o == null)
            stream.writeNull();
        else
            stream.writeInt(o);
    }
}
