package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class LongObjectType extends AnyTypeImpl<Long> {

    @Override
    public Class<Long> getType() {
        return Long.class;
    }

    @Override
    public Type getGenericType() {
        return Long.class;
    }

    @Override
    public String getName() {
        return "Long";
    }

    @Override
    public <R extends Long> Long read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return stream.readNull() ? null : stream.readLong();
    }

    @Override
    public <R extends Long> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        if (o == null)
            stream.writeNull();
        else
            stream.writeLong(o);
    }
}
