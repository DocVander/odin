package com.newpixelcoffee.odin.internal.types;

import com.newpixelcoffee.odin.exceptions.OdinTypeException;
import com.newpixelcoffee.odin.internal.processor.Processor;

public class ClassTypeImpl<T> extends ObjectTypeImpl<T> {

    ClassTypeImpl(TypeManager manager, Class<T> type) {
        super(manager, type);
    }

    ClassTypeImpl(TypeManager manager, Class<T> type, String name) {
        super(manager, type, name);
    }

    @Override
    protected <E extends T> AnyTypeImpl<E> getInstanceType(E o) {
        Class<E> c = (Class<E>) o.getClass();
        if (forceProcessor) {
            AnyTypeImpl<E> r = manager.getCustomInstanceType(c);
            if (r instanceof ClassTypeImpl) {
                ClassTypeImpl<E> t = (ClassTypeImpl<E>) r;
                t.processor = (Processor<E>) processor;
                t.processorArguments = t.getSuperClass(getType()).getGenericArguments();
            }
            return r;
        } else
            return manager.getInstanceType(c);
    }

    @Override
    protected <E extends T> AnyTypeImpl<E> readInstanceType(String name) {
        if (forceProcessor) {
            AnyTypeImpl<E> r = manager.getCustomInstanceType(manager.getClass(name));
            if (r instanceof ClassTypeImpl) {
                ClassTypeImpl<E> t = (ClassTypeImpl<E>) r;
                t.processor = (Processor<E>) processor;
                t.processorArguments = t.getSuperClass(getType()).getGenericArguments();
            }
            return r;
        } else
            return manager.getInstanceType(manager.getClass(name));
    }

    @Override
    public AnyTypeImpl getGenericArgument(int index) {
        return manager.UNKNOWN_TYPE;
    }

    @Override
    public AnyTypeImpl[] getGenericArguments() {
        return TypeManager.EMPTY_TYPE;
    }

    @Override
    public Processor<T> getClassProcessor() {
        return processor;
    }

    @Override
    public ClassTypeImpl registerAdapterType() {
        throw new OdinTypeException("Wrong Adapter parameter, specify type parameter to class implementing OdinAdapter.");
    }
}
