package com.newpixelcoffee.odin.internal.types;

import com.newpixelcoffee.odin.FieldType;

import java.lang.reflect.Field;

public class TypeField<T> implements FieldType<T> {

    final String name;

    private final Field field;
    private final AnyTypeImpl<T> type;
    private final String[] comments;
    private boolean readTo;

    TypeField(String name, Field field, AnyTypeImpl<T> type, String[] comments, boolean readTo) {
        this.name = name;
        this.field = field;
        this.type = type;
        this.comments = comments;
        this.readTo = readTo;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public AnyTypeImpl<T> getType() {
        return type;
    }

    @Override
    public Field getField() {
        return field;
    }

    @Override
    public String[] getComments() {
        return comments;
    }

    public boolean isReadTo() {
        return readTo;
    }
}
