package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class DoubleObjectType extends AnyTypeImpl<Double> {

    @Override
    public Class<Double> getType() {
        return Double.class;
    }

    @Override
    public Type getGenericType() {
        return Double.class;
    }

    @Override
    public String getName() {
        return "Double";
    }

    @Override
    public <R extends Double> Double read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return stream.readNull() ? null : stream.readDouble();
    }

    @Override
    public <R extends Double> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        if (o == null)
            stream.writeNull();
        else
            stream.writeDouble(o);
    }
}
