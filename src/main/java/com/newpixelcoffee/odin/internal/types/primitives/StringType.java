package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.ArrayTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class StringType extends AnyTypeImpl<String> {

    @Override
    public Class<String> getType() {
        return String.class;
    }

    @Override
    public Type getGenericType() {
        return String.class;
    }

    @Override
    public String getName() {
        return "string";
    }

    @Override
    public ArrayTypeImpl<String[], String> toArray() {
        if (array == null)
            array = new StringArrayType(this);
        return array;
    }

    @Override
    public <R extends String> String read(OdinReader reader, StreamReader stream, R o) throws IOException {
        if (stream.readNull())
            return null;
        return stream.readString();
    }

    @Override
    public <R extends String> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        if (o == null)
            stream.writeNull();
        else
            stream.writeString(o);
    }
}
