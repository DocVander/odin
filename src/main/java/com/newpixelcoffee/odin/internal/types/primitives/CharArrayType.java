package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.ReaderImpl;
import com.newpixelcoffee.odin.internal.WriterImpl;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.ArrayTypeImpl;

import java.io.IOException;

/**
 * @author DocVander
 */
public class CharArrayType extends ArrayTypeImpl<char[], Character> {

    CharArrayType(AnyTypeImpl<Character> content) {
        super(content);
    }

    @Override
    public char[] read(OdinReader reader, StreamReader stream, char[] o) throws IOException {
        if (stream.readNull())
            return null;
        int i = 0;
        ReaderImpl in = (ReaderImpl) reader;
        if (stream.eat('[')) {
            int size = 8;
            char[] r = new char[size];
            OdinReader sub = in.openSubReader(null);
            while (!in.reachArrayEnd()) {
                char element = sub.readChar();
                if (i == size) {
                    size += size >> 1;
                    char[] a = new char[size];
                    System.arraycopy(r, 0, a, 0, i);
                    r = a;
                }
                r[i++] = element;
            }
            stream.eatEnd();
            char[] a = new char[i];
            System.arraycopy(r, 0, a, 0, i);
            return a;
        } else
            throw stream.getUnwantedCharException("Unable to get array beginning.", '[');
    }

    @Override
    public void write(OdinWriter writer, StreamWriter stream, char[] o) throws IOException {
        WriterImpl w = (WriterImpl) writer;
        if (o == null)
            stream.writeNull();
        else {
            WriterImpl sub = w.openSubWriter('[', o);
            for (char element : o) sub.writeChar(element);
            sub.closeObject(']');
        }
    }
}
