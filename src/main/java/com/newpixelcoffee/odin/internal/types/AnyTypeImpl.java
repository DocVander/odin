package com.newpixelcoffee.odin.internal.types;

import com.newpixelcoffee.odin.AnyType;
import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.ReaderImpl;
import com.newpixelcoffee.odin.internal.WriterImpl;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;

import java.io.IOException;

public abstract class AnyTypeImpl<T> implements AnyType<T> {

    protected ArrayTypeImpl<T[], T> array;

    public abstract <R extends T> T read(OdinReader reader, StreamReader stream, R o) throws IOException;

    public abstract <R extends T> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException;

    protected <R extends T> T subRead(ReaderImpl reader, StreamReader stream, R o) throws IOException {
        return read(reader, stream, o);
    }

    protected <R extends T> void subWrite(WriterImpl writer, StreamWriter stream, R o) throws IOException {
        write(writer, stream, o);
    }

    public void writeType(StreamWriter stream, boolean notUnknown) throws IOException {
        if (notUnknown)
            stream.writeType(getName());
    }

    public ArrayTypeImpl<T[], T> toArray() {
        if (array == null)
            array = new ArrayTypeImpl<>(this);
        return array;
    }

    @Override
    public boolean isObject() {
        return false;
    }

    @Override
    public ObjectTypeImpl<T> asObject() {
        return (ObjectTypeImpl<T>) this;
    }
}
