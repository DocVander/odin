package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.ReaderImpl;
import com.newpixelcoffee.odin.internal.WriterImpl;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.ArrayTypeImpl;

import java.io.IOException;

/**
 * @author DocVander
 */
public class DoubleArrayType extends ArrayTypeImpl<double[], Double> {

    DoubleArrayType(AnyTypeImpl<Double> content) {
        super(content);
    }

    @Override
    public double[] read(OdinReader reader, StreamReader stream, double[] o) throws IOException {
        if (stream.readNull())
            return null;
        int i = 0;
        ReaderImpl in = (ReaderImpl) reader;
        if (stream.eat('[')) {
            int size = 8;
            double[] r = new double[size];
            OdinReader sub = in.openSubReader(null);
            while (!in.reachArrayEnd()) {
                double element = sub.readDouble();
                if (i == size) {
                    size += size >> 1;
                    double[] a = new double[size];
                    System.arraycopy(r, 0, a, 0, i);
                    r = a;
                }
                r[i++] = element;
            }
            stream.eatEnd();
            double[] a = new double[i];
            System.arraycopy(r, 0, a, 0, i);
            return a;
        } else
            throw stream.getUnwantedCharException("Unable to get array beginning.", '[');
    }

    @Override
    public void write(OdinWriter writer, StreamWriter stream, double[] o) throws IOException {
        WriterImpl w = (WriterImpl) writer;
        if (o == null)
            stream.writeNull();
        else {
            WriterImpl sub = w.openSubWriter('[', o);
            for (double element : o) sub.writeDouble(element);
            sub.closeObject(']');
        }
    }
}
