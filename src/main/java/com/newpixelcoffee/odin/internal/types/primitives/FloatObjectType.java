package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class FloatObjectType extends AnyTypeImpl<Float> {

    @Override
    public Class<Float> getType() {
        return Float.class;
    }

    @Override
    public Type getGenericType() {
        return Float.class;
    }

    @Override
    public String getName() {
        return "Float";
    }

    @Override
    public <R extends Float> Float read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return stream.readNull() ? null : stream.readFloat();
    }

    @Override
    public <R extends Float> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        if (o == null)
            stream.writeNull();
        else
            stream.writeFloat(o);
    }
}
