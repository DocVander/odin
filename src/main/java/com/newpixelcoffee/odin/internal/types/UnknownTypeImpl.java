package com.newpixelcoffee.odin.internal.types;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.ReaderImpl;
import com.newpixelcoffee.odin.internal.WriterImpl;
import com.newpixelcoffee.odin.internal.defaults.DefaultObjectAdapter;
import com.newpixelcoffee.odin.internal.processor.ObjectProcessor;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;

import java.io.IOException;

public class UnknownTypeImpl extends ClassTypeImpl<Object> {

    UnknownTypeImpl(TypeManager manager) {
        super(manager, Object.class, "object");
        setProcessor(new ObjectProcessor<>(new DefaultObjectAdapter()));
    }

    @Override
    protected <E> ObjectTypeImpl<E> getInstanceType(E o) {
        return null;
    }

    @Override
    protected <E> ObjectTypeImpl<E> readInstanceType(String name) {
        return null;
    }

    @Override
    public <R> Object read(OdinReader reader, StreamReader stream, R o) throws IOException {
        // just in case of unknown object contains rows
        stream.eat(':');
        if (stream.readNull())
            return null;
        ReaderImpl r = (ReaderImpl) reader;
        if (stream.eat('(')) {
            int index = stream.readInt();
            stream.eat(')');
            stream.eatEnd();
            return r.getRecursive(index);
        }
        if (stream.eat('<')) {
            int dimensions = 0;
            while (stream.eat('['))
                dimensions++;
            String name = stream.readType();
            if (dimensions > 0) {
                AnyTypeImpl t = manager.getType(name);
                while (dimensions-- > 0)
                    t = t.toArray();
                return t.subRead(r, stream, o);
            } else {
                AnyTypeImpl<R> t = manager.getType(name);
                return t.subRead(r, stream, o);
            }
        }
        char next = stream.skip();
        switch (next) {
            case '\'':
                return stream.readChar();
            case '"':
                return stream.readString();
            case '{':
                return getProcessor().read(r, stream, this, o);
            case '[':
                return ((AnyTypeImpl<R>) toArray()).subRead(r, stream, o);
            default:
                String value = stream.readValue();
                if ("true".equalsIgnoreCase(value))
                    return true;
                else if ("false".equalsIgnoreCase(value))
                    return false;
                try {
                    return Integer.parseInt(value);
                } catch (NumberFormatException ignored) {}
                try {
                    return Long.parseLong(value);
                } catch (NumberFormatException ignored) {}
                try {
                    return Float.parseFloat(value);
                } catch (NumberFormatException ignored) {}
                try {
                    return Double.parseDouble(value);
                } catch (NumberFormatException ignored) {}
                return value;
        }
    }

    @Override
    public <R> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        WriterImpl w = (WriterImpl) writer;
        if (o == null)
            stream.writeNull();
        else if (w.isNotWriteRecursive(o)) {
            if (o.getClass() != Object.class) {
                Class<R> c = (Class<R>) o.getClass();
                AnyTypeImpl<R> type = manager.getAnyInstanceType(c);
                type.writeType(stream, false);
                type.subWrite(w, stream, o);
            } else
                getProcessor().write(w, stream, this, o);
        }
    }

    @Override
    public void writeType(StreamWriter stream, boolean notUnknown) {
        // unused
    }

    @Override
    public ObjectTypeImpl<? super Object> getSuperClass() {
        return null;
    }

    @Override
    public ObjectTypeImpl<? super Object> getSuperClass(Class<? super Object> target) {
        return null;
    }
}
