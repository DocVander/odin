package com.newpixelcoffee.odin.internal.types;

import com.newpixelcoffee.odin.AnyType;
import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.adapters.OdinImmutableAdapter;
import com.newpixelcoffee.odin.adapters.OdinInlineAdapter;
import com.newpixelcoffee.odin.adapters.OdinObjectAdapter;
import com.newpixelcoffee.odin.annotations.OdinCustomAdapter;
import com.newpixelcoffee.odin.annotations.OdinField;
import com.newpixelcoffee.odin.annotations.OdinMapAsList;
import com.newpixelcoffee.odin.annotations.OdinReadTo;
import com.newpixelcoffee.odin.exceptions.OdinTypeException;
import com.newpixelcoffee.odin.internal.ReaderImpl;
import com.newpixelcoffee.odin.internal.WriterImpl;
import com.newpixelcoffee.odin.internal.collections.MapValuesAdapter;
import com.newpixelcoffee.odin.internal.processor.Processor;
import com.newpixelcoffee.odin.internal.processor.RuntimeProcessor;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public abstract class ObjectTypeImpl<T> extends AnyTypeImpl<T> implements ObjectType<T> {

    protected TypeManager manager;

    private Class<T> type;
    private String name;

    private ObjectConstructor<T> constructor;
    private ObjectTypeImpl<? super T> parent;
    private TypeField[] fields;

    protected Processor<T> processor;
    protected AnyType[] processorArguments = TypeManager.EMPTY_TYPE;
    // used when a object use custom adapter or map as list adapter, when true, sub instance type use a new class with this processor instead of using cache
    protected boolean forceProcessor;

    public ObjectTypeImpl(TypeManager manager, Class<T> type) {
        this.type = type;
        this.name = type.getName();
        this.manager = manager;
    }

    public ObjectTypeImpl(TypeManager manager, Class<T> type, String name) {
        this.type = type;
        this.name = name;
        this.manager = manager;
    }

    public abstract Processor<T> getClassProcessor();

    public abstract ClassTypeImpl registerAdapterType();

    protected abstract <E extends T> AnyTypeImpl<E> getInstanceType(E o);

    protected abstract <E extends T> AnyTypeImpl<E> readInstanceType(String name);

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Class<T> getType() {
        return type;
    }

    @Override
    public Type getGenericType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public <R extends T> T read(OdinReader reader, StreamReader stream, R o) throws IOException {
        if (stream.readNull())
            return null;
        ReaderImpl r = (ReaderImpl) reader;
        if (stream.eat('(')) {
            int index = stream.readInt();
            stream.eat(')');
            stream.eatEnd();
            return r.getRecursive(index);
        }
        if (stream.eat('<')) {
            String name = stream.readType();
            AnyTypeImpl<R> t = readInstanceType(name);
            return t.subRead(r, stream, o);
        }
        return getProcessor().read(r, stream, this, o);
    }

    @Override
    public <R extends T> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        WriterImpl w = (WriterImpl) writer;
        if (o == null)
            stream.writeNull();
        else if (w.isNotWriteRecursive(o)) {
            if (o.getClass() != type && !type.isEnum()) {
                AnyTypeImpl<R> t = getInstanceType(o);
                t.writeType(stream, true);
                t.subWrite(w, stream, o);
            } else
                getProcessor().write(w, stream, this, o);
        }
    }

    @Override
    protected <R extends T> T subRead(ReaderImpl reader, StreamReader stream, R o) throws IOException {
        return getProcessor().read(reader, stream, this, o);
    }

    @Override
    protected <R extends T> void subWrite(WriterImpl writer, StreamWriter stream, R o) throws IOException {
        getProcessor().write(writer, stream, this, o);
    }

    @Override
    public void writeType(StreamWriter stream, boolean notUnknown) throws IOException {
        stream.writeType(name);
    }

    @Override
    public boolean isObject() {
        return true;
    }

    @Override
    public T newInstance() {
        if (constructor == null)
            constructor = TypeManager.getConstructor(getType());
        return constructor.instance();
    }

    @Override
    public ObjectTypeImpl<? super T> getSuperClass() {
        if (parent == null) {
            Class<T> c = getType();
            if (c == Object.class || c.isInterface())
                return manager.UNKNOWN_TYPE;
            else
                parent = (ObjectTypeImpl<? super T>) manager.getType(c.getGenericSuperclass(), this);
        }
        return parent;
    }

    @Override
    public ObjectTypeImpl<? super T> getSuperClass(Class<? super T> target) {
        return getSuperClass(this, target);
    }

    @Override
    public AnyType getAdapterArgument(int index) {
        if (index < processorArguments.length)
            return processorArguments[index];
        return manager.UNKNOWN_TYPE;
    }

    @Override
    public AnyType[] getAdapterArguments() {
        return processorArguments;
    }

    @Override
    public <E> TypeField<E> findField(String name) {
        ObjectTypeImpl<? super T> p = this;
        while (p != null) {
            TypeField f = p.getField(name);
            if (f != null)
                return f;
            p = p.getSuperClass();
        }
        return null;
    }

    @Override
    public <E> TypeField<E> getField(String name) {
        if (fields == null)
            loadFields();
        for (TypeField field : fields) {
            if (field.name.equals(name))
                return field;
        }
        return null;
    }

    @Override
    public <E> TypeField<E> getField(String name, int index) {
        if (fields == null)
            loadFields();
        if (index < fields.length && fields[index].name.equals(name))
            return fields[index];
        for (TypeField field : fields) {
            if (field.name.equals(name))
                return field;
        }
        return null;
    }

    @Override
    public TypeField[] getFields() {
        if (fields == null)
            loadFields();
        return fields;
    }

    public Processor<T> getProcessor() {
        if (processor == null)
            findProcessor(this, 0, 0);
        return processor;
    }

    public void setProcessor(Processor<T> processor) {
        this.processor = processor;
    }

    private void loadFields() {
        Class<T> t = getType();
        if (t == Object.class || t.isInterface())
            fields = new TypeField[0];
        else {
            List<TypeField> list = new ArrayList<>();
            Field[] d = t.getDeclaredFields();
            for (Field f : d) {
                if (manager.fieldFilter.test(f))
                    list.add(readField(f));
            }
            fields = list.toArray(new TypeField[0]);
        }
    }

    private TypeField readField(Field field) {
        field.setAccessible(true);
        String name = field.getName();
        String[] comments = null;

        OdinField customField = field.getAnnotation(OdinField.class);
        OdinCustomAdapter custom = field.getAnnotation(OdinCustomAdapter.class);
        OdinMapAsList mapList = field.getAnnotation(OdinMapAsList.class);

        if (customField != null) {
            name = customField.name();
            if (customField.comments().length > 0)
                comments = customField.comments();
        }

        AnyTypeImpl<?> type;
        if (custom != null)
            type = manager.getCustomType(field.getGenericType(), this, TypeManager.getConstructor(custom.adapter()).instance());
        else if (mapList != null)
            type = manager.getCustomType(field.getGenericType(), this, new MapValuesAdapter(mapList.keyField()));
        else
            type = manager.getType(field.getGenericType(), this);
        return new TypeField<>(name, field, type, comments, field.isAnnotationPresent(OdinReadTo.class));
    }

    private int findProcessor(ObjectTypeImpl<? super T> generic, int depth, int actual) {
        Processor<? super T> p = generic.getClassProcessor();
        if (p != null) {
            processor = (Processor<T>) p;
            processorArguments = generic.getGenericArguments();
            return depth;
        } else if (actual == 0 || depth + 1 < actual) {
            Type[] interfaces = generic.getType().getGenericInterfaces();
            ObjectTypeImpl<? super T>[] interfacesTypes = new ObjectTypeImpl[interfaces.length];
            int iIndex = 0;
            for (Type i : interfaces) {
                ObjectTypeImpl<? super T> genericInterfaceType = (ObjectTypeImpl<? super T>) manager.getType(i, generic);
                interfacesTypes[iIndex++] = genericInterfaceType;
                // Inline and immutable has no object when read begin
                // That would mean create new instance to read another instance
                if (genericInterfaceType.getType() == OdinObjectAdapter.class) {
                    AnyType arg = genericInterfaceType.getGenericArgument(0);
                    if (arg instanceof ObjectType) {
                        processor = new RuntimeProcessor<>();
                        processorArguments = ((ObjectType) arg).getGenericArguments();
                        return depth;
                    } else
                        throw new OdinTypeException("OdinObjectAdapter can not be used with primitive or array parameter. class: " + generic.getType());
                } else if (genericInterfaceType.getType() == OdinImmutableAdapter.class)
                    System.err.println("OdinImmutableAdapter can not be used in the object itself, you need to register it in Type Manager, skip adapter in : " + generic.getType());
                else if (genericInterfaceType.getType() == OdinInlineAdapter.class)
                    System.err.println("OdinInlineAdapter can not be used in the object itself, you need to register it in Type Manager, skip adapter in : " + generic.getType());
            }
            ObjectTypeImpl<? super T> genericSuperType = generic.getSuperClass();
            int d = depth + 1;
            int a = actual;
            if (genericSuperType != null) {
                a = findProcessor(genericSuperType, d, actual);
                if (a == d)
                    return a;
            }
            for (ObjectTypeImpl<? super T> genericInterfaceType : interfacesTypes) {
                int r = findProcessor(genericInterfaceType, d, a);
                if (r == d)
                    return r;
                else if (r > 0 && r < a)
                    a = r;
            }
        }
        return 0;
    }

    private ObjectTypeImpl<? super T> getSuperClass(ObjectTypeImpl<? super T> generic, Class<? super T> target) {
        Class<? super T> c = generic.getType();
        if (c == target)
            return generic;
        else if (target.isInterface()) {
            Type[] interfaces = c.getGenericInterfaces();
            for (Type i : interfaces) {
                ObjectTypeImpl<? super T> genericInterfaceType = (ObjectTypeImpl<? super T>) manager.getType(i, generic);
                ObjectTypeImpl<? super T> r = getSuperClass(genericInterfaceType, target);
                if (r != null)
                    return r;
            }
        }
        return getSuperClass(generic.getSuperClass(), target);
    }
}
