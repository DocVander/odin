package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.ArrayTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class DoubleType extends AnyTypeImpl<Double> {

    @Override
    public Class<Double> getType() {
        return double.class;
    }

    @Override
    public Type getGenericType() {
        return double.class;
    }

    @Override
    public String getName() {
        return "double";
    }

    @Override
    public ArrayTypeImpl<Double[], Double> toArray() {
        if (array == null)
            array = (ArrayTypeImpl) new DoubleArrayType(this);
        return array;
    }

    @Override
    public <R extends Double> Double read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return stream.readDouble();
    }

    @Override
    public <R extends Double> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        stream.writeDouble(o);
    }
}
