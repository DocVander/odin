package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class IntObjectType extends AnyTypeImpl<Integer> {

    @Override
    public Class<Integer> getType() {
        return Integer.class;
    }

    @Override
    public Type getGenericType() {
        return Integer.class;
    }

    @Override
    public String getName() {
        return "Int";
    }

    @Override
    public <R extends Integer> Integer read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return stream.readNull() ? null : stream.readInt();
    }

    @Override
    public <R extends Integer> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        if (o == null)
            stream.writeNull();
        else
            stream.writeInt(o);
    }
}
