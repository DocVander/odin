package com.newpixelcoffee.odin.internal.types;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.ReaderImpl;
import com.newpixelcoffee.odin.internal.WriterImpl;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;

import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Type;

public class ArrayTypeImpl<T, E> extends AnyTypeImpl<T> {

    private Class<T> type;
    private AnyTypeImpl<E> content;
    private String name;

    public ArrayTypeImpl(AnyTypeImpl<E> content) {
        this.content = content;
        this.name = '['+content.getName();
        type = (Class<T>) Array.newInstance(content.getType(), 0).getClass();
    }

    public AnyTypeImpl getContent() {
        return content;
    }

    @Override
    public Class<T> getType() {
        return type;
    }

    @Override
    public Type getGenericType() {
        return type;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public <R extends T> T read(OdinReader reader, StreamReader stream, R o) throws IOException {
        if (stream.readNull())
            return null;
        ReaderImpl in = (ReaderImpl) reader;
        if (stream.eat('(')) {
            int index = in.readInt();
            stream.eat(')');
            stream.eatEnd();
            return in.getRecursive(index);
        }
        AnyTypeImpl<E> contentType = content;
        if (stream.eat('<')) {
            int dimensions = 0;
            while (stream.eat('['))
                dimensions++;
            String name = stream.readType();
            AnyTypeImpl t = ((ReaderImpl) reader).getTypeManager().getType(name);
            while (dimensions-- > 1)
                t = t.toArray();
            contentType = t;
        }
        int i = 0;
        if (stream.eat('[')) {
            int size = 8;
            Class<E> clazz = contentType.getType();
            T r = (T) Array.newInstance(clazz, size);
            OdinReader sub = in.openSubReader(null);

            while (!in.reachArrayEnd()) {
                E element = sub.readTyped(contentType);
                if (i == size) {
                    size += size >> 1;
                    T a = (T) Array.newInstance(clazz, size);
                    System.arraycopy(r, 0, a, 0, i);
                    r = a;
                }
                try {
                    Array.set(r, i, element);
                } catch (IllegalArgumentException e) {
                    if (element == null)
                        throw new IllegalArgumentException("Wrong array argument, read null but array of " + clazz);
                    else
                        throw new IllegalArgumentException("Wrong array argument, read " + element.getClass() + " but array of " + clazz);
                }
                i++;
            }
            stream.eatEnd();
            T a = (T) Array.newInstance(clazz, i);
            System.arraycopy(r, 0, a, 0, i);
            return a;
        } else
            throw stream.getUnwantedCharException("Unable to get array beginning.", '[');
    }

    @Override
    public <R extends T> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        WriterImpl w = (WriterImpl) writer;
        if (o == null)
            stream.writeNull();
        else if (w.isNotWriteRecursive(o)) {
            Class<R> c = (Class<R>) o.getClass();
            AnyTypeImpl<E> contentType = content;
            if (c != type) {
                ArrayTypeImpl t = w.getTypeManager().getType(c.getComponentType()).toArray();
                t.writeType(stream, false);
                contentType = t.content;
            }
            WriterImpl sub = w.openSubWriter('[', o);
            int size = Array.getLength(o);
            for (int i = 0; i < size; i++)
                sub.writeTyped((E) Array.get(o, i), contentType);
            sub.closeObject(']');
        }
    }

    @Override
    public void writeType(StreamWriter stream, boolean notUnknown) throws IOException {
        stream.writeType(name);
    }
}
