package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class BooleanObjectType extends AnyTypeImpl<Boolean> {

    @Override
    public Class<Boolean> getType() {
        return Boolean.class;
    }

    @Override
    public Type getGenericType() {
        return Boolean.class;
    }

    @Override
    public String getName() {
        return "Boolean";
    }

    @Override
    public <R extends Boolean> Boolean read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return stream.readNull() ? null : stream.readBoolean();
    }

    @Override
    public <R extends Boolean> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        if (o == null)
            stream.writeNull();
        else
            stream.writeBoolean(o);
    }
}
