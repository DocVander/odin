package com.newpixelcoffee.odin.internal.types;

interface ObjectConstructor<T> {
    T instance();
}
