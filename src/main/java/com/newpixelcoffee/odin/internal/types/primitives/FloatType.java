package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.ArrayTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class FloatType extends AnyTypeImpl<Float> {

    @Override
    public Class<Float> getType() {
        return float.class;
    }

    @Override
    public Type getGenericType() {
        return float.class;
    }

    @Override
    public String getName() {
        return "float";
    }

    @Override
    public ArrayTypeImpl<Float[], Float> toArray() {
        if (array == null)
            array = (ArrayTypeImpl) new FloatArrayType(this);
        return array;
    }

    @Override
    public <R extends Float> Float read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return stream.readFloat();
    }

    @Override
    public <R extends Float> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        stream.writeFloat(o);
    }
}
