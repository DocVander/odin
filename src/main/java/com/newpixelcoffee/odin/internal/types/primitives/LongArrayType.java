package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.ReaderImpl;
import com.newpixelcoffee.odin.internal.WriterImpl;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.ArrayTypeImpl;

import java.io.IOException;

/**
 * @author DocVander
 */
public class LongArrayType extends ArrayTypeImpl<long[], Long> {

    LongArrayType(AnyTypeImpl<Long> content) {
        super(content);
    }

    @Override
    public long[] read(OdinReader reader, StreamReader stream, long[] o) throws IOException {
        if (stream.readNull())
            return null;
        int i = 0;
        ReaderImpl in = (ReaderImpl) reader;
        if (stream.eat('[')) {
            int size = 8;
            long[] r = new long[size];
            OdinReader sub = in.openSubReader(null);
            while (!in.reachArrayEnd()) {
                long element = sub.readLong();
                if (i == size) {
                    size += size >> 1;
                    long[] a = new long[size];
                    System.arraycopy(r, 0, a, 0, i);
                    r = a;
                }
                r[i++] = element;
            }
            stream.eatEnd();
            long[] a = new long[i];
            System.arraycopy(r, 0, a, 0, i);
            return a;
        } else
            throw stream.getUnwantedCharException("Unable to get array beginning.", '[');
    }

    @Override
    public void write(OdinWriter writer, StreamWriter stream, long[] o) throws IOException {
        WriterImpl w = (WriterImpl) writer;
        if (o == null)
            stream.writeNull();
        else {
            WriterImpl sub = w.openSubWriter('[', o);
            for (long element : o) sub.writeLong(element);
            sub.closeObject(']');
        }
    }
}
