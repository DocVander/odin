package com.newpixelcoffee.odin.internal.types;

import com.newpixelcoffee.odin.AnyType;
import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.adapters.*;
import com.newpixelcoffee.odin.exceptions.OdinTypeException;
import com.newpixelcoffee.odin.internal.collections.ListAdapter;
import com.newpixelcoffee.odin.internal.collections.MapAdapter;
import com.newpixelcoffee.odin.internal.collections.SetAdapter;
import com.newpixelcoffee.odin.internal.defaults.*;
import com.newpixelcoffee.odin.internal.processor.ImmutableProcessor;
import com.newpixelcoffee.odin.internal.processor.InlineProcessor;
import com.newpixelcoffee.odin.internal.processor.ObjectProcessor;
import com.newpixelcoffee.odin.internal.processor.Processor;
import com.newpixelcoffee.odin.internal.types.primitives.*;

import java.lang.reflect.*;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class TypeManager {

    static final AnyTypeImpl[] EMPTY_TYPE = new AnyTypeImpl[0];

    private static Object unsafeObject;

    private static Method unsafeInstantiate;
    private static Method objectFieldOffset;
    private static Method putBoolean;
    private static Field overrideField;

    static {
        try {
            Class<?> unsafeClass = Class.forName("sun.misc.Unsafe");
            Field unsafe = unsafeClass.getDeclaredField("theUnsafe");
            unsafe.setAccessible(true);
            unsafeObject = unsafe.get(null);
            // call
            unsafeInstantiate = unsafeClass.getDeclaredMethod("allocateInstance", Class.class);
            objectFieldOffset = unsafeClass.getMethod("objectFieldOffset", Field.class);
            putBoolean = unsafeClass.getMethod("putBoolean", Object.class, long.class, boolean.class);
            overrideField = AccessibleObject.class.getDeclaredField("override");
        } catch (Exception e) {
            System.err.println("Unable to initialize Unsafe constructor. Odin will not be able to instantiate object without empty constructor");
        }
    }

    private final HashMap<Class<?>, AnyTypeImpl> classTypes = new HashMap<>();
    private final HashMap<String, AnyTypeImpl> namesTypes = new HashMap<>();

    public final UnknownTypeImpl UNKNOWN_TYPE = new UnknownTypeImpl(this);

    public final ListAdapter listAdapter = new ListAdapter();
    public final SetAdapter setAdapter = new SetAdapter();
    public final MapAdapter mapAdapter = new MapAdapter();

    public Predicate<Field> fieldFilter = (Field field) -> !(Modifier.isTransient(field.getModifiers()) || Modifier.isStatic(field.getModifiers()));

    public TypeManager() {
        register(UNKNOWN_TYPE);
        register(new BooleanObjectType());
        register(new BooleanType());
        register(new ByteObjectType());
        register(new ByteType());
        register(new CharObjectType());
        register(new CharType());
        register(new DoubleObjectType());
        register(new DoubleType());
        register(new FloatObjectType());
        register(new FloatType());
        register(new IntObjectType());
        register(new IntType());
        register(new LongObjectType());
        register(new LongType());
        register(new ShortObjectType());
        register(new ShortType());
        register(new StringType());

        // default
        register("enum", new DefaultEnumAdapter());
        register("class", new DefaultClassAdapter());
        register("date", new DefaultDateAdapter());
        register("local date", new DefaultLocalDateAdapter());
        register("local time", new DefaultLocalTimeAdapter());
        register("local datetime", new DefaultLocalDateTimeAdapter());
        register("zoned datetime", new DefaultZonedDateTimeAdapter());
        register("locale", new DefaultLocaleAdapter());
        register("uuid", new DefaultUuidAdapter());

        registerImplementation("array list", ArrayList.class, listAdapter);
        registerImplementation("linked list", LinkedList.class, listAdapter);
        registerImplementation("vector", Vector.class, listAdapter);

        registerImplementation("hash set", HashSet.class, setAdapter);
        registerImplementation("linked hash set", LinkedHashSet.class, setAdapter);
        registerImplementation("tree set", TreeSet.class, setAdapter);
        registerImplementation("enum set", EnumSet.class, setAdapter);

        registerImplementation("hash map", HashMap.class, mapAdapter);
        registerImplementation("linked hash map", LinkedHashMap.class, mapAdapter);
        registerImplementation("tree map", TreeMap.class, mapAdapter);
        registerImplementation("enum map", EnumMap.class, mapAdapter);
    }

    private Function<Class<?>, ClassTypeImpl> typeComputer = (Class<?> c) -> new ClassTypeImpl<>(this, c, c.getName());

    public <T> TypeManager name(String name, Class<T> type) {
        if (type.isArray())
            throw new OdinTypeException("Unable to register array class");
        AnyTypeImpl<T> t = classTypes.computeIfAbsent(type, typeComputer);
        if (t instanceof ObjectTypeImpl) {
            ((ObjectTypeImpl<T>) t).setName(name);
            namesTypes.put(name, t);
            return this;
        } else
            throw new OdinTypeException("Primitive type can not be changed");
    }

    public <T> TypeManager register(OdinAdapter<T> adapter) {
        ClassTypeImpl<T> type = getAdapterType(new ClassTypeImpl<>(this, adapter.getClass()));
        type.setProcessor(createProcessor(adapter));
        classTypes.put(type.getType(), type);
        namesTypes.put(type.getName(), type);
        return this;
    }

    public <T> TypeManager register(String name, OdinAdapter<T> adapter) {
        ClassTypeImpl<T> type = getAdapterType(new ClassTypeImpl<>(this, adapter.getClass()));
        type.setProcessor(createProcessor(adapter));
        type.setName(name);
        classTypes.put(type.getType(), type);
        namesTypes.put(name, type);
        return this;
    }

    public <T> TypeManager registerImplementation(String name, Class<T> target, OdinAdapter<?> adapter) {
        ClassTypeImpl<T> type = new ClassTypeImpl<>(this, target);
        type.setProcessor((Processor<T>) createProcessor(adapter));
        type.setName(name);
        classTypes.put(type.getType(), type);
        namesTypes.put(name, type);
        return this;
    }

    public DefaultAdapterOption getDefaultAdapterOption() {
        return (DefaultAdapterOption) ((ObjectProcessor) UNKNOWN_TYPE.getProcessor()).getAdapter();
    }

    public <T> Class<T> getClass(String name) {
        AnyTypeImpl<T> type = namesTypes.get(name);
        if (type != null)
            return type.getType();
        else {
            try {
                return (Class<T>) Class.forName(name);
            } catch (ClassNotFoundException e) {
                throw new OdinTypeException("Type not in classpath : " + name, e);
            }
        }
    }

    public <T> OdinAdapter<T> getAdapter(Type type) {
        AnyTypeImpl<T> t = getType(type);
        if (t instanceof ObjectTypeImpl)
            return ((ObjectTypeImpl<T>) t).getProcessor().getAdapter();
        else
            throw new OdinTypeException("Type refer to primitive or array and does not have adapter");
    }

    public <T> AnyTypeImpl<T> getType(String name) {
        AnyTypeImpl<T> type = namesTypes.get(name);
        if (type != null)
            return type;
        else {
            try {
                return classTypes.computeIfAbsent(Class.forName(name), typeComputer);
            } catch (ClassNotFoundException e) {
                throw new OdinTypeException("Type not in classpath : " + name, e);
            }
        }
    }

    public <T> AnyTypeImpl<T> getType(Type type) {
        if (type == Object.class)
            return (AnyTypeImpl<T>) UNKNOWN_TYPE;
        else if (type instanceof Class) {
            Class c = (Class) type;
            if (c.isArray())
                return (AnyTypeImpl<T>) getType(c.getComponentType()).toArray();
            else {
                Class s = c.getSuperclass();
                if (s != null && s.isEnum())
                    return classTypes.computeIfAbsent(c.getSuperclass(), typeComputer);
                return classTypes.computeIfAbsent(c, typeComputer);
            }
        } else if (type instanceof TypeVariable)
            return (AnyTypeImpl<T>) UNKNOWN_TYPE;
        else if (type instanceof WildcardType) {
            WildcardType t = (WildcardType) type;
            return getType(t.getUpperBounds()[0]);
        } else if (type instanceof GenericArrayType) {
            GenericArrayType t = (GenericArrayType) type;
            return (AnyTypeImpl<T>) getType(t.getGenericComponentType()).toArray();
        }
        Class<T> clazz = (Class<T>) getRawType(type, null);
        ClassTypeImpl<T> row = (ClassTypeImpl<T>) classTypes.get(clazz);
        return new GenericTypeImpl<>(this, clazz, row, type, null);
    }

    <T> AnyTypeImpl<T> getType(Type type, ObjectTypeImpl declaration) {
        if (type == Object.class)
            return (AnyTypeImpl<T>) UNKNOWN_TYPE;
        else if (type instanceof Class) {
            Class c = (Class) type;
            if (c.isArray())
                return (AnyTypeImpl<T>) getType(c.getComponentType(), declaration).toArray();
            else {
                Class s = c.getSuperclass();
                if (s != null && s.isEnum())
                    return classTypes.computeIfAbsent(c.getSuperclass(), typeComputer);
                return classTypes.computeIfAbsent(c, typeComputer);
            }
        } else if (type instanceof TypeVariable)
            return getType(declaration.getGenericArgument(typeIndexInDeclaration((TypeVariable) type)).getGenericType(), declaration);
        else if (type instanceof WildcardType) {
            WildcardType t = (WildcardType) type;
            return getType(t.getUpperBounds()[0], declaration);
        } else if (type instanceof GenericArrayType) {
            GenericArrayType t = (GenericArrayType) type;
            return (AnyTypeImpl<T>) getType(t.getGenericComponentType(), declaration).toArray();
        }
        Class<T> clazz = (Class<T>) getRawType(type, declaration);
        ClassTypeImpl<T> row = (ClassTypeImpl<T>) classTypes.get(clazz);
        return new GenericTypeImpl<>(this, clazz, row, type, declaration);
    }

    // CALLED BY UnknownType
    <T> AnyTypeImpl<T> getAnyInstanceType(Class<T> type) {
        if (type.isArray())
            return (AnyTypeImpl<T>) getType(type.getComponentType()).toArray();
        else {
            Class s = type.getSuperclass();
            if (s != null && s.isEnum())
                return classTypes.computeIfAbsent(s, typeComputer);
            return classTypes.computeIfAbsent(type, typeComputer);
        }
    }

    // CALLED BY ClassType, can only be extends of object ( exclus )
    <T> AnyTypeImpl<T> getInstanceType(Class<T> type) {
        return classTypes.computeIfAbsent(type, typeComputer);
    }

    // CALLED BY GenericType, can only be extends of object ( exclus )
    <T> AnyTypeImpl<T> getInstanceGenericType(String name, ObjectTypeImpl declaration, Type generic) {
        AnyTypeImpl<T> type = namesTypes.get(name);
        if (type instanceof ClassTypeImpl)
            return new GenericTypeImpl<>(this, type.getType(), type.getName(), (ClassTypeImpl<T>) type, generic, declaration);
        else if (type != null)
            return type;
        else {
            try {
                Class<T> c = (Class<T>) Class.forName(name);
                return new GenericTypeImpl<>(this, c, null, generic, declaration);
            } catch (ClassNotFoundException e) {
                throw new OdinTypeException("Type not in classpath : " + name, e);
            }
        }
    }

    // CALLED BY GenericType, can only be extends of object ( exclus )
    <T> AnyTypeImpl<T> getInstanceGenericType(Class<T> type, ObjectTypeImpl declaration, Type generic) {
        AnyTypeImpl<T> row = (AnyTypeImpl<T>) classTypes.get(type);
        if (row == null || row instanceof ClassTypeImpl)
            return new GenericTypeImpl<>(this, type, (ClassTypeImpl<T>) row, generic, declaration);
        else
            return row;
    }

    <T> AnyTypeImpl<T> getCustomGenericType(Class<T> type, ObjectTypeImpl declaration, Type generic) {
        AnyTypeImpl<T> row = classTypes.get(type);
        if (row == null) {
            GenericTypeImpl<T> o = new GenericTypeImpl<>(this, type, type.getName(), null, generic, declaration);
            o.forceProcessor = true;
            return o;
        } else if (row instanceof ClassTypeImpl) {
            GenericTypeImpl<T> o = new GenericTypeImpl<>(this, type, row.getName(), null, generic, declaration);
            o.forceProcessor = true;
            return o;
        } else
            return row;
    }

    <T> AnyTypeImpl<T> getCustomInstanceType(Class<T> type) {
        AnyTypeImpl<T> row = classTypes.get(type);
        if (row == null) {
            ClassTypeImpl<T> o = new ClassTypeImpl<>(this, type, type.getName());
            o.forceProcessor = true;
            return o;
        } else if (row instanceof ClassTypeImpl) {
            ClassTypeImpl<T> o = new ClassTypeImpl<>(this, type, row.getName());
            o.forceProcessor = true;
            return o;
        } else
            return row;
    }

    <T> ClassTypeImpl<T> registerAdapterType(Type type, ObjectTypeImpl declaration) {
        if (type instanceof Class) {
            Class<T> c = (Class<T>) type;
            if (c.isArray())
                throw new OdinTypeException("Unable to register adapter for array type");
            AnyType<T> existing = classTypes.get(c);
            if (existing == null || existing instanceof ObjectTypeImpl)
                return new ClassTypeImpl<>(this, c);
            else
                throw new OdinTypeException("Unable to register adapter for primitive type");
        } else if (type instanceof TypeVariable)
            return registerAdapterType(declaration.getGenericArgument(typeIndexInDeclaration((TypeVariable) type)).getGenericType(), declaration);
        else if (type instanceof WildcardType) {
            WildcardType t = (WildcardType) type;
            return registerAdapterType(t.getUpperBounds()[0], declaration);
        } else if (type instanceof GenericArrayType)
            throw new OdinTypeException("Unable to register adapter for array type");
        // Skip Parameterized type
        Class<T> clazz = (Class<T>) getRawType(type, declaration);
        return new ClassTypeImpl<>(this, clazz);
    }

    <T> ObjectTypeImpl<T> getCustomType(Type type, ObjectTypeImpl declaration, OdinAdapter adapter) {
        if (type instanceof Class) {
            Class<T> c = (Class<T>) type;
            if (c.isArray())
                throw new OdinTypeException("Unable to register adapter for array type");
            AnyType<T> existing = classTypes.get(c);
            if (existing == null || existing instanceof ObjectTypeImpl) {
                ObjectTypeImpl<T> o = new ClassTypeImpl<>(this, c, existing == null ? c.getName() : existing.getName());
                o.setProcessor(createProcessor(adapter));
                o.forceProcessor = true;
                return o;
            } else
                throw new OdinTypeException("Unable to register adapter for primitive type");
        } else if (type instanceof TypeVariable)
            return getCustomType(declaration.getGenericArgument(TypeManager.typeIndexInDeclaration((TypeVariable) type)).getGenericType(), declaration, adapter);
        else if (type instanceof WildcardType) {
            WildcardType t = (WildcardType) type;
            return getCustomType(t.getUpperBounds()[0], declaration, adapter);
        } else if (type instanceof GenericArrayType)
            throw new OdinTypeException("Unable to register adapter for array type");
        Class<T> clazz = (Class<T>) TypeManager.getRawType(type, declaration);
        ClassTypeImpl<T> row = (ClassTypeImpl<T>) classTypes.get(clazz);
        ObjectTypeImpl<T> o = new GenericTypeImpl<>(this, clazz, row == null ? clazz.getName() : row.getName(), row, type, declaration);
        o.setProcessor(createProcessor(adapter));
        o.processorArguments = o.getGenericArguments();
        o.forceProcessor = true;
        return o;
    }

    // <------------------------------------------------------------------------------------------------------------------------------------------------ HIDDEN

    private <T> void register(AnyTypeImpl<T> adapter) {
        classTypes.put(adapter.getType(), adapter);
        namesTypes.put(adapter.getName(), adapter);
    }

    private <T> ClassTypeImpl<T> getAdapterType(ObjectTypeImpl parent) {
        if (parent.getType() == OdinAdapter.class)
            return parent.registerAdapterType();
        Type[] interfaces = parent.getType().getGenericInterfaces();
        for (Type i : interfaces) {
            ObjectTypeImpl t = getAdapterType(i, parent);
            ClassTypeImpl<T> o = getAdapterType(t);
            if (o != null)
                return o;
        }
        ObjectTypeImpl superClass = parent.getSuperClass();
        if (superClass != null)
            return getAdapterType(superClass);
        else
            throw new OdinTypeException("Unable to find adapter implementation.");
    }

    private <T> ObjectTypeImpl<T> getAdapterType(Type type, ObjectTypeImpl declaration) {
        if (type instanceof Class)
            return new ClassTypeImpl<>(this, (Class<T>) type);
        else {
            Class<T> clazz = (Class<T>) ((ParameterizedType) type).getRawType();
            return new GenericTypeImpl<>(this, clazz, null, type, declaration);
        }
    }

    private <T> Processor<T> createProcessor(OdinAdapter<T> adapter) {
        if (adapter instanceof OdinObjectAdapter)
            return new ObjectProcessor<>((OdinObjectAdapter<T>) adapter);
        else if (adapter instanceof OdinImmutableAdapter)
            return new ImmutableProcessor<>((OdinImmutableAdapter<T>) adapter);
        else if (adapter instanceof OdinInlineAdapter) {
            Class c = adapter.getClass();
            while (c != Object.class) {
                for (Method m : c.getDeclaredMethods()) {
                    if (!m.isBridge() && "write".equals(m.getName()) && m.getParameterTypes().length == 2 && m.getParameterTypes()[0] == ObjectType.class)
                        return new InlineProcessor<>((OdinInlineAdapter<T, ?>) adapter, getType(m.getGenericReturnType(), null));
                }
                c = c.getSuperclass();
            }
            throw new OdinTypeException("Unable to find return type of the inline adapter.");
        } else
            throw new OdinTypeException("You can only register an Odin adapter.");
    }

    // <------------------------------------------------------------------------------------------------------------------------------------------------ STATIC

    static <T> ObjectConstructor<T> getConstructor(Class<T> type) {
        if (Modifier.isInterface(type.getModifiers()) || Modifier.isAbstract(type.getModifiers()))
            throw new OdinTypeException("Can not create instance for interface or abstract class.");
        try {
            Constructor<T> constructor = type.getDeclaredConstructor();
            if (!constructor.isAccessible()) {
                if (access(constructor))
                    return getDefaultConstructor(constructor);
                else
                    return getUnsafeConstructor(type);
            } else
                return getDefaultConstructor(constructor);
        } catch (Exception e) {
            return getUnsafeConstructor(type);
        }
    }

    private static <T> ObjectConstructor<T> getDefaultConstructor(Constructor<T> constructor) {
        return () -> {
            try {
                return constructor.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                throw new OdinTypeException("Unable to create class instance using default constructor.", e);
            } catch (InvocationTargetException e) {
                throw new OdinTypeException("Unable to create class instance using default constructor.", e.getTargetException());
            }
        };
    }

    private static <T> ObjectConstructor<T> getUnsafeConstructor(Class<T> type) {
        if (unsafeInstantiate != null) {
            return () -> {
                try {
                    return (T) unsafeInstantiate.invoke(unsafeObject, type);
                } catch (Exception e) {
                    throw new OdinTypeException("Unable to create class instance using Unsafe allocate.", e);
                }
            };
        }
        throw new OdinTypeException("Unable to get default class constructor and Unsafe allocate is not accessible on this jvm.");
    }

    private static boolean access(Constructor constructor) {
        try {
            constructor.setAccessible(true);
            return true;
        } catch (SecurityException e) {
            try {
                long overrideOffset = (long) objectFieldOffset.invoke(unsafeObject, overrideField);
                putBoolean.invoke(unsafeObject, constructor, overrideOffset, true);
                return true;
            } catch (Exception ignored) {
                return false;
            }
        }
    }

    private static Class<?> getRawType(Type type, ObjectTypeImpl parent) {
        if (type.getClass() == Class.class) {
            Class<?> c = (Class) type;
            if (c.isArray())
                return getRawType(c.getComponentType(), parent);
            else
                return c;
        }
        if (type instanceof ParameterizedType)
            return (Class<?>) ((ParameterizedType) type).getRawType();
        else if (type instanceof GenericArrayType)
            return getRawType(((GenericArrayType)type).getGenericComponentType(), parent);
        else if (type instanceof WildcardType)
            return getRawType(((WildcardType)type).getUpperBounds()[0], parent);
        else if (type instanceof TypeVariable) {
            if (parent != null)
                return parent.getGenericArgument(typeIndexInDeclaration((TypeVariable) type)).getType();
            else
                return Object.class;
        } else
            throw new OdinTypeException("Unable to get class from java type.");
    }

    private static int typeIndexInDeclaration(TypeVariable type) {
        TypeVariable[] classVars = type.getGenericDeclaration().getTypeParameters();
        int l = classVars.length;
        for (int i = 0; i < l; i++) {
            if (type == classVars[i])
                return i;
        }
        return -1;
    }
}
