package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.ArrayTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class IntType extends AnyTypeImpl<Integer> {

    @Override
    public Class<Integer> getType() {
        return int.class;
    }

    @Override
    public Type getGenericType() {
        return int.class;
    }

    @Override
    public String getName() {
        return "int";
    }

    @Override
    public ArrayTypeImpl<Integer[], Integer> toArray() {
        if (array == null)
            array = (ArrayTypeImpl) new IntArrayType(this);
        return array;
    }

    @Override
    public <R extends Integer> Integer read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return stream.readInt();
    }

    @Override
    public <R extends Integer> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        stream.writeInt(o);
    }
}
