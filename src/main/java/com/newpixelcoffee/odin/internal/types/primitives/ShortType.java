package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.ArrayTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class ShortType extends AnyTypeImpl<Short> {

    @Override
    public Class<Short> getType() {
        return short.class;
    }

    @Override
    public Type getGenericType() {
        return short.class;
    }

    @Override
    public String getName() {
        return "short";
    }

    @Override
    public ArrayTypeImpl<Short[], Short> toArray() {
        if (array == null)
            array = (ArrayTypeImpl) new ShortArrayType(this);
        return array;
    }

    @Override
    public <R extends Short> Short read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return (short) stream.readInt();
    }

    @Override
    public <R extends Short> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        stream.writeInt(o);
    }
}
