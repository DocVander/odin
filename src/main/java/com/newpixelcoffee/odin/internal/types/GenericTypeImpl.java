package com.newpixelcoffee.odin.internal.types;

import com.newpixelcoffee.odin.exceptions.OdinTypeException;
import com.newpixelcoffee.odin.internal.processor.Processor;

import java.lang.reflect.*;

public class GenericTypeImpl<T> extends ObjectTypeImpl<T> {

    private ObjectTypeImpl declaration;
    private AnyTypeImpl[] arguments;
    private Type generic;

    private ClassTypeImpl<T> row;

    GenericTypeImpl(TypeManager manager, Class<T> clazz, ClassTypeImpl<T> row, Type type, ObjectTypeImpl declaration) {
        super(manager, clazz);
        this.row = row;
        this.generic = type;
        this.declaration = declaration;
        if (row != null)
            setName(row.getName());
    }

    GenericTypeImpl(TypeManager manager, Class<T> clazz, String name, ClassTypeImpl<T> row, Type type, ObjectTypeImpl declaration) {
        super(manager, clazz, name);
        this.row = row;
        this.generic = type;
        this.declaration = declaration;
    }

    public Type getGenericType() {
        return generic;
    }

    @Override
    protected <E extends T> AnyTypeImpl<E> getInstanceType(E o) {
        Class<E> c = (Class<E>) o.getClass();
        if (forceProcessor) {
            AnyTypeImpl<E> r = manager.getCustomGenericType(c, declaration, generic);
            if (r instanceof GenericTypeImpl) {
                GenericTypeImpl<E> t = (GenericTypeImpl<E>) r;
                t.processor = (Processor<E>) processor;
                t.processorArguments = t.getSuperClass(getType()).getGenericArguments();
            }
            return r;
        } else
            return manager.getInstanceGenericType(c, declaration, generic);
    }

    @Override
    protected <E extends T> AnyTypeImpl<E> readInstanceType(String name) {
        if (forceProcessor) {
            AnyTypeImpl<E> r = manager.getCustomGenericType(manager.getClass(name), declaration, generic);
            if (r instanceof GenericTypeImpl) {
                GenericTypeImpl<E> t = (GenericTypeImpl<E>) r;
                t.processor = (Processor<E>) processor;
                t.processorArguments = t.getSuperClass(getType()).getGenericArguments();
            }
            return r;
        } else
            return manager.getInstanceGenericType(name, declaration, generic);
    }

    @Override
    public AnyTypeImpl getGenericArgument(int index) {
        if (arguments == null)
            loadArguments();
        if (index < arguments.length)
            return arguments[index];
        else
            return manager.UNKNOWN_TYPE;
    }

    @Override
    public AnyTypeImpl[] getGenericArguments() {
        if (arguments == null)
            loadArguments();
        return arguments;
    }

    @Override
    public Processor<T> getClassProcessor() {
        if (row != null)
            return row.processor;
        return processor;
    }

    @Override
    public ClassTypeImpl registerAdapterType() {
        if (generic instanceof ParameterizedType) {
            Type[] types = ((ParameterizedType) generic).getActualTypeArguments();
            return manager.registerAdapterType(types[0], this);
        }
        throw new OdinTypeException("Wrong Adapter parameter, specify generic parameter to class implementing OdinAdapter.");
    }

    private void loadArguments() {
        arguments = TypeManager.EMPTY_TYPE;
        if (generic instanceof ParameterizedType) {
            Type[] types = ((ParameterizedType) generic).getActualTypeArguments();
            AnyTypeImpl[] args = new AnyTypeImpl[types.length];
            for (int i = 0; i < args.length; i++)
                args[i] = manager.getType(types[i], declaration);
            this.arguments = args;
        }
    }
}
