package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class ShortObjectType extends AnyTypeImpl<Short> {

    @Override
    public Class<Short> getType() {
        return Short.class;
    }

    @Override
    public Type getGenericType() {
        return Short.class;
    }

    @Override
    public String getName() {
        return "Short";
    }

    @Override
    public <R extends Short> Short read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return stream.readNull() ? null : (short) stream.readInt();
    }

    @Override
    public <R extends Short> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        if (o == null)
            stream.writeNull();
        else
            stream.writeInt(o);
    }
}
