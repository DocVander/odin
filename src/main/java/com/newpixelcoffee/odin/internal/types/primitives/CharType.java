package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.ArrayTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class CharType extends AnyTypeImpl<Character> {

    @Override
    public Class<Character> getType() {
        return char.class;
    }

    @Override
    public Type getGenericType() {
        return char.class;
    }

    @Override
    public String getName() {
        return "char";
    }

    @Override
    public ArrayTypeImpl<Character[], Character> toArray() {
        if (array == null)
            array = (ArrayTypeImpl) new CharArrayType(this);
        return array;
    }

    @Override
    public <R extends Character> Character read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return stream.readChar();
    }

    @Override
    public <R extends Character> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        stream.writeChar(o);
    }
}
