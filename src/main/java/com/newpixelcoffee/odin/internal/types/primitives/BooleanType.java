package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.ArrayTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class BooleanType extends AnyTypeImpl<Boolean> {

    @Override
    public Class<Boolean> getType() {
        return boolean.class;
    }

    @Override
    public Type getGenericType() {
        return boolean.class;
    }

    @Override
    public String getName() {
        return "boolean";
    }

    @Override
    public ArrayTypeImpl<Boolean[], Boolean> toArray() {
        if (array == null)
            array = (ArrayTypeImpl) new BooleanArrayType(this);
        return array;
    }

    @Override
    public <R extends Boolean> Boolean read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return stream.readBoolean();
    }

    @Override
    public <R extends Boolean> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        stream.writeBoolean(o);
    }
}
