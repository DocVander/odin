package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.ArrayTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class ByteType extends AnyTypeImpl<Byte> {

    @Override
    public Class<Byte> getType() {
        return byte.class;
    }

    @Override
    public Type getGenericType() {
        return byte.class;
    }

    @Override
    public String getName() {
        return "byte";
    }

    @Override
    public ArrayTypeImpl<Byte[], Byte> toArray() {
        if (array == null)
            array = (ArrayTypeImpl) new ByteArrayType(this);
        return array;
    }

    @Override
    public <R extends Byte> Byte read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return (byte) stream.readInt();
    }

    @Override
    public <R extends Byte> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        stream.writeInt(o);
    }
}
