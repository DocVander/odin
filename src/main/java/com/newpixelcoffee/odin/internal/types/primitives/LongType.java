package com.newpixelcoffee.odin.internal.types.primitives;

import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.ArrayTypeImpl;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public class LongType extends AnyTypeImpl<Long> {

    @Override
    public Class<Long> getType() {
        return long.class;
    }

    @Override
    public Type getGenericType() {
        return long.class;
    }

    @Override
    public String getName() {
        return "long";
    }

    @Override
    public ArrayTypeImpl<Long[], Long> toArray() {
        if (array == null)
            array = (ArrayTypeImpl) new LongArrayType(this);
        return array;
    }

    @Override
    public <R extends Long> Long read(OdinReader reader, StreamReader stream, R o) throws IOException {
        return stream.readLong();
    }

    @Override
    public <R extends Long> void write(OdinWriter writer, StreamWriter stream, R o) throws IOException {
        stream.writeLong(o);
    }
}
