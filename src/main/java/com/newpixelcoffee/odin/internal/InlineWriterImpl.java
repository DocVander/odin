package com.newpixelcoffee.odin.internal;

import com.newpixelcoffee.odin.elements.OdinExtra;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.TypeManager;

import java.io.IOException;
import java.util.List;

public class InlineWriterImpl extends WriterImpl {

    private InlineWriterImpl children;

    InlineWriterImpl(WriterImpl parent, TypeManager types, StreamWriter stream) {
        super(parent, types, stream);
    }

    @Override
    public WriterImpl getSubWriter() {
        if (children != null) return children;
        return children = new InlineWriterImpl(this, types, stream);
    }

    @Override
    protected WriterImpl openObject() {
        stream.changeIndentation(null);
        return this;
    }

    @Override
    public void closeObject(char end) throws IOException {
        parent.content = null;
        parent.openObject();
        firstNode = true;
        firstRow = true;
        stream.feed(' ');
        stream.feed(end);
    }

    @Override
    public void compressSubObjects() {
        // do nothing
    }

    @Override
    public void indentSubObjects() {
        // do nothing
    }

    @Override
    public void writeComment(String comment) {
        // do nothing
    }

    @Override
    public void writeComment(String[] comments) {
        // do nothing
    }

    @Override
    public void writeComment(List<String> comments) {
        // do nothing
    }

    @Override
    protected void writeExtra(OdinExtra extra) {
        // do nothing
    }

    @Override
    protected void newEntry() throws IOException {
        if (!firstNode) {
            stream.feed(',');
            stream.feed(' ');
        } else
            stream.feed(' ');
        firstNode = false;
    }

    @Override
    protected void newEntry(String name) throws IOException {
        newEntry();
        stream.writeKey(name);
        stream.feed(' ');
        stream.feed('=');
        stream.feed(' ');
    }

    @Override
    protected void newRow() throws IOException {
        if (firstRow) {
            newEntry();
            firstRow = false;
        } else {
            stream.feed(' ');
            stream.feed(':');
            stream.feed(' ');
        }
        firstNode = false;
    }
}
