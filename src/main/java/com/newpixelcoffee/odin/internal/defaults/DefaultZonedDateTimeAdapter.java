package com.newpixelcoffee.odin.internal.defaults;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.adapters.OdinInlineAdapter;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

public class DefaultZonedDateTimeAdapter implements OdinInlineAdapter<ZonedDateTime, String>, LocalDateTimeAdapterParameter {

    private static DateTimeFormatter DEFAULT_FORMATTER = new DateTimeFormatterBuilder()
            .appendPattern("yyyy-MM-dd").optionalStart().appendLiteral(' ').appendPattern("HH:mm:ss").optionalEnd()
            .optionalStart().appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true).optionalEnd()
            .optionalStart().appendLiteral(" [").appendZoneOrOffsetId().appendLiteral(']').optionalEnd().toFormatter();

    /**
     * This formatter describe how {@link ZonedDateTime} was converted to String.
     */
    public DateTimeFormatter formatter = DEFAULT_FORMATTER;

    @Override
    public ZonedDateTime read(ObjectType<? extends ZonedDateTime> type, String o) {
        return ZonedDateTime.parse(o, formatter);
    }

    @Override
    public String write(ObjectType<? extends ZonedDateTime> type, ZonedDateTime o) {
        return o.format(formatter);
    }

    @Override
    public void changeParameter(DateTimeFormatter value) {
        formatter = value;
    }
}
