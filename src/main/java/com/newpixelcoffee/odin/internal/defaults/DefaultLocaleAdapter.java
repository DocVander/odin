package com.newpixelcoffee.odin.internal.defaults;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.adapters.OdinInlineAdapter;

import java.util.Locale;

public class DefaultLocaleAdapter implements OdinInlineAdapter<Locale, String> {

    @Override
    public Locale read(ObjectType<? extends Locale> type, String o) {
        return Locale.forLanguageTag(o);
    }

    @Override
    public String write(ObjectType<? extends Locale> type, Locale o) {
        return o.toLanguageTag();
    }
}
