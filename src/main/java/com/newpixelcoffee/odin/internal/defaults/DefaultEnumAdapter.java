package com.newpixelcoffee.odin.internal.defaults;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.adapters.OdinInlineAdapter;

public class DefaultEnumAdapter implements OdinInlineAdapter<Enum, String> {

    @Override
    public Enum read(ObjectType<? extends Enum> type, String o) {
        return Enum.valueOf(type.getType(), o);
    }

    @Override
    public String write(ObjectType<? extends Enum> type, Enum o) {
        return o.name();
    }
}
