package com.newpixelcoffee.odin.internal.defaults;

import java.time.format.DateTimeFormatter;

/**
 * @author DocVander
 */
public interface LocalDateTimeAdapterParameter {
    void changeParameter(DateTimeFormatter formatter);
}
