package com.newpixelcoffee.odin.internal.defaults;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.adapters.OdinInlineAdapter;

import java.util.UUID;

public class DefaultUuidAdapter implements OdinInlineAdapter<UUID, String> {

    @Override
    public UUID read(ObjectType<? extends UUID> type, String o) {
        return UUID.fromString(o);
    }

    @Override
    public String write(ObjectType<? extends UUID> type, UUID o) {
        return o.toString();
    }
}
