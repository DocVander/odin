package com.newpixelcoffee.odin.internal.defaults;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.OdinWriter;
import com.newpixelcoffee.odin.adapters.DefaultAdapterOption;
import com.newpixelcoffee.odin.adapters.OdinObjectAdapter;
import com.newpixelcoffee.odin.exceptions.*;
import com.newpixelcoffee.odin.internal.types.ObjectTypeImpl;
import com.newpixelcoffee.odin.internal.types.TypeField;

import java.io.IOException;

public class DefaultObjectAdapter implements OdinObjectAdapter<Object>, DefaultAdapterOption {

    private ExceptionStrategy fieldGetStrategy = ExceptionsStrategy.FORWARD_EXCEPTION;
    private ExceptionStrategy readerStrategy = ExceptionsStrategy.FORWARD_EXCEPTION;
    private ExceptionStrategy writerStrategy = ExceptionsStrategy.FORWARD_EXCEPTION;
    private boolean writeNull = true;

    @Override
    public void read(OdinReader reader, ObjectType<?> type, Object o) throws IOException {
        int i = 0;
        ObjectTypeImpl current = (ObjectTypeImpl) type;
        while (reader.hasNext()) {
            String fieldName = reader.readNextKey();
            TypeField f = current.getField(fieldName, i);
            if (f == null) {
                ObjectTypeImpl p = current;
                while (f == null && (p = p.getSuperClass()) != null) {
                    if ((f = p.getField(fieldName)) != null) {
                        current = p;
                        i = 0;
                    }
                }
                // upwards search
                if (f == null && current != type) {
                    p = (ObjectTypeImpl) type;
                    while (f == null && p != current) {
                        f = p.getField(fieldName);
                        p = p.getSuperClass();
                    }
                }
            }

            if (f != null) {
                Object value = null;
                if (f.isReadTo()) {
                    try {
                        value = f.getField().get(o);
                    } catch (IllegalAccessException e) {
                        fieldGetStrategy.processException(type.getName(), f.getName(), "Error while getting the readTo value of the field", e);
                    }
                }
                try {
                    value = reader.readTo(value, f.getType());
                    f.getField().set(o, value);
                } catch (IllegalAccessException e) {
                    readerStrategy.processException(type.getName(), f.getName(), "Error while setting value of the field", e);
                } catch (OdinFormatException e) {
                    reader.read();
                    readerStrategy.processException(type.getName(), f.getName(), "Error while reading object", e);
                } catch (RuntimeException e) {
                    readerStrategy.processException(type.getName(), f.getName(), "Error while reading object", e);
                }
                i++;
            } else
                reader.read();
        }
    }

    @Override
    public void write(OdinWriter writer, ObjectType<?> type, Object o) throws IOException {
        ObjectTypeImpl p = (ObjectTypeImpl) type;
        do {
            TypeField[] fields = p.getFields();
            for (TypeField f : fields) {
                Object value = null;
                try {
                    value = f.getField().get(o);
                } catch (IllegalAccessException e) {
                    fieldGetStrategy.processException(type.getName(), f.getName(), "Error while getting the value of the field", e);
                }
                try {
                    if (writeNull || value != null) {
                        if (f.getComments() != null)
                            writer.writeComment(f.getComments());
                        writer.writeField(f.getName()).withTyped(value, f.getType());
                    }
                } catch (RuntimeException e) {
                    writerStrategy.processException(type.getName(), f.getName(), "Error while writing object", e);
                }
            }
        } while ((p = p.getSuperClass()) != null);
    }

    @Override
    public ExceptionStrategy getFieldGetExceptionStrategy() {
        return fieldGetStrategy;
    }

    @Override
    public void setFieldGetExceptionStrategy(ExceptionStrategy strategy) {
        this.fieldGetStrategy = strategy;
    }

    @Override
    public ExceptionStrategy getReaderExceptionStrategy() {
        return readerStrategy;
    }

    @Override
    public void setReaderExceptionStrategy(ExceptionStrategy strategy) {
        this.readerStrategy = strategy;
    }

    @Override
    public ExceptionStrategy getWriterExceptionStrategy() {
        return writerStrategy;
    }

    @Override
    public void setWriterExceptionStrategy(ExceptionStrategy strategy) {
        this.writerStrategy = strategy;
    }

    @Override
    public boolean isSkippingNull() {
        return !writeNull;
    }

    @Override
    public void setSkippingNull(boolean writeNull) {
        this.writeNull = !writeNull;
    }
}
