package com.newpixelcoffee.odin.internal.defaults;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.adapters.OdinInlineAdapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DefaultDateAdapter implements OdinInlineAdapter<Date, String> {

    public DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public Date read(ObjectType<? extends Date> type, String o) {
        try {
            return dateFormat.parse(o);
        } catch (ParseException e) {
            return null;
        }
    }

    @Override
    public String write(ObjectType<? extends Date> type, Date o) {
        return dateFormat.format(o);
    }
}
