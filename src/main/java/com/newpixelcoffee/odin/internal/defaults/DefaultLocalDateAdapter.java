package com.newpixelcoffee.odin.internal.defaults;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.adapters.OdinInlineAdapter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

public class DefaultLocalDateAdapter implements OdinInlineAdapter<LocalDate, String>, LocalDateTimeAdapterParameter {

    private static DateTimeFormatter DEFAULT_FORMATTER = new DateTimeFormatterBuilder()
            .appendPattern("yyyy-MM-dd").toFormatter();

    /** New Java 8 {@link LocalDate} format. */
    public DateTimeFormatter formatter = DEFAULT_FORMATTER;

    @Override
    public LocalDate read(ObjectType<? extends LocalDate> type, String o) {
        return LocalDate.parse(o, formatter);
    }

    @Override
    public String write(ObjectType<? extends LocalDate> type, LocalDate o) {
        return o.format(formatter);
    }

    @Override
    public void changeParameter(DateTimeFormatter value) {
        formatter = value;
    }
}
