package com.newpixelcoffee.odin.internal.defaults;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.adapters.OdinInlineAdapter;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

public class DefaultLocalDateTimeAdapter implements OdinInlineAdapter<LocalDateTime, String>, LocalDateTimeAdapterParameter {

    private static DateTimeFormatter DEFAULT_FORMATTER = new DateTimeFormatterBuilder()
            .appendPattern("yyyy-MM-dd").optionalStart().appendLiteral(' ').appendPattern("HH:mm:ss").optionalEnd()
            .optionalStart().appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true).optionalEnd().toFormatter();

    /** New Java 8 {@link LocalDateTime} format. */
    public DateTimeFormatter formatter = DEFAULT_FORMATTER;

    @Override
    public LocalDateTime read(ObjectType<? extends LocalDateTime> type, String o) {
        return LocalDateTime.parse(o, formatter);
    }

    @Override
    public String write(ObjectType<? extends LocalDateTime> type, LocalDateTime o) {
        return o.format(formatter);
    }

    @Override
    public void changeParameter(DateTimeFormatter value) {
        formatter = value;
    }
}
