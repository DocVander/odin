package com.newpixelcoffee.odin.internal.defaults;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.adapters.OdinInlineAdapter;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

public class DefaultLocalTimeAdapter implements OdinInlineAdapter<LocalTime, String>, LocalDateTimeAdapterParameter {

    private static DateTimeFormatter DEFAULT_FORMATTER = new DateTimeFormatterBuilder()
            .appendPattern("HH:mm:ss")
            .optionalStart().appendFraction(ChronoField.NANO_OF_SECOND, 0, 9, true).optionalEnd().toFormatter();

    /** New Java 8 {@link LocalTime} format. */
    public DateTimeFormatter formatter = DEFAULT_FORMATTER;

    @Override
    public LocalTime read(ObjectType<? extends LocalTime> type, String o) {
        return LocalTime.parse(o, formatter);
    }

    @Override
    public String write(ObjectType<? extends LocalTime> type, LocalTime o) {
        return o.format(formatter);
    }

    @Override
    public void changeParameter(DateTimeFormatter value) {
        formatter = value;
    }
}
