package com.newpixelcoffee.odin.internal.defaults;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.adapters.OdinInlineAdapter;

public class DefaultClassAdapter implements OdinInlineAdapter<Class, String> {

    @Override
    public Class read(ObjectType<? extends Class> type, String o) {
        try {
            return Class.forName(o);
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    @Override
    public String write(ObjectType<? extends Class> type, Class o) {
        return o.getName();
    }
}
