package com.newpixelcoffee.odin.internal;

import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.elements.OdinExtra;
import com.newpixelcoffee.odin.internal.types.TypeManager;

import java.io.IOException;
import java.util.List;

public class IndentWriterImpl extends WriterImpl {

    private IndentWriterImpl children;
    private String indentation;
    private boolean hasContents;

    private InlineWriterImpl inlineChildren;
    private boolean useInline;

    public IndentWriterImpl(TypeManager types, StreamWriter stream) {
        super(null, types, stream);
        this.indentation = "";
        stream.changeIndentation(indentation);
    }

    private IndentWriterImpl(WriterImpl parent, TypeManager types, StreamWriter stream, String indentation) {
        super(parent, types, stream);
        this.indentation = indentation;
        hasContents = true;
    }

    @Override
    public WriterImpl getSubWriter() {
        if (useInline) {
            if (inlineChildren != null) return inlineChildren;
            else return inlineChildren = new InlineWriterImpl(this, types, stream);
        } else {
            if (children != null) return children;
            return children = new IndentWriterImpl(this, types, stream, indentation + "\t");
        }
    }

    @Override
    protected WriterImpl openObject() {
        stream.changeIndentation(indentation);
        return this;
    }

    @Override
    public void closeObject(char end) throws IOException {
        parent.content = null;
        parent.openObject();
        firstRow = true;
        newEntry();
        stream.feed(end);
        useInline = false;
    }

    @Override
    public void compressSubObjects() {
        useInline = true;
    }

    @Override
    public void indentSubObjects() {
        useInline = false;
    }

    @Override
    public void writeComment(String comment) throws IOException {
        newEntry();
        if (!comment.isEmpty()) {
            stream.feed('#');
            stream.feed(' ');
            stream.writeComment(comment);
        }
    }

    @Override
    public void writeComment(String[] comments) throws IOException {
        for (String s : comments) {
            newEntry();
            if (!s.isEmpty()) {
                stream.feed('#');
                stream.feed(' ');
                stream.writeComment(s);
            }
        }
    }

    @Override
    public void writeComment(List<String> comments) throws IOException {
        for (String s : comments) {
            newEntry();
            if (!s.isEmpty()) {
                stream.feed('#');
                stream.feed(' ');
                stream.writeComment(s);
            }
        }
    }

    @Override
    protected void writeExtra(OdinExtra extra) throws IOException {
        if (extra != null)
            writeComment(extra.getComments());
    }

    @Override
    protected void newEntry() throws IOException {
        if (hasContents)
            stream.feedNewLine();
        else
            hasContents = true;
    }

    @Override
    protected void newEntry(String name) throws IOException {
        newEntry();
        stream.writeKey(name);
        stream.feed(' ');
        stream.feed('=');
        stream.feed(' ');
    }

    @Override
    protected void newRow() throws IOException {
        if (firstRow) {
            newEntry();
            firstRow = false;
        } else {
            stream.feed(' ');
            stream.feed(':');
            stream.feed(' ');
        }
    }
}
