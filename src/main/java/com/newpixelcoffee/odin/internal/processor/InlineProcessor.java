package com.newpixelcoffee.odin.internal.processor;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.adapters.OdinAdapter;
import com.newpixelcoffee.odin.adapters.OdinInlineAdapter;
import com.newpixelcoffee.odin.exceptions.OdinAdapterException;
import com.newpixelcoffee.odin.internal.ReaderImpl;
import com.newpixelcoffee.odin.internal.WriterImpl;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;

import java.io.IOException;

public class InlineProcessor<T, R> implements Processor<T> {

    private OdinInlineAdapter<T, R> adapter;
    private AnyTypeImpl<R> resultType;

    public InlineProcessor(OdinInlineAdapter<T, R> adapter, AnyTypeImpl<R> resultType) {
        this.adapter = adapter;
        this.resultType = resultType;
    }

    @Override
    public T read(ReaderImpl reader, StreamReader stream, ObjectType<T> type, T o) throws IOException {
        try {
            R result = resultType.read(reader, stream, null);
            return adapter.read(type, result);
        } catch (RuntimeException e) {
            if (e instanceof OdinAdapterException && ((OdinAdapterException) e).isThrownByAdapter(type.getName()))
                throw e;
            else
                throw new OdinAdapterException(type.getName(), null, "Error while reading object", e);
        }
    }

    @Override
    public void write(WriterImpl writer, StreamWriter stream, ObjectType<T> type, T o) throws IOException {
        try {
            R result = adapter.write(type, o);
            resultType.write(writer, stream, result);
        } catch (RuntimeException e) {
            if (e instanceof OdinAdapterException && ((OdinAdapterException) e).isThrownByAdapter(type.getName()))
                throw e;
            else
                throw new OdinAdapterException(type.getName(), null, "Error while writing object", e);
        }
    }

    @Override
    public OdinAdapter<T> getAdapter() {
        return adapter;
    }
}
