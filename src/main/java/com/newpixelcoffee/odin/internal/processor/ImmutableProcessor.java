package com.newpixelcoffee.odin.internal.processor;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.adapters.OdinAdapter;
import com.newpixelcoffee.odin.adapters.OdinImmutableAdapter;
import com.newpixelcoffee.odin.exceptions.OdinAdapterException;
import com.newpixelcoffee.odin.internal.ReaderImpl;
import com.newpixelcoffee.odin.internal.WriterImpl;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;

import java.io.IOException;

public class ImmutableProcessor<T> implements Processor<T> {

    private OdinImmutableAdapter<T> adapter;

    public ImmutableProcessor(OdinImmutableAdapter<T> adapter) {
        this.adapter = adapter;
    }

    @Override
    public T read(ReaderImpl reader, StreamReader stream, ObjectType<T> type, T o) throws IOException {
        if (stream.eat('{')) {
            ReaderImpl sub = reader.openSubReader(null);
            try {
                T result = adapter.read(sub, type);
                sub.readEntries(type);
                if (stream.eat('}')) {
                    stream.eatEnd();
                    return result;
                }
            } catch (RuntimeException e) {
                sub.readEntries(type);
                if (stream.eat('}'))
                    stream.eatEnd();
                if (e instanceof OdinAdapterException && ((OdinAdapterException) e).isThrownByAdapter(type.getName()))
                    throw e;
                else
                    throw new OdinAdapterException(type.getName(), null, "Error while reading object", e);
            }
            throw stream.getUnwantedCharException("Unable to get object ending.", '}');
        } else
            throw stream.getUnwantedCharException("Unable to get object beginning.", '{');
    }

    @Override
    public void write(WriterImpl writer, StreamWriter stream, ObjectType<T> type, T o) throws IOException {
        WriterImpl sub = writer.openSubWriter('{', o);
        try {
            adapter.write(sub, type, o);
        } catch (RuntimeException e) {
            if (e instanceof OdinAdapterException && ((OdinAdapterException) e).isThrownByAdapter(type.getName()))
                throw e;
            else
                throw new OdinAdapterException(type.getName(), null, "Error while writing object", e);
        } finally {
            sub.closeObject('}');
        }
    }

    @Override
    public OdinAdapter<T> getAdapter() {
        return adapter;
    }
}
