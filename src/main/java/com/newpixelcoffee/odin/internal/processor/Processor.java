package com.newpixelcoffee.odin.internal.processor;

import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.adapters.OdinAdapter;
import com.newpixelcoffee.odin.internal.ReaderImpl;
import com.newpixelcoffee.odin.internal.WriterImpl;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;

import java.io.IOException;

public interface Processor<T> {

    T read(ReaderImpl reader, StreamReader stream, ObjectType<T> type, T o) throws IOException;

    void write(WriterImpl writer, StreamWriter stream, ObjectType<T> type, T o) throws IOException;

    OdinAdapter<T> getAdapter();
}
