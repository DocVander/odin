package com.newpixelcoffee.odin.internal;

import com.newpixelcoffee.odin.elements.OdinExtra;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.TypeManager;

import java.io.IOException;
import java.util.List;

public class ShortWriterImpl extends WriterImpl {

    private ShortWriterImpl children;
    private boolean hasContents;

    public ShortWriterImpl(TypeManager types, StreamWriter stream) {
        super(null, types, stream);
    }

    ShortWriterImpl(WriterImpl parent, TypeManager types, StreamWriter stream, boolean hasContents) {
        super(parent, types, stream);
        this.hasContents = hasContents;
    }

    @Override
    public WriterImpl getSubWriter() {
        if (children != null) return children;
        return children = new ShortWriterImpl(this, types, stream, true);
    }

    @Override
    protected WriterImpl openObject() {
        stream.changeIndentation(null);
        return this;
    }

    @Override
    public void closeObject(char end) throws IOException {
        parent.content = null;
        parent.openObject();
        firstNode = true;
        firstRow = true;
        stream.feed(end);
    }

    @Override
    public void compressSubObjects() {
        // do nothing
    }

    @Override
    public void indentSubObjects() {
        // do nothing
    }

    @Override
    public void writeComment(String comment) {
        // do nothing
    }

    @Override
    public void writeComment(String[] comments) {
        // do nothing
    }

    @Override
    public void writeComment(List<String> comments) {
        // do nothing
    }

    @Override
    protected void writeExtra(OdinExtra extra) {
        // do nothing
    }

    @Override
    protected void newEntry() throws IOException {
        if (hasContents) {
            if (!firstNode)
                stream.feed(',');
        } else
            hasContents = true;
        firstNode = false;
    }

    @Override
    protected void newEntry(String name) throws IOException {
        newEntry();
        stream.writeKey(name);
        stream.feed('=');
    }

    @Override
    protected void newRow() throws IOException {
        if (firstRow) {
            newEntry();
            firstRow = false;
        } else
            stream.feed(':');
        firstNode = false;
    }
}
