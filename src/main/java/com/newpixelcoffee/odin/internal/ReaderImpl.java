package com.newpixelcoffee.odin.internal;

import com.newpixelcoffee.odin.AnyType;
import com.newpixelcoffee.odin.ObjectType;
import com.newpixelcoffee.odin.OdinReader;
import com.newpixelcoffee.odin.access.IOConsumer;
import com.newpixelcoffee.odin.access.OdinFieldReader;
import com.newpixelcoffee.odin.access.OdinRowReader;
import com.newpixelcoffee.odin.elements.*;
import com.newpixelcoffee.odin.exceptions.OdinAdapterException;
import com.newpixelcoffee.odin.exceptions.OdinFormatException;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.types.AnyTypeImpl;
import com.newpixelcoffee.odin.internal.types.TypeManager;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class ReaderImpl implements OdinReader, OdinFieldReader, OdinRowReader {

    private StreamReader stream;
    private TypeManager types;

    private ReaderImpl parent;
    private ReaderImpl children;
    private Object content;

    private Map<String, ReaderEntry> entries;
    private ReaderEntry entry;

    private boolean firstRow;

    public ReaderImpl(TypeManager types, StreamReader stream) {
        this.types = types;
        this.stream = stream;
    }

    private ReaderImpl(ReaderImpl parent, TypeManager types, StreamReader stream) {
        this.parent = parent;
        this.types = types;
        this.stream = stream;
    }

    @Override
    public void close() throws IOException {
        if (entries != null && !entries.isEmpty())
            readEntries(null);
        stream.close();
    }

    public ReaderImpl openSubReader(Object object) {
        content = object;
        return getSubReader();
    }

    private ReaderImpl getSubReader() {
        if (children != null) return children;
        else return children = new ReaderImpl(this, types, stream);
    }

    public void readEntries(ObjectType<?> type) throws IOException {
        if (entries != null) {
            while (!entries.isEmpty() && hasNext()) {
                String name = readNextKey();
                ReaderEntry<Object> entry = entries.remove(name);
                if (entry != null) {
                    try {
                        readExtra();
                        entry.reader.read(this, stream);
                    } catch (RuntimeException e) {
                        throw new OdinAdapterException((type == null ? "root" : type.getName()), name, "Error while reading entry", e);
                    }
                } else {
                    try {
                        types.UNKNOWN_TYPE.read(this, stream, null);
                    } catch (RuntimeException ignored) {
                        // ignore skipped value exceptions
                    }
                }
            }
            if (!entries.isEmpty()) {
                for (ReaderEntry e : entries.values())
                    e.notFound();
                entries.clear();
            }
        }
        while (hasNext()) {
            try {
                types.UNKNOWN_TYPE.read(this, stream, null);
            } catch (RuntimeException ignored) {
                // ignore skipped value exceptions
            }
        }
    }

    public boolean reachArrayEnd() throws IOException {
        readExtra();
        return stream.eat(']');
    }

    public <T> T getRecursive(int depth) {
        if (depth == -1)
            return (T) content;
        else if (parent != null)
            return parent.getRecursive(depth-1);
        else
            return null;
    }

    public TypeManager getTypeManager() {
        return types;
    }

    // ----------------------------------------------------------------------------------------------------------------------------------------------- ELEMENTS

    @Override
    public OdinNode readNode() throws IOException {
        OdinExtra extra = readExtra();
        if (stream.eat('{')) {
            OdinNode node = new OdinNode();
            node.setExtra(extra);
            openSubReader(node).readNodeContent(node);
            return node;
        } else
            throw stream.getUnwantedCharException("Invalid odin node beginning.", '{');
    }

    private void readNodeContent(OdinNode node) throws IOException {
        stream.eatEnd();
        OdinExtra extra = readExtra();
        while (!stream.eat('}')) {
            String name = stream.readKey();
            OdinElement element = readElement();
            element.setExtra(extra);
            node.addElement(name, element);
            extra = readExtra();
        }
        stream.eatEnd();
        node.setContentExtra(extra);
    }

    @Override
    public OdinArray readArray() throws IOException {
        OdinExtra extra = readExtra();
        if (stream.eat('[')) {
            OdinArray array = new OdinArray();
            array.setExtra(extra);
            openSubReader(array).readArrayContent(array);
            return array;
        } else
            throw stream.getUnwantedCharException("Invalid odin array beginning.", '[');
    }

    private void readArrayContent(OdinArray array) throws IOException {
        stream.eatEnd();
        OdinExtra extra = readExtra();
        while (!stream.eat(']')) {
            OdinElement element = readElement();
            element.setExtra(extra);
            array.addElement(element);
            extra = readExtra();
        }
        stream.eatEnd();
        array.setContentExtra(extra);
    }

    @Override
    public <T> OdinObject<T> readObject() throws IOException {
        OdinExtra extra = readExtra();
        OdinObject<T> object = new OdinObject<>((T) types.UNKNOWN_TYPE.read(this, stream, null));
        object.setExtra(extra);
        return object;
    }

    private OdinElement readElement() throws IOException {
        if (stream.eat('{')) {
            OdinNode node = new OdinNode();
            openSubReader(node).readNodeContent(node);
            return node;
        } else if (stream.eat('[')) {
            OdinArray array = new OdinArray();
            openSubReader(array).readArrayContent(array);
            return array;
        } else
            return new OdinObject<>(types.UNKNOWN_TYPE.read(this, stream, null));
    }

    private OdinExtra readExtra() throws IOException {
        boolean n;
        if ((n = stream.eatCrLf()) || stream.next(false) == '#') {
            OdinExtra e = new OdinExtra();
            do {
                if (n)
                    e.addSpace();
                else {
                    stream.eat('#');
                    e.addComment(stream.readComment());
                }
            } while ((n = stream.eatCrLf()) || stream.next(false) == '#');
            return e;
        }
        return null;
    }

    // ------------------------------------------------------------------------------------------------------------------------------------------------- VALUES

    @Override
    public boolean hasNext() throws IOException {
        readExtra();
        char c = stream.next(false);
        return c != '}' && c != '\u0000';
    }

    @Override
    public String readKey() throws IOException {
        readExtra();
        return stream.readKey();
    }

    @Override
    public String readNextKey() throws IOException {
        while (hasNext()) {
            try {
                return stream.readKey();
            } catch (OdinFormatException ignored) {
                // row separator is not read after key unlike other separator
                stream.eat(':');
            }
        }
        return null;
    }

    @Override
    public boolean readBoolean() throws IOException {
        readExtra();
        return stream.readBoolean();
    }

    @Override
    public byte readByte() throws IOException {
        readExtra();
        return (byte) stream.readInt();
    }

    @Override
    public short readShort() throws IOException {
        readExtra();
        return (short) stream.readInt();
    }

    @Override
    public int readInt() throws IOException {
        readExtra();
        return stream.readInt();
    }

    @Override
    public long readLong() throws IOException {
        readExtra();
        return stream.readLong();
    }

    @Override
    public float readFloat() throws IOException {
        readExtra();
        return stream.readFloat();
    }

    @Override
    public double readDouble() throws IOException {
        readExtra();
        return stream.readDouble();
    }

    @Override
    public char readChar() throws IOException {
        readExtra();
        return stream.readChar();
    }

    @Override
    public String readString() throws IOException {
        readExtra();
        return stream.readString();
    }

    @Override
    public <T> T read() throws IOException {
        readExtra();
        return (T) types.UNKNOWN_TYPE.read(this, stream, null);
    }

    @Override
    public <T> T readTo(T o) throws IOException {
        readExtra();
        if (o == null)
            return (T) types.UNKNOWN_TYPE.read(this, stream, null);
        else {
            readExtra();
            AnyTypeImpl<T> t = types.getType(o.getClass());
            if (stream.eat('<'))
                stream.readType();
            return t.read(this, stream, o);
        }
    }

    @Override
    public <T> T readTo(T o, AnyType<T> type) throws IOException {
        readExtra();
        return ((AnyTypeImpl<T>) type).read(this, stream, o);
    }

    @Override
    public <T> T readTo(T o, Type type) throws IOException {
        readExtra();
        return (T) types.getType(type).read(this, stream, o);
    }

    @Override
    public <T> T readTyped(AnyType<T> type) throws IOException {
        readExtra();
        return ((AnyTypeImpl<T>) type).read(this, stream, null);
    }

    @Override
    public <T> T readTyped(Type type) throws IOException {
        readExtra();
        return (T) types.getType(type).read(this, stream, null);
    }

    @Override
    public void readSubContent(IOConsumer<OdinReader> subReader) throws IOException {
        readExtra();
        if (stream.eat('{')) {
            ReaderImpl sub = openSubReader(null);
            subReader.accept(sub);
            sub.readEntries(null);
            if (stream.eat('}'))
                stream.eatEnd();
            else
                throw stream.getUnwantedCharException("Unable to get object ending.", '}');
        } else
            throw stream.getUnwantedCharException("Unable to get object beginning.", '{');
    }

    @Override
    public OdinFieldReader readField(String name) {
        entry = new ReaderEntry();
        if (entries == null)
            entries = new HashMap<>();
        entries.put(name, entry);
        return this;
    }

    @Override
    public OdinRowReader readRow() throws IOException {
        readExtra();
        firstRow = true;
        return this;
    }

    // -------------------------------------------------------------------------------------------------------------------------------------------------- FIELD

    @Override
    public OdinFieldReader listenNotFound(Runnable listener) {
        entry.notFound = listener;
        return this;
    }

    @Override
    public void listenBoolean(Consumer<Boolean> listener) {
        entry.define(listener, types.getType(Boolean.TYPE));
    }

    @Override
    public void listenByte(Consumer<Byte> listener) {
        entry.define(listener, types.getType(Byte.TYPE));
    }

    @Override
    public void listenShort(Consumer<Short> listener) {
        entry.define(listener, types.getType(Short.TYPE));
    }

    @Override
    public void listenInt(Consumer<Integer> listener) {
        entry.define(listener, types.getType(Integer.TYPE));
    }

    @Override
    public void listenLong(Consumer<Long> listener) {
        entry.define(listener, types.getType(Long.TYPE));
    }

    @Override
    public void listenFloat(Consumer<Float> listener) {
        entry.define(listener, types.getType(Float.TYPE));
    }

    @Override
    public void listenDouble(Consumer<Double> listener) {
        entry.define(listener, types.getType(Double.TYPE));
    }

    @Override
    public void listenChar(Consumer<Character> listener) {
        entry.define(listener, types.getType(Character.TYPE));
    }

    @Override
    public void listenString(Consumer<String> listener) {
        entry.define(listener, types.getType(String.class));
    }

    @Override
    public <T> void listen(Consumer<T> listener) {
        entry.define(listener, types.UNKNOWN_TYPE);
    }

    @Override
    public <T> void listenTo(T o) {
        entry.define(null, o == null ? types.UNKNOWN_TYPE : types.getType(o.getClass()), o);
    }

    @Override
    public <T> void listenTo(T o, Consumer<T> listener) {
        entry.define(listener, types.UNKNOWN_TYPE, o);
    }

    @Override
    public <T> void listenTo(T o, AnyType<T> type, Consumer<T> listener) {
        entry.define(listener, type, o);
    }

    @Override
    public <T> void listenTo(T o, Type type, Consumer<T> listener) {
        entry.define(listener, types.getType(type), o);
    }

    @Override
    public <T> void listenTyped(AnyType<T> type, Consumer<T> listener) {
        entry.define(listener, type);
    }

    @Override
    public <T> void listenTyped(Type type, Consumer<T> listener) {
        entry.define(listener, types.getType(type));
    }

    @Override
    public void listenSubContent(IOConsumer<OdinReader> subReader) {
        entry.define(subReader);
    }

    // ---------------------------------------------------------------------------------------------------------------------------------------------------- MAP

    @Override
    public boolean hasRowNext() throws IOException {
        if (firstRow)
            return hasNext();
        else {
            char c = stream.next(false);
            return c == ':';
        }
    }

    @Override
    public boolean takeBoolean() throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return stream.readBoolean();
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public byte takeByte() throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return (byte) stream.readInt();
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public short takeShort() throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return (short) stream.readInt();
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public int takeInt() throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return stream.readInt();
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public long takeLong() throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return stream.readLong();
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public float takeFloat() throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return stream.readFloat();
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public double takeDouble() throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return stream.readDouble();
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public char takeChar() throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return stream.readChar();
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public String takeString() throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return stream.readString();
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public <T> T take() throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return (T) types.UNKNOWN_TYPE.read(this, stream, null);
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public <T> T takeTo(T o) throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return (T) types.UNKNOWN_TYPE.read(this, stream, o);
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public <T> T takeTo(T o, AnyType<T> type) throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return ((AnyTypeImpl<T>) type).read(this, stream, o);
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public <T> T takeTo(T o, Type type) throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return (T) types.getType(type).read(this, stream, o);
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public <T> T takeTyped(AnyType<T> type) throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return ((AnyTypeImpl<T>) type).read(this, stream, null);
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public <T> T takeTyped(Type type) throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            return (T) types.getType(type).read(this, stream, null);
        }
        throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }

    @Override
    public void takeSubContent(IOConsumer<OdinReader> subReader) throws IOException {
        if (firstRow || stream.eat(':')) {
            firstRow = false;
            if (stream.eat('{')) {
                ReaderImpl sub = openSubReader(null);
                subReader.accept(sub);
                sub.readEntries(null);
                if (stream.eat('}'))
                    stream.eatEnd();
                else
                    throw stream.getUnwantedCharException("Unable to get object ending.", '}');
            } else
                throw stream.getUnwantedCharException("Unable to get object beginning.", '{');
        } else
            throw stream.getUnwantedCharException("Unable to get row value start.", ':');
    }
}
