package com.newpixelcoffee.odin.access;

import com.newpixelcoffee.odin.AnyType;
import com.newpixelcoffee.odin.OdinWriter;

import java.io.IOException;
import java.lang.reflect.Type;

public interface OdinFieldWriter {

    /**
     * Write a boolean to the current field
     * @param value boolean to write
     * @throws IOException if an IO exception occur
     */
    void withBoolean(boolean value) throws IOException;

    /**
     * Write a byte to the current field
     * @param value byte to write
     * @throws IOException if an IO exception occur
     */
    void withByte(byte value) throws IOException;

    /**
     * Write a short to the current field
     * @param value short to write
     * @throws IOException if an IO exception occur
     */
    void withShort(short value) throws IOException;

    /**
     * Write a int to the current field
     * @param value int to write
     * @throws IOException if an IO exception occur
     */
    void withInt(int value) throws IOException;

    /**
     * Write a long to the current field
     * @param value long to write
     * @throws IOException if an IO exception occur
     */
    void withLong(long value) throws IOException;

    /**
     * Write a float to the current field
     * @param value float to write
     * @throws IOException if an IO exception occur
     */
    void withFloat(float value) throws IOException;

    /**
     * Write a double to the current field
     * @param value double to write
     * @throws IOException if an IO exception occur
     */
    void withDouble(double value) throws IOException;

    /**
     * Write a char to the current field
     * @param value char to write
     * @throws IOException if an IO exception occur
     */
    void withChar(char value) throws IOException;

    /**
     * Write a String to the current field
     * @param value String to write
     * @throws IOException if an IO exception occur
     */
    void withString(String value) throws IOException;

    /**
     * Write object to the current field without a generic type. {@literal Class<Object>} is used as type, so if the object is not a primitive, null or an instance of Object, the type
     * definition is added before the object
     * @param o object to write
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for this object.
     * Sub object type error is placed as cause of a {@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    void with(Object o) throws IOException;

    /**
     * Write an object to the current field, if object class is not the same than type class, like implementation, the type name is added before the object.<br>
     * This method take a odin type to be use inside adapter by using {@link com.newpixelcoffee.odin.ObjectType#getField(String)} or all other field getter and
     * {@link com.newpixelcoffee.odin.internal.types.TypeField#getType()}
     * @param o object to write
     * @param type object generic type
     * @param <T> object generic type
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for this object.
     * Sub object type error is placed as cause of a {@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    <T> void withTyped(T o, AnyType<T> type) throws IOException;

    /**
     * Write an object to the current field, odin get the {@link AnyType} associated to the given type, if the object class is not the same than type class, like implementation,
     * the type name is added before the object.<br>
     * Custom type can be created to add generic argument ( can be useful for collection ) using {@link com.newpixelcoffee.odin.TypeBuilder}.
     * @param o object to write
     * @param type object type
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for this object.
     * Sub object type error is placed as cause of a {@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    void withTyped(Object o, Type type) throws IOException;

    /**
     * Use a consumer to write content to a sub object. All writing do with the {@literal subWriter} are done inside a sub object, inside the {} delimiter
     * @param subWriter the sub writer consumer
     * @throws IOException if an IO exception occur
     */
    void withSubContent(IOConsumer<OdinWriter> subWriter) throws IOException;
}
