package com.newpixelcoffee.odin.access;

import com.newpixelcoffee.odin.AnyType;
import com.newpixelcoffee.odin.OdinWriter;

import java.io.IOException;
import java.lang.reflect.Type;

public interface OdinRowWriter {

    /**
     * Add a boolean to the row
     * @param value boolean to write
     * @return this {@link OdinRowWriter} instance
     * @throws IOException if an IO exception occur
     */
    OdinRowWriter addBoolean(boolean value) throws IOException;

    /**
     * Add a byte to the row
     * @param value byte to write
     * @return this {@link OdinRowWriter} instance
     * @throws IOException if an IO exception occur
     */
    OdinRowWriter addByte(byte value) throws IOException;

    /**
     * Add a short to the row
     * @param value short to write
     * @return this {@link OdinRowWriter} instance
     * @throws IOException if an IO exception occur
     */
    OdinRowWriter addShort(short value) throws IOException;

    /**
     * Add a int to the row
     * @param value int to write
     * @return this {@link OdinRowWriter} instance
     * @throws IOException if an IO exception occur
     */
    OdinRowWriter addInt(int value) throws IOException;

    /**
     * Add a long to the row
     * @param value long to write
     * @return this {@link OdinRowWriter} instance
     * @throws IOException if an IO exception occur
     */
    OdinRowWriter addLong(long value) throws IOException;

    /**
     * Add a float to the row
     * @param value float to write
     * @return this {@link OdinRowWriter} instance
     * @throws IOException if an IO exception occur
     */
    OdinRowWriter addFloat(float value) throws IOException;

    /**
     * Add a double to the row
     * @param value double to write
     * @return this {@link OdinRowWriter} instance
     * @throws IOException if an IO exception occur
     */
    OdinRowWriter addDouble(double value) throws IOException;

    /**
     * Add a char to the row
     * @param value char to write
     * @return this {@link OdinRowWriter} instance
     * @throws IOException if an IO exception occur
     */
    OdinRowWriter addChar(char value) throws IOException;

    /**
     * Add a String to the row
     * @param value String to write
     * @return this {@link OdinRowWriter} instance
     * @throws IOException if an IO exception occur
     */
    OdinRowWriter addString(String value) throws IOException;

    /**
     * Write an object, if object is not a primitive or an {@link Object}, the type definition is added before the object.
     * @param o object to write
     * @return this {@link OdinRowWriter} instance
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for this object.
     * Sub object type error is placed as cause of a {@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    OdinRowWriter add(Object o) throws IOException;

    /**
     * Write an object, if the object class is not the same than type class, like implementation, the type definition is added before the object.<br>
     * This method take a odin type to be use inside adapter by using {@link com.newpixelcoffee.odin.ObjectType#getField(String)} or all other field getter and
     * {@link com.newpixelcoffee.odin.internal.types.TypeField#getType()}
     * @param o object to write
     * @param type object generic type
     * @param <T> object generic type
     * @return this {@link OdinRowWriter} instance
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for this object.
     * Sub object type error is placed as cause of a {@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    <T> OdinRowWriter addTyped(T o, AnyType<T> type) throws IOException;

    /**
     * Write an object, odin get the {@link AnyType} associated to the given type, if the object class is not the same than type class, like implementation,
     * the type definition is added before the object.<br>
     * Custom type can be created to add generic argument ( can be useful for collection ) by using {@link com.newpixelcoffee.odin.TypeBuilder}.
     * @param o object to write
     * @param type object type
     * @return this {@link OdinRowWriter} instance
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for this object.
     * Sub object type error is placed as cause of a {@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    OdinRowWriter addTyped(Object o, Type type) throws IOException;

    /**
     * Use a consumer to write content to a sub object. All writing do with the {@literal subWriter} are done inside a sub object, inside the {} delimiter
     * @param subWriter the sub writer consumer
     * @return this {@link OdinRowWriter} instance
     * @throws IOException if an IO exception occur
     */
    OdinRowWriter addSubContent(IOConsumer<OdinWriter> subWriter) throws IOException;
}
