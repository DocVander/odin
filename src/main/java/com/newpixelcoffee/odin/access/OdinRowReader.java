package com.newpixelcoffee.odin.access;

import com.newpixelcoffee.odin.AnyType;
import com.newpixelcoffee.odin.OdinReader;

import java.io.IOException;
import java.lang.reflect.Type;

public interface OdinRowReader {

    /**
     * Get if there is any remaining value inside the current row, this method simply test if a ':' separator if found on the stream before any other elements.
     * @return true if remaining values is present
     * @throws IOException if an IO exception occur
     */
    boolean hasRowNext() throws IOException;

    /**
     * Read next element as a boolean
     * @return a boolean
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid boolean
     */
    boolean takeBoolean() throws IOException;

    /**
     * Read next element as a byte
     * @return a byte
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid number
     */
    byte takeByte() throws IOException;

    /**
     * Read next element as a short
     * @return a short
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid number
     */
    short takeShort() throws IOException;

    /**
     * Read next element as a int
     * @return a int
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid number
     */
    int takeInt() throws IOException;

    /**
     * Read next element as a long
     * @return a long
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid number
     */
    long takeLong() throws IOException;

    /**
     * Read next element as a float
     * @return a float
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid number
     */
    float takeFloat() throws IOException;

    /**
     * Read next element as a double
     * @return a double
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid number
     */
    double takeDouble() throws IOException;

    /**
     * Read next element as a char
     * @return a char
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid char
     */
    char takeChar() throws IOException;

    /**
     * Read next element as a String
     * @return a String
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid String
     */
    String takeString() throws IOException;

    /**
     * Read value from the current object without any generic type, if no type definition is present before the value in the stream a simple {@literal Class<Object>} is used as type, so only
     * null, primitives, Object, Object[] or recursive object can be return.<br>
     * If a type definition is present the reader will use it
     * @param <T> object type
     * @return the object read
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    <T> T take() throws IOException;

    /**
     * Read value to a given object instance, if a type definition is present in the input stream it will be skip to use the given instance type.<br>
     * This method is not recursive, so all object fields are set with a new object instances according to the stream content.<br>
     * If the object is null this method do the same as {@link #take()}.<br>
     * If the field is a primitive, this method return his new value.<br>
     * If the object use a inline or a immutable adapter, this method return a new object instance
     * @param o object instance
     * @param <T> object type
     * @return the given 'o' object
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    <T> T takeTo(T o) throws IOException;

    /**
     * Read value to a given object instance, if a type definition is present in the input stream it will be skip to use the given instance type.<br>
     * This method is not recursive, so all object fields are set with a new object instances according to the stream content.<br>
     * If the object is null this method do the same as {@link #takeTyped(AnyType)} and use the given type.<br>
     * If the field is a primitive, this method return his new value.<br>
     * If the object use a inline or a immutable adapter, this method return a new object instance.
     * This method differ from {@link #takeTo(Object)} by his security for null object case, if the given object is null and the input stream doesn't contains the type definition, the given type is
     * used instead of Object class
     * @param o object instance
     * @param type object type
     * @param <T> object type
     * @return the given 'o' object
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    <T> T takeTo(T o, AnyType<T> type) throws IOException;

    /**
     * Read value to a given object instance, if a type definition is present in the input stream it will be skip to use the given instance type.<br>
     * This method is not recursive, so all object fields are set with a new object instances according to the stream content.<br>
     * If the object is null this method do the same as {@link #takeTyped(Type)} and use the given type.<br>
     * If the field is a primitive, this method return his new value.<br>
     * If the object use a inline or a immutable adapter, this method return a new object instance.
     * This method differ from {@link #takeTo(Object)} by his security for null object case, if the given object is null and the input stream doesn't contains the type definition, the given type is
     * used instead of Object class
     * @param o object instance
     * @param type object type
     * @param <T> object type
     * @return the given 'o' object
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    <T> T takeTo(T o, Type type) throws IOException;

    /**
     * Read value using given generic type, if a type definition is present it will be used as object type.<br>
     * This method take a odin type to be use inside adapter by using {@link com.newpixelcoffee.odin.ObjectType#getField(String)} or all other field getter and
     * {@link com.newpixelcoffee.odin.internal.types.TypeField#getType()}
     * @param type generic type
     * @param <T> object type
     * @return the object read
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    <T> T takeTyped(AnyType<T> type) throws IOException;

    /**
     * Read value using given generic type, if a type definition is present it will be used as object type.<br>
     * Custom type can be created to add generic argument ( can be useful for collection ) using {@link com.newpixelcoffee.odin.TypeBuilder}.
     * @param type generic type
     * @param <T> object type
     * @return the object read
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    <T> T takeTyped(Type type) throws IOException;

    /**
     * Use a consumer to read content of a sub object. All reading do with the {@literal subReader} are done inside a sub object, inside the {} delimiter.<br>
     * You can use all the standard reader methods, like {@literal subReader.hasNext()} to know if the sub content contains remaining objects
     * @param subReader the sub reader consumer
     * @throws IOException if an IO exception occur
     */
    void takeSubContent(IOConsumer<OdinReader> subReader) throws IOException;
}
