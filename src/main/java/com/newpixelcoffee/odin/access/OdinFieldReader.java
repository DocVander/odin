package com.newpixelcoffee.odin.access;

import com.newpixelcoffee.odin.AnyType;
import com.newpixelcoffee.odin.OdinReader;

import java.lang.reflect.Type;
import java.util.function.Consumer;

public interface OdinFieldReader {

    /**
     * Add a listener that will be called if this field is not found inside the current object.<br>
     * This method return this reader instance in order to use a fluent design :
     * <pre>{@code // int v; is a class field
     * reader.readField("value")
     *      .listenNotFound(() -> v = -1)
     *      .listenInt((Integer i) -> v = i);
     * }</pre>
     * @param listener listener
     * @return this field reader instance
     */
    OdinFieldReader listenNotFound(Runnable listener);

    void listenBoolean(Consumer<Boolean> listener);

    void listenByte(Consumer<Byte> listener);

    void listenShort(Consumer<Short> listener);

    void listenInt(Consumer<Integer> listener);

    void listenLong(Consumer<Long> listener);

    void listenFloat(Consumer<Float> listener);

    void listenDouble(Consumer<Double> listener);

    void listenChar(Consumer<Character> listener);

    void listenString(Consumer<String> listener);

    <T> void listen(Consumer<T> listener);

    /**
     * Read value to a given object instance, if a type definition is present in the input stream it will be skip to use the given instance type.<br>
     * This method is not recursive, so all the object fields are set with new object instances according to the stream content.<br>
     * If the object is null this method simply skip the object to read, if you need to get object with null as given object use {@link #listenTo(Object, Consumer)}.<br>
     * If the field is a primitive, this method skip the object to read.<br>
     * If the object use a inline or a immutable adapter, this method skip the object to read
     * @param o object instance
     * @param <T> object type
     */
    <T> void listenTo(T o);

    /**
     * Read value to a given object instance and call listener with it, if a type definition is present in the input stream it will be skip to use the given instance type.<br>
     * This method is not recursive, so all the object fields are set with new object instances according to the stream content.<br>
     * If the object is null this method do the same as {@link #listen(Consumer)}.<br>
     * If the field is a primitive, this method call the consumer with his new value.<br>
     * If the object use a inline or a immutable adapter, this method call the consumer with a new object instance
     * @param o object instance
     * @param listener listener to call when value is read
     * @param <T> object type
     */
    <T> void listenTo(T o, Consumer<T> listener);

    /**
     * Read value to a given object instance and call listener with it, if a type definition is present in the input stream it will be skip to use the given instance type.<br>
     * This method is not recursive, so all the object fields are set with new object instances according to the stream content.<br>
     * If the object is null this method do the same as {@link #listenTyped(AnyType, Consumer)} and use the given type.<br>
     * If the field is a primitive, this method call the consumer with his new value.<br>
     * If the object use a inline or a immutable adapter, this method call the consumer with a new object instance.
     * This method differ from {@link #listenTo(Object)} by his security for null object case, if the object is null and the input stream doesn't contains the type definition, the given type is used
     * instead of Object class
     * @param o object instance
     * @param type object type
     * @param listener listener to call when value is read
     * @param <T> object type
     */
    <T> void listenTo(T o, AnyType<T> type, Consumer<T> listener);

    /**
     * Read value to a given object instance and call listener with it, if a type definition is present in the input stream it will be skip to use the given instance type.<br>
     * This method is not recursive, so all the object fields are set with new object instances according to the stream content.<br>
     * If the object is null this method do the same as {@link #listenTyped(Type, Consumer)} and use the given type.<br>
     * If the field is a primitive, this method call the consumer with his new value.<br>
     * If the object use a inline or a immutable adapter, this method call the consumer with a new object instance.
     * This method differ from {@link #listenTo(Object)} by his security for null object case, if the object is null and the input stream doesn't contains the type definition, the given type is used
     * instead of Object class
     * @param o object instance
     * @param type object type
     * @param listener listener to call when value is read
     * @param <T> object type
     */
    <T> void listenTo(T o, Type type, Consumer<T> listener);

    /**
     * Read value using the given generic type and call listener with it, if a type definition is present it will be used as object type.<br>
     * This method take a odin type to be use inside adapter by using {@link com.newpixelcoffee.odin.ObjectType#getField(String)} or all other field getter and
     * {@link com.newpixelcoffee.odin.internal.types.TypeField#getType()}
     * @param type generic type
     * @param listener listener to call when value is read
     * @param <T> object type
     */
    <T> void listenTyped(AnyType<T> type, Consumer<T> listener);

    /**
     * Read value using the given generic type and call listener with it, if a type definition is present it will be used as object type.<br>
     * Custom type can be created to add generic argument ( can be useful for collection ) using {@link com.newpixelcoffee.odin.TypeBuilder}.
     * @param type generic type
     * @param listener listener to call when value is read
     * @param <T> object type
     */
    <T> void listenTyped(Type type, Consumer<T> listener);

    /**
     * Use a consumer to read content of a sub object. All reading do with the {@literal subReader} are done inside a sub object, inside the {} delimiter.<br>
     * You can use all the standard reader methods, like {@literal subReader.hasNext()} to know if the sub content contains remaining objects
     * @param subReader the sub reader consumer
     */
    void listenSubContent(IOConsumer<OdinReader> subReader);
}
