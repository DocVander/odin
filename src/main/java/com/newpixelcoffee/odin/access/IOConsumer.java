package com.newpixelcoffee.odin.access;

import java.io.IOException;

/**
 * @author DocVander
 */
@FunctionalInterface
public interface IOConsumer<T> {
    void accept(T value) throws IOException;
}
