package com.newpixelcoffee.odin;

import com.newpixelcoffee.odin.adapters.*;
import com.newpixelcoffee.odin.elements.OdinArray;
import com.newpixelcoffee.odin.elements.OdinElement;
import com.newpixelcoffee.odin.elements.OdinExtra;
import com.newpixelcoffee.odin.elements.OdinNode;
import com.newpixelcoffee.odin.exceptions.OdinAdapterException;
import com.newpixelcoffee.odin.exceptions.OdinFormatException;
import com.newpixelcoffee.odin.exceptions.OdinTypeException;
import com.newpixelcoffee.odin.internal.IndentWriterImpl;
import com.newpixelcoffee.odin.internal.ReaderImpl;
import com.newpixelcoffee.odin.internal.ShortWriterImpl;
import com.newpixelcoffee.odin.internal.defaults.DefaultDateAdapter;
import com.newpixelcoffee.odin.internal.defaults.LocalDateTimeAdapterParameter;
import com.newpixelcoffee.odin.internal.streams.StreamReader;
import com.newpixelcoffee.odin.internal.streams.StreamWriter;
import com.newpixelcoffee.odin.internal.types.TypeManager;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Main class to access to Odin.<br>
 *
 * <h1>Format</h1>
 * Odin is able to generate 2 types of output, indented or compressed.<br>
 * - <b>Indented</b>, a human readable format can be used for config file or data storage needing to be human readable.
 * Entry is separated with system line separator ( CR (old Mac OS), CRLF (Windows) or LF (Linux, Mac OSX) ) and each depth add a tabulation before entry start. <br>
 * String containing line separator is also write as multilines String where line separator is write without escape char.<br>
 * Indented output allowing the use of {@link OdinExtra}, so all {@link OdinElement} can be write with multiple comments or empty lines.<br>
 * Example of indented output :
 * <pre>{@code {
 *     node = {
 *         a bool = true
 *         a int = 10
 *     }
 *     string = "
 *          Multilines string
 *          With a single line separator
 *     "
 * }}</pre>
 * - <b>Compressed</b>, the default format, used for network or data storage, ',' is used as entry separator and no more space or tabulation is added, all line separator in String are escaped.<br>
 * Example of compressed output :
 * <pre>{@code {node={a bool=true,a int=10},string="Multiline string\nWith a single line separator"}}</pre>
 *
 * <h1>Types</h1>
 * Odin is able to use OdinElement to write node, array or object with comments and blank lines using {@link OdinElement#getExtra()} but also any type of java object.<br>
 * Some basic java type are formatted to String like Class, Enum and UUID but also old and new java Date using custom formatter like {@literal yyyy-MM-dd HH:mm:ss}.<br>
 * Odin have 3 different type of object :<br>
 * - <b>Primitives</b>, managed by Odin stream : null, boolean, byte, short, int, long, float, double, char, String. these types never have a type definition before them, even if their generic type is
 * unknown.<br>
 * - <b>Array</b>, used by {@link OdinArray} and java array object, use '[' and ']' as delimiter. the type definition is write with an additional '[' before name for each dimension.<br>
 * Example for a int[][] :
 * <pre>{@code <[[int>[[1,2],[3,4]]}</pre>
 * - <b>Object</b>, used by {@link OdinNode} and all type which are not listed above, use '{' and '}' as delimiter.<br>
 *
 * <h1>Elements</h1>
 * Object have 3 way to write content :<br>
 * - <b>Field</b>, field use a String key and associate a value with a '=' as separator, used by {@link OdinNode} and default object adapter. Field name use escape for all special char like
 * tabulation or line separator, space and tabulation before and after the key was ignored on read :
 * <pre>{@code {key 1=value 1,key 2=value 2}}</pre>
 * - <b>Value</b>, used by {@link java.util.List} and {@link java.util.Set} adapter, value is write without key :
 * <pre>{@code {value 1,value 2}}</pre>
 * - <b>Row</b>, used by {@link java.util.Map} adapter, row is a element with a list of value, each value is associated together and separated by a ':', a row can contains an unlimited number of
 * values :
 * <pre>{@code {field 1=field value,field 2=field value}}</pre>
 * The root writer and reader is consider as Object and can contains directly these 3 types of elements<br>
 * every objects have a recusivity check, if an object contains it's parent, the object write a reference instead of infinitely looping on it:
 * <pre>{@code {content={id=1,parent=(1)}}}</pre>
 * The int is the position of the object from the current object, 0 is herself, 1 is parent, ...<br>
 *
 * <h1>Adapters</h1>
 * Object use adapters to known how to write and read objects.<br>
 * By registering an adapter you can modify the way your object is serialized and deserialized, like Date using an inline adapter to be write as a String.<br>
 * You can register an adapter with {@link #register(OdinAdapter)} or {@link #register(String, OdinAdapter)} to also add a type alias.<br>
 * Odin has 3 types of adapters :<br>
 * - {@link OdinObjectAdapter} : the standard adapter, used to process default object adapter<br>
 * - {@link OdinImmutableAdapter} : used to read immutable object needing to get content passed to constructor.<br>
 * - {@link OdinInlineAdapter} : used to map an object type to an another, like class, enum and date.<br>
 * Object implementing {@link OdinObjectAdapter} is used as runtime adapter, the object itself is used as the adapter, and write and read method is called inside the object instance.<br>
 *
 * <h1>Annotations</h1>
 * If you need to do some simple change to a class and don't want to registering a adapter for that, you can use annotations :<br>
 * - {@link com.newpixelcoffee.odin.annotations.OdinField}, used to rename a field or add comments lines before it on writer.<br>
 * - {@link com.newpixelcoffee.odin.annotations.OdinMapAsList}, used to write map as a list by searching key inside value fields.<br>
 * - {@link com.newpixelcoffee.odin.annotations.OdinCustomAdapter}, used to change a field adapter without registering a global adapter in Odin.<br>
 * MapAsList, CustomAdapter  bypass adapters registered in Odin, even for implementations.
 * @author DocVander
 */
public class Odin {

    private static final Charset UTF8 = StandardCharsets.UTF_8;

    private final TypeManager types = new TypeManager();
    private boolean indentedOutput = false;
    private int readerBufferSize = 4096;

    /**
     * Add an alias for a class, an alias replace the class name by name in stream.<br>
     * Example: {@literal <com.newpixelcoffee.odin.Odin>} by {@literal <odin>}
     * @param name alias
     * @param type class to name
     * @param <T> Class type
     * @return this odin instance
     * @throws OdinTypeException if type is primitive or array
     */
    public <T> Odin name(String name, Class<T> type) {
        types.name(name, type);
        return this;
    }

    /**
     * Register an adapter in order to modify the writer and reader behavior, an adapter can be defined for any object except primitives and arrays.<br>
     * Three type of adapter is available : <br>
     * - {@link OdinObjectAdapter} is the standard adapter, in write and read process you receive the object type and object instance, you say to odin what to do with that, for example all object use
     * by default an OdinObjectAdapter, for each fields this adapter call writeTyped and readTyped.<br>
     * - {@link OdinImmutableAdapter} is used to read immutable object needing to get content passed to constructor, this adapter does not have recursivity check.<br>
     * - {@link OdinInlineAdapter} is used to transform an object to another object like primitive, for example this adapter is used to transform Class, Enum, UUID or Date to a simple String, this
     * adapter does not have recursive check.
     * @param adapter adapter to register
     * @param <T> adapter object type
     * @return this odin instance
     * @throws OdinTypeException if adapter type is primitive or array
     */
    public <T> Odin register(OdinAdapter<T> adapter) {
        types.register(adapter);
        return this;
    }

    /**
     * Register an adapter and a alias in order to modify the writer and reader behavior, an adapter can be defined for any object except primitives and arrays.<br>
     * Three type of adapter is available : <br>
     * - {@link OdinObjectAdapter} is the standard adapter, in write and read process you receive the object type and object instance, you say to odin what to do with that, for example all object use
     * by default an OdinObjectAdapter, for each fields this adapter call writeTyped and readTyped.<br>
     * - {@link OdinImmutableAdapter} is used to read immutable object who need content value to be passed in constructor, this adapter does not have recursivity check.<br>
     * - {@link OdinInlineAdapter} is used to transform an object to another object like primitive, for example this adapter is used to transform Class, Enum, UUID or Date to a simple String.
     * @param name adapter type alias
     * @param adapter adapter to register
     * @param <T> adapter object type
     * @return this odin instance
     * @throws OdinTypeException if adapter type is primitive or array
     * @see #name(String, Class)
     * @see #register(OdinAdapter)
     */
    public <T> Odin register(String name, OdinAdapter<T> adapter) {
        types.register(name, adapter);
        return this;
    }

    /**
     * <p>Register a class in order to process it as a list, by default only ArrayList, LinkedList and Vector use the list adapter.
     * <p>List that does not use the list adapter are serialized with the default object adapter, by serializing the list fields.
     * <p>The class need to contains a valid {@link List#add(Object)} method.
     * <p>If the type of your list is immutable and need to get the content in the constructor, create a custom list adapter.
     * @param name adapter type alias
     * @param type class to register
     * @param <T> class type
     * @return this odin instance
     */
    public <T extends List> Odin registerAsList(String name, Class<T> type) {
        types.registerImplementation(name, type, types.listAdapter);
        return this;
    }

    /**
     * <p>Register a class in order to process it as a set, by default only HashSet, LinkedHashSet, TreeSet and EnumSet use the set adapter.
     * <p>Set that does not use the set adapter are serialized with the default object adapter, by serializing the set fields.
     * <p>The class need to contains a valid {@link List#add(Object)} method.
     * <p>If the type of your set is immutable and need to get the content in the constructor, create a custom set adapter.
     * @param name adapter type alias
     * @param type class to register
     * @param <T> class type
     * @return this odin instance
     */
    public <T extends Set> Odin registerAsSet(String name, Class<T> type) {
        types.registerImplementation(name, type, types.setAdapter);
        return this;
    }

    /**
     * <p>Register a class in order to process it as a map, by default only HashMap, LinkedHashMap, TreeMap and EnumMap use the map adapter.
     * <p>Map that does not use the map adapter are serialized with the default object adapter, by serializing the map fields.
     * <p>The class need to contains a valid {@link List#add(Object)} method.
     * <p>If the type of your map is immutable and need to get the content in the constructor, create a custom map adapter.
     * @param name adapter type alias
     * @param type class to register
     * @param <T> class type
     * @return this odin instance
     */
    public <T extends Map> Odin registerAsMap(String name, Class<T> type) {
        types.registerImplementation(name, type, types.mapAdapter);
        return this;
    }

    /**
     * Get the odin type associated to a type, if no odin type is associated to this type, odin will create it.<br>
     * Class always use the same odin type instance for same {@link Odin} instance, except generic types like parameterized type or generic array type, a new odin instance is
     * created for each call to this method.
     * @param type type to get
     * @param <T> generic type
     * @return any type
     */
    public <T> AnyType<T> getType(Type type) {
        return types.getType(type);
    }

    /**
     * Get the odin type associated to a classname or type alias, if no odin type is associated to this type, odin will create it using {@link Class#forName(String)}.
     * @param name type to get
     * @param <T> generic type
     * @return any type
     */
    public <T> AnyType<T> getType(String name) {
        return types.getType(name);
    }

    /**
     * Get the registered adapter for given type, this method retrieve nearest adapter in parents class if the type has no registered adapter
     * @param type adapter type
     * @param <T> adapter type
     * @return adapter
     * @throws IllegalStateException if type is primitive or array, or if the type use a runtime adapter
     */
    public <T> OdinAdapter<T> getAdapter(Type type) {
        return types.getAdapter(type);
    }

    /**
     * <p>Change the class fields filter to keep only wanted fields. This filter affect object type and not only the default object adapter.
     * <p>By this way, if a field is hidden by this filter, it will not be present in the result of {@link ObjectType#findField(String)} and other fields access methods
     * <p>The default filter hide transient and static fields. You can filter in a custom annotation using {@link Field#isAnnotationPresent(Class)}
     * @param filter the filter to use
     * @return this odin instance
     */
    public Odin setFieldFilter(Predicate<Field> filter) {
        types.fieldFilter = filter;
        return this;
    }

    /**
     * Change the default date format located in odin adapter, by default, date use 'yyyy-MM-dd HH:mm:ss'.
     * @param format new date format
     * @throws OdinTypeException if date does not use default date adapter
     */
    public void setDefaultDateFormat(DateFormat format) {
        OdinAdapter<Date> date = types.getAdapter(Date.class);
        if (date instanceof DefaultDateAdapter)
            ((DefaultDateAdapter) date).dateFormat = format;
        else
            throw new OdinTypeException("Date does not use default date adapter, unable to change the format");
    }

    /**
     * Change the default local date/time format located in odin adapter, list of default java time format :<br>
     * - <b>LocalDate</b> : yyyy-MM-dd<br>
     * - <b>LocalDateTime</b> : yyyy-MM-dd HH:mm:ss [NANO_OF_SECOND]<br>
     * - <b>LocalTime</b> : HH:mm:ss [NANO_OF_SECOND]<br>
     * - <b>ZonedDateTime</b> : yyyy-MM-dd HH:mm:ss [NANO_OF_SECOND] [ZoneOrZoneID]<br>
     * '[]' refers to optional part
     * @param dateTimeType local date/time type
     * @param format new date format
     * @throws OdinTypeException if date does not use default date adapter
     */
    public void setDefaultDateFormat(Class<? extends Temporal> dateTimeType, DateTimeFormatter format) {
        OdinAdapter<? extends Temporal> date = types.getAdapter(dateTimeType);
        if (date instanceof LocalDateTimeAdapterParameter)
            ((LocalDateTimeAdapterParameter) date).changeParameter(format);
        else
            throw new OdinTypeException(dateTimeType + " does not use default date adapter, unable to change the format");
    }

    /**
     * Change the default object adapter options.<br>
     * This options allow to change the way to write and read all object without custom adapters for some special case :<br>
     * - what to do with reflect exception<br>
     * - what to do with reader exception<br>
     * - what to do with writer exception<br>
     * - what to do with null fields value
     * @return default adapter options
     */
    public DefaultAdapterOption getDefaultAdapterOption() {
        return types.getDefaultAdapterOption();
    }

    /**
     * Create a new {@link OdinWriter} using io {@link Writer}
     * @param writer writer to use
     * @return new writer
     */
    public OdinWriter writer(Writer writer) {
        StreamWriter w = new StreamWriter(writer);
        if (indentedOutput)
            return new IndentWriterImpl(types, w);
        else return new ShortWriterImpl(types, w);
    }

    /**
     * Create a new {@link OdinWriter} using io {@link OutputStream}, the given output stream is transformed to writer with UTF-8 charset
     * @param output stream to use
     * @return new writer
     */
    public OdinWriter writer(OutputStream output) {
        StreamWriter w = new StreamWriter(new OutputStreamWriter(output, UTF8));
        if (indentedOutput)
            return new IndentWriterImpl(types, w);
        else return new ShortWriterImpl(types, w);
    }

    /**
     * Create a new {@link OdinWriter} using nio2 {@link Path}, the given path is transformed to writer with UTF-8 charset.<br>
     * If the file does not exist, it will be created
     * @param path path to use
     * @return new writer
     * @throws IOException if the path generate an IO exception
     */
    public OdinWriter writer(Path path) throws IOException {
        return writer(Files.newOutputStream(path, StandardOpenOption.WRITE, StandardOpenOption.CREATE));
    }

    /**
     * Create a new {@link OdinWriter} using io {@link File}, the given file is transformed to writer with UTF-8 charset.<br>
     * If the file does not exist, it will be created
     * @param file file to use
     * @return new writer
     * @throws FileNotFoundException if file does not exist and cannot be created or file is a directory
     */
    public OdinWriter writer(File file) throws FileNotFoundException {
        return writer(new FileOutputStream(file));
    }

    /**
     * Create a new {@link OdinReader} using io {@link Reader}
     * @param reader reader to use
     * @return new reader
     * @throws IllegalArgumentException if the reader buffer size is lower than 1
     */
    public OdinReader reader(Reader reader) {
        return new ReaderImpl(types, new StreamReader(reader, readerBufferSize));
    }

    /**
     * Create a new {@link OdinReader} using io {@link InputStream}, the given input stream is transformed to reader with UTF-8 charset
     * @param input input to use
     * @return new reader
     * @throws IllegalArgumentException if the reader buffer size is lower than 1
     */
    public OdinReader reader(InputStream input) {
        return new ReaderImpl(types, new StreamReader(new InputStreamReader(input, UTF8), readerBufferSize));
    }

    /**
     * Create a new {@link OdinReader} using nio2 {@link Path}, the given path is transformed to reader with UTF-8 charset.
     * @param path path to use
     * @return new reader
     * @throws IOException is an IO exception occur
     * @throws IllegalArgumentException if the reader buffer size is lower than 1
     */
    public OdinReader reader(Path path) throws IOException {
        return reader(Files.newInputStream(path, StandardOpenOption.READ));
    }

    /**
     * Create a new {@link OdinReader} using io {@link File}, the given file is transformed to reader with UTF-8 charset
     * @param file file to use
     * @return new reader
     * @throws FileNotFoundException if file is not accessible
     * @throws IllegalArgumentException if the reader buffer size is lower than 1
     */
    public OdinReader reader(File file) throws FileNotFoundException {
        return reader(new FileInputStream(file));
    }

    /**
     * Transform object to an odn string using {@link OdinWriter#write(Object)}
     * @param o object to write
     * @return odn result
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for this object.
     * Sub object type error is placed as cause of a {@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    public String toOdn(Object o) {
        StringWriter w = new StringWriter();
        try (OdinWriter writer = writer(w)) {
            writer.write(o);
        } catch (IOException e) {
            // can not be thrown
        }
        return w.toString();
    }

    /**
     * Transform object to an odn string using {@link OdinWriter#writeTyped(Object, Type)}
     * @param o object to write
     * @param type object generic type
     * @return odn result
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for this object.
     * Sub object type error is placed as cause of a {@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    public String toOdn(Object o, Type type) {
        StringWriter w = new StringWriter();
        try (OdinWriter writer = writer(w)) {
            writer.writeTyped(o, type);
        } catch (IOException e) {
            // can not be thrown
        }
        return w.toString();
    }

    /**
     * Transform an odn string to object using {@link OdinReader#read()}
     * @param odn String to read
     * @param <T> object type
     * @return object
     * @throws OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws OdinTypeException if the type adapter can not be build for an object
     * @throws OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    public <T> T fromOdn(String odn) {
        StringReader r = new StringReader(odn);
        try (OdinReader reader = reader(r)) {
            return reader.read();
        } catch (IOException e) {
            // can not be thrown
            return null;
        }
    }

    /**
     * Transform an odn string to object using {@link OdinReader#readTyped(Type)}
     * @param odn String to read
     * @param type object type
     * @param <T> object type
     * @return object
     * @throws OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws OdinTypeException if the type adapter can not be build for an object
     * @throws OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    public <T> T fromOdn(String odn, Type type) {
        StringReader r = new StringReader(odn);
        try (OdinReader reader = reader(r)) {
            return reader.readTyped(type);
        } catch (IOException e) {
            // can not be thrown
            return null;
        }
    }

    /**
     * Get if writer use an indented output. writer created before call to {@link #indentOutput()} or {@link #compressOutput()} can be different from this value
     * @return true if output is human readable
     */
    public boolean isIndented() {
        return indentedOutput;
    }

    /**
     * Change output to be indented and human readable. indented output use the system line separator ( CR (old Mac OS), CRLF (Windows) or LF (Linux, Mac OSX) ) as entry separator and tabulation for
     * indentation.<br>
     * Example with a simple node write to file :
     * <pre>{@code {
     *     node = {
     *         a bool = true
     *         a int = 10
     *     }
     *     a char = 'c'
     * }}</pre>
     *
     * String containing line return is write as multilines String :
     * <pre>{@code string = "
     *      This is a multiline string
     *      With a single line separator
     * "}</pre>
     * Multilines string use the line separator in the String and not the Writer line separator, so a String with a \n at end of first line and \r in second line is write with only this \n or \n at
     * end of lines in order to keep the good chars when reading the String.<br>
     * <br>
     * If the String contains space or tab after a line return, a new backslash is added to detect the line start :
     * <pre>{@code string = "
     *      This is a multiline string
     *      \ With a single line separator
     * " }</pre>
     * Indented output allowing the use of {@link OdinExtra}, so all {@link OdinElement} can be write with multiple comments or empty lines :
     * <pre>{@code {
     *     node = {
     *         # COMMENT 1
     *         # COMMENT 2
     *
     *         # COMMENT 3
     *         a bool = true
     *
     *         # COMMENT 4
     *     }
     * }}</pre>
     * @return this odin instance
     * @see #compressOutput()
     */
    public Odin indentOutput() {
        this.indentedOutput = true;
        return this;
    }

    /**
     * Change output to be short as possible, ',' is used as entry separator and no new line or indentation is used.<br>
     * Example with a simple node write to file :
     * <pre>{@code {node={a bool=true,a int=10},a char='c'}}</pre>
     *
     * String containing line return is write with backslash :
     * <pre>{@code string="This is a multiline string\nWith a single line separator" }</pre>
     *
     * @return this odin instance
     * @see #indentOutput()
     */
    public Odin compressOutput() {
        this.indentedOutput = false;
        return this;
    }

    /**
     * Get the actual buffer size used in {@link OdinReader}. reader created before call to {@link #setReaderBufferSize(int)} can have a different buffer size. The default buffer size is 4096
     * @return actual buffer size
     */
    public int getReaderBufferSize() {
        return readerBufferSize;
    }

    /**
     * Set buffer size for futurely created readers. The default buffer size is 4096
     * @param readerBufferSize new buffer size
     * @return this odin instance
     */
    public Odin setReaderBufferSize(int readerBufferSize) {
        this.readerBufferSize = readerBufferSize;
        return this;
    }
}
