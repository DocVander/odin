package com.newpixelcoffee.odin.exceptions;

/**
 * Thrown by odin when a object type can not be initialized. For example: unknown class or wrong adapter parameter
 * @author DocVander
 */
public class OdinTypeException extends RuntimeException {

    public OdinTypeException(String message) {
        super(message);
    }

    public OdinTypeException(String message, Throwable cause) {
        super(message, cause);
    }
}
