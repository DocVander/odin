package com.newpixelcoffee.odin.exceptions;

/**
 * Thrown by Odin if the reader reach an error in the format or trying to read an object with the wrong type. for example: odn contains a number but trying to read a String.
 * @author DocVander
 */
public class OdinFormatException extends RuntimeException {

    public OdinFormatException(String message) {
        super(message);
    }
}
