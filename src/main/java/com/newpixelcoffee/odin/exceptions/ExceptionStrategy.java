package com.newpixelcoffee.odin.exceptions;

/**
 * Define the strategy to use when the Default object adapter reach a {@link RuntimeException}.
 * @author DocVander
 */
public interface ExceptionStrategy {
    /**
     * Define the strategy to use when the Default object adapter reach a {@link RuntimeException}.
     * @param type the name of the class containing the field
     * @param field the name of the field where the exceptions occurred
     * @param message the exception message
     * @param cause the cause of this exception
     */
    void processException(String type, String field, String message, Throwable cause);
}
