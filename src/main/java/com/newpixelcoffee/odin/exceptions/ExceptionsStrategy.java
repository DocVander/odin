package com.newpixelcoffee.odin.exceptions;

/**
 * Default Odin exceptions strategy
 * @author DocVander
 */
public enum ExceptionsStrategy implements ExceptionStrategy {

    /**
     * When an exception occur, do nothing with it, continue to read or write the current object.
     */
    CONTINUE_EXECUTION() {
        @Override
        public void processException(String type, String field, String message, Throwable cause) {
            // do nothing
        }
    },
    /**
     * When an exception occur, this strategy log the exception with the path between the object using this strategy and the field where the exception occur.
     */
    LOG_EXCEPTION() {
        @Override
        public void processException(String type, String field, String message, Throwable cause) {
            new OdinAdapterException(type, field, message, cause).printStackTrace();
        }
    },
    /**
     * <p>When an exception occur, this strategy throw a new {@link OdinAdapterException} with the given layer information.
     * <p>When the {@link OdinAdapterException} is printed to the console or other destination, it will also display the path between the current object and the field where the exception occur.
     */
    FORWARD_EXCEPTION() {
        @Override
        public void processException(String type, String field, String message, Throwable cause) {
            throw new OdinAdapterException(type, field, message, cause);
        }
    };
}
