package com.newpixelcoffee.odin;

import java.lang.reflect.Field;

/**
 * Represent a class field. This class can be used to manipulate java reflect fields or to get a field type
 * @author DocVander
 */
public interface FieldType<T> {

    /**
     * The name of the field. If the field contains a {@link com.newpixelcoffee.odin.annotations.OdinField} annotation, this method return the name defined by the annotation.
     * @return the field name
     */
    String getName();

    /**
     * Get the field type. That can be used to specify the generic type in writer or reader.
     * @return the field type
     */
    AnyType<T> getType();

    /**
     * Get the java reflect field.
     * @return field
     */
    Field getField();

    /**
     * Get the field comments. the default value is null. comments can only be specified using {@link com.newpixelcoffee.odin.annotations.OdinField} annotation.<br>
     * Empty String represent blank lines.
     * @return field comments
     */
    String[] getComments();
}
