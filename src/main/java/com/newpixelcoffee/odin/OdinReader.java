package com.newpixelcoffee.odin;

import com.newpixelcoffee.odin.access.IOConsumer;
import com.newpixelcoffee.odin.access.OdinFieldReader;
import com.newpixelcoffee.odin.access.OdinRowReader;
import com.newpixelcoffee.odin.elements.OdinArray;
import com.newpixelcoffee.odin.elements.OdinNode;
import com.newpixelcoffee.odin.elements.OdinObject;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Type;

/**
 * @author DocVander
 */
public interface OdinReader extends Closeable {

    // elements

    /**
     * Read Odin Node and all extra if present
     * @return a node
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    OdinNode readNode() throws IOException;

    /**
     * Read Odin Array and all extra if present
     * @return a array
     * @throws IOException if a IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    OdinArray readArray() throws IOException;

    /**
     * Read Odin object and it's extra if present
     * @param <T> the object content type
     * @return a object
     * @throws IOException if IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    <T> OdinObject<T> readObject() throws IOException;

    // objects

    /**
     * Indicates whether there is any data left in the current object.<br>
     * If comments is present before the next data, it will be skipped.
     * @return false if reached end of current object
     * @throws IOException if an IO exception occur
     */
    boolean hasNext() throws IOException;

    /**
     * Read next element as a key
     * @return next field key
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element is not an field key
     */
    String readKey() throws IOException;

    /**
     * Read all next elements until the next key, this method return null if the object contains no more key
     * @return next field key
     * @throws IOException if an IO exception occur
     */
    String readNextKey() throws IOException;

    /**
     * Read next element as a boolean
     * @return a boolean
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid boolean
     */
    boolean readBoolean() throws IOException;

    /**
     * Read next element as a byte
     * @return a byte
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid number
     */
    byte readByte() throws IOException;

    /**
     * Read next element as a short
     * @return a short
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid number
     */
    short readShort() throws IOException;

    /**
     * Read next element as a int
     * @return a int
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid number
     */
    int readInt() throws IOException;

    /**
     * Read next element as a long
     * @return a long
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid number
     */
    long readLong() throws IOException;

    /**
     * Read next element as a float
     * @return a float
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid number
     */
    float readFloat() throws IOException;

    /**
     * Read next element as a double
     * @return a double
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid number
     */
    double readDouble() throws IOException;

    /**
     * Read next element as a char
     * @return a bte
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid char
     */
    char readChar() throws IOException;

    /**
     * Read next element as a String
     * @return a String
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if next element was not a valid String
     */
    String readString() throws IOException;

    /**
     * Read value from the current object without any generic type, if no type definition is present before the value in the stream a simple {@literal Class<Object>} is used as type, so only
     * null, primitives, Object, Object[] or recursive object can be return.<br>
     * If a type definition is present the reader will use it
     * @param <T> object type
     * @return the object read
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    <T> T read() throws IOException;

    /**
     * Read value to a given object instance, if a type definition is present in the input stream it will be skip to use the given instance type.<br>
     * This method is not recursive, so all object fields are set with new object instances according to the stream content.<br>
     * If the object is null this method do the same as {@link #read()}.<br>
     * If the field is a primitive, this method return his new value.<br>
     * If the object use a inline or a immutable adapter, this method return a new object instance
     * @param o object instance
     * @param <T> object type
     * @return the given 'o' object
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    <T> T readTo(T o) throws IOException;

    /**
     * Read value to a given object instance, if a type definition is present in the input stream it will be skip to use the given instance type.<br>
     * This method is not recursive, so all object fields are set with new object instances according to the stream content.<br>
     * If the object is null this method do the same as {@link #readTyped(AnyType)} and use the given type.<br>
     * If the field is a primitive, this method return his new value.<br>
     * If the object use a inline or a immutable adapter, this method return a new object instance.
     * This method differ from {@link #readTo(Object)} by his security for null object case, if the object is null and the input stream doesn't contains the type definition, the given type is used
     * instead of Object class
     * @param o object instance
     * @param type object type
     * @param <T> object type
     * @return the given 'o' object
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    <T> T readTo(T o, AnyType<T> type) throws IOException;

    /**
     * Read value to a given object instance, if a type definition is present in the input stream it will be skip to use the given instance type.<br>
     * This method is not recursive, so all object fields are set with new object instances according to the stream content.<br>
     * If the object is null this method do the same as {@link #readTyped(Type)} and use the given type.<br>
     * If the field is a primitive, this method return his new value.<br>
     * If the object use a inline or a immutable adapter, this method return a new object instance.
     * This method differ from {@link #readTo(Object)} by his security for null object case, if the object is null and the input stream doesn't contains the type definition, the given type is used
     * instead of Object class
     * @param o object instance
     * @param type object type
     * @param <T> object type
     * @return the given 'o' object
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    <T> T readTo(T o, Type type) throws IOException;

    /**
     * Read value using given generic type, if a type definition is present in the input stream it will be used as object type.<br>
     * This method take a odin type to be use inside adapter by using {@link ObjectType#getField(String)} or all other field getter and
     * {@link com.newpixelcoffee.odin.internal.types.TypeField#getType()}
     * @param type generic type
     * @param <T> object type
     * @return the object read
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    <T> T readTyped(AnyType<T> type) throws IOException;

    /**
     * Read value using given generic type, if a type definition is present in the input stream it will be used as object type.<br>
     * Custom type can be created to add generic argument ( can be useful for collection ) using {@link TypeBuilder}.
     * @param type generic type
     * @param <T> object type
     * @return the object read
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the reader reach an invalid element delimiter or wrong element format
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when reading the object
     */
    <T> T readTyped(Type type) throws IOException;

    /**
     * Use a consumer to read content of a sub object. All reading do with the {@literal subReader} are done inside a sub object, inside the {} delimiter.<br>
     * You can use all the standard reader methods, like {@literal subReader.hasNext()} to know if the sub content contains remaining objects
     * @param subReader the sub reader consumer
     * @throws IOException if an IO exception occur
     */
    void readSubContent(IOConsumer<OdinReader> subReader) throws IOException;

    /**
     * Read field with the given name, this method add in cache the field to read and return an {@link OdinFieldReader} in order to set listener.<br>
     * All listener of an object are called after the adapter process, or in case of main reader, when the close method is called.<br>
     * <br>
     * Other read methods doesn't known if the next element is a field and try to read it as a value, so the following code does not work :
     * <pre>{@code {
     *     key = 1
     *     10
     * }
     *
     * reader.readField("key").getInt((Integer i) -> this.v = i);
     * reader.readInt(); // trying to read "key" as a int and throw an IllegalStateException
     * }</pre>
     * In general, never mix field and value inside an object.<br>
     * When the field is found on the stream, the listener is called with the object, else, when the current object is fully read, not found listener of remaining fields are called.<br>
     * @param name field name
     * @return field reader
     */
    OdinFieldReader readField(String name);

    /**
     * This method return a {@link OdinRowReader} in order to read row values.<br>
     * A row need to have at least one value, you can use {@link OdinRowReader#hasRowNext()} to test if there are remaining values in the current row.
     * @return row reader
     * @throws IOException if an IO exception occur
     */
    OdinRowReader readRow() throws IOException;
}
