package com.newpixelcoffee.odin;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * {@link Type} builder.<br>
 * Use anonymous inner class with generic parameter to define type to read.<br>
 * For example {@literal new TypeBuilder<List<String>>() {};} to get {@literal List<String>}.
 *
 * @param <T> type to read
 */
public abstract class TypeBuilder<T> {
    public final Type type;
    protected TypeBuilder() {
        ParameterizedType parameterized = (ParameterizedType) this.getClass().getGenericSuperclass();
        this.type = parameterized.getActualTypeArguments()[0];
    }
}
