package com.newpixelcoffee.odin;

import com.newpixelcoffee.odin.access.IOConsumer;
import com.newpixelcoffee.odin.access.OdinFieldWriter;
import com.newpixelcoffee.odin.access.OdinRowWriter;
import com.newpixelcoffee.odin.elements.OdinArray;
import com.newpixelcoffee.odin.elements.OdinNode;
import com.newpixelcoffee.odin.elements.OdinObject;

import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @author DocVander
 */
public interface OdinWriter extends Closeable {

    /**
     * Say to current writer that all sub objects need to be written using inline format.<br>
     * If this writer is already an inline or a compressed writer, this method do nothing.<br>
     * Inline format use ', ' and ' = ' as separator :
     * <pre>{@code {node = {a bool = true, a int = 10}, a char = 'c'}}</pre>
     */
    void compressSubObjects();

    /**
     * Say to current writer that all sub object can be rewritten using indented format, this method can be call after {@link #compressSubObjects()} to reset format.<br>
     * If this writer is not an indented writer, this method do nothing
     */
    void indentSubObjects();

    /**
     * Write a comment line, if this writer use compressed output, this method do nothing. an empty String add an empty new line without #
     * @param comment comment to add
     * @throws IOException if an IO exception occur
     */
    void writeComment(String comment) throws IOException;

    /**
     * Write multiple comment lines, if this writer use compressed output, this method do nothing. an empty String add an empty new line without #
     * @param comments comments to add
     * @throws IOException if an IO exception occur
     */
    void writeComment(String[] comments) throws IOException;

    /**
     * Write multiple comment lines, if this writer use compressed output, this method do nothing. an empty String add an empty new line without #
     * @param comments comments to add
     * @throws IOException if an IO exception occur
     */
    void writeComment(List<String> comments) throws IOException;

    // elements

    /**
     * Write Odin node, if writer is indented all odin {@link com.newpixelcoffee.odin.elements.OdinExtra} are write before elements
     * @param node node to write
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if node or a sub node contains an invalid field key ( key can not contains the following delimiter <b>,:={}[]&lt;&gt;'"</b> )
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    void writeNode(OdinNode node) throws IOException;

    /**
     * Write Odin array, if writer is indented all odin {@link com.newpixelcoffee.odin.elements.OdinExtra} are write before elements
     * @param array array to write
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if a sub node contains an invalid field key ( key can not contains the following delimiter <b>,:={}[]&lt;&gt;'"</b> )
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    void writeArray(OdinArray array) throws IOException;

    /**
     * Write Odin object, if writer is indented odin {@link com.newpixelcoffee.odin.elements.OdinExtra} was write before this object
     * @param object object to write
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if an invalid field key is write by an adapter ( key can not contains the following delimiter <b>,:={}[]&lt;&gt;'"</b> )
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for an object
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    void writeObject(OdinObject object) throws IOException;

    // objects

    /**
     * Write a boolean
     * @param value boolean to write
     * @throws IOException if an IO exception occur
     */
    void writeBoolean(boolean value) throws IOException;

    /**
     * Write a byte
     * @param value byte to write
     * @throws IOException if an IO exception occur
     */
    void writeByte(byte value) throws IOException;

    /**
     * Write a short
     * @param value short to write
     * @throws IOException if an IO exception occur
     */
    void writeShort(short value) throws IOException;

    /**
     * Write a int
     * @param value int to write
     * @throws IOException if an IO exception occur
     */
    void writeInt(int value) throws IOException;

    /**
     * Write a long
     * @param value long to write
     * @throws IOException if an IO exception occur
     */
    void writeLong(long value) throws IOException;

    /**
     * Write a float
     * @param value float to write
     * @throws IOException if an IO exception occur
     */
    void writeFloat(float value) throws IOException;

    /**
     * Write a double
     * @param value double to write
     * @throws IOException if an IO exception occur
     */
    void writeDouble(double value) throws IOException;

    /**
     * Write a char
     * @param value char to write
     * @throws IOException if an IO exception occur
     */
    void writeChar(char value) throws IOException;

    /**
     * Write a String
     * @param value String to write
     * @throws IOException if an IO exception occur
     */
    void writeString(String value) throws IOException;

    /**
     * Write an object, if object is not a primitive or a {@link Object}, the type definition is added before the object.
     * @param o object to write
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for this object.
     * Sub object type error is placed as cause of a {@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    void write(Object o) throws IOException;

    /**
     * Write an object, if the object class is not the same than type class, like implementation, the type definition is added before the object.<br>
     * This method take a odin type to be use inside adapter by using {@link ObjectType#getField(String)} or all other field getter and
     * {@link com.newpixelcoffee.odin.internal.types.TypeField#getType()}
     * @param o object to write
     * @param type object generic type
     * @param <T> object generic type
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for this object.
     * Sub object type error is placed as cause of a {@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    <T> void writeTyped(T o, AnyType<T> type) throws IOException;

    /**
     * Write an object, odin get the {@link AnyType} associated to the given type, if the object class is not the same than type class, like implementation,
     * the type definition is added before the object.<br>
     * Custom type can be created to add generic argument ( can be useful for collection ) using {@link TypeBuilder}.
     * @param o object to write
     * @param type object type
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinTypeException if the type adapter can not be build for this object.
     * Sub object type error is placed as cause of a {@link com.newpixelcoffee.odin.exceptions.OdinAdapterException}
     * @throws com.newpixelcoffee.odin.exceptions.OdinAdapterException if a runtime exception is throw by an adapter when writing the object
     */
    void writeTyped(Object o, Type type) throws IOException;

    /**
     * Use a consumer to write content to a sub object. All writing do with the {@literal subWriter} are done inside a sub object, inside the {} delimiter
     * @param subWriter the sub writer consumer
     * @throws IOException if an IO exception occur
     */
    void writeSubContent(IOConsumer<OdinWriter> subWriter) throws IOException;

    /**
     * Write a new field with the given field name
     * @param name field name
     * @return a field writer for setting the value
     * @throws IOException if an IO exception occur
     * @throws com.newpixelcoffee.odin.exceptions.OdinFormatException if the name contains invalid characters ( <b>,:={}[]&lt;&gt;'"</b> )
     */
    OdinFieldWriter writeField(String name) throws IOException;

    /**
     * Write a new row, while there are no new call to this method, all value added using {@link OdinRowWriter} is write to same row
     * @return row writer for adding values
     * @throws IOException if an IO exception occur
     */
    OdinRowWriter writeRow() throws IOException;
}
