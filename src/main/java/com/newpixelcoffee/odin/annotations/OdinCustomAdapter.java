package com.newpixelcoffee.odin.annotations;

import com.newpixelcoffee.odin.adapters.OdinAdapter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Define a custom adapter for a field, useful if the field type has another adapter registered in Odin but a field need specific treatment.<br>
 * If the field value is an implementation of the field type and the implementation type also have an adapter registered in Odin, this custom adapter is forced and still used.<br>
 * Like standard Odin adapter, this adapter cannot be used with primitive and array type.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface OdinCustomAdapter {
    /**
     * Define the custom adapter for this field. Give a class implementing one of the {@link OdinAdapter}
     * @return class representing adapter to use
     */
    Class<? extends OdinAdapter> adapter();
}