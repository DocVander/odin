package com.newpixelcoffee.odin.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * If a field is a {@link java.util.Map} and the key of the map is inside values ( in a field ), you can write map as list of these values, so a map :
 * <pre>{@code map = {
 *     "key1" : {
 *         id = "key1"
 *         foo = 10
 *     }
 *     "key2" : {
 *         id = "key2"
 *         foo = 11
 *     }
 * }}</pre>
 * Can be replaced by :
 * <pre>{@code map = {
 *     {
 *         id = "key1"
 *         foo = 10
 *     }
 *     {
 *         id = "key2"
 *         foo = 11
 *     }
 * }}</pre>
 * using this annotations :
 * <pre>{@code @OdinMapAsList(keyField = "id")
 * Map<String, Foo> map;}</pre>
 * If the field value is an implementation of the field type and the implementation type also have an adapter registered in Odin, this adapter is forced and still used.<br>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface OdinMapAsList {
    String keyField();
}
