package com.newpixelcoffee.odin.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Type;

/**
 * Say to the default object adapter to use {@link com.newpixelcoffee.odin.OdinReader#readTo(Object)} instead of {@link com.newpixelcoffee.odin.OdinReader#readTyped(Type)} in order to keep the same
 * object instance.<br>
 * @see com.newpixelcoffee.odin.OdinReader#readTo(Object)
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface OdinReadTo {}
