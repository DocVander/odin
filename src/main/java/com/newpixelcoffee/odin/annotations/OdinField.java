package com.newpixelcoffee.odin.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Change the name of a field in Odin or add a comments that are write before the field by the default object adapter.<br>
 * Renamed field is only accessible by her new name in {@link com.newpixelcoffee.odin.ObjectType}, so :
 * <pre>{@code @OdinField(name = "custom field")
 * Test test;}</pre>
 * Is known only as "custom field" by {@link com.newpixelcoffee.odin.ObjectType#getField(String)}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface OdinField {

    /**
     * Change the name of this field in Odin
     * @return custom field name
     */
    String name();

    /**
     * Add comments to be write before the field by the default object adapter.
     * @return comments array, empty for nothing
     */
    String[] comments() default {};
}
