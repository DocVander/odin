package com.newpixelcoffee.odin;

import java.util.function.Predicate;

/**
 * @author DocVander
 */
public interface ObjectType<T> extends AnyType<T> {

    /**
     * Create new object instance. Call the default class constructor or use a unsafe invocation if no constructor is found.
     * @return a new object instance
     */
    T newInstance();

    /**
     * Get the type representing the {@link Class#getGenericSuperclass()}
     * @return parent type
     */
    ObjectType<? super T> getSuperClass();

    /**
     * Get a super class of this type, the target can be class or interface
     * @param target target class
     * @return class type
     */
    ObjectType<? super T> getSuperClass(Class<? super T> target);

    /**
     * Get generic argument for the processed class.<br>
     * For a class {@literal A<E>} extending {@literal B<T>} where only B have adapter, this methods return the generic argument know for B ( the adapter type ).<br>
     * To known the generic arguments of A ( this type ), use {@link #getGenericArgument(int)}
     * @param index argument index
     * @return argument type
     */
    AnyType getAdapterArgument(int index);

    /**
     * Get generic arguments for the processed class.<br>
     * For a class {@literal A<E>} extending {@literal B<T>} where only B have adapter, this methods return the generic arguments know for B ( the adapter type ).<br>
     * To known the generic arguments of A ( this type ), use {@link #getGenericArguments()}
     * @return arguments array
     */
    AnyType[] getAdapterArguments();

    /**
     * Get generic argument in given index, if this type does not have generic type arguments, this method return an unknown type instance.<br>
     * Else this methods load all arguments during the first call to {@link #getAdapterArguments()} or this method.<br>
     * <br>
     * For a class {@literal A<E>} extending {@literal B<T>} where only B have adapter, this methods return the generic argument know for A ( this type ).<br>
     * To known the generic arguments of B ( the adapter type ), use {@link #getAdapterArgument(int)}
     * @param index argument index
     * @return argument type
     */
    AnyType getGenericArgument(int index);

    /**
     * Get generic arguments array, if this type does not have generic type arguments, this method return an empty type array.<br>
     * Else this methods load all arguments during the first call to {@link #getAdapterArguments()} or this method.<br>
     * <br>
     * For a class {@literal A<E>} extending {@literal B<T>} where only B have adapter, this methods return the generic argument know for A ( this type ).<br>
     * To known the generic arguments of B ( the adapter type ), use {@link #getAdapterArguments()}
     * @return arguments
     */
    AnyType[] getGenericArguments();

    /**
     * Get field type. this method search field in {@link #getType()} class and then in all superclass recursively while no field is found for this name.<br>
     * Return null if no field was found for this name.<br>
     * This method can only return a field that matching the filter defined by {@link Odin#setFieldFilter(Predicate)}
     * @param name field name
     * @param <E> field content type
     * @return field type
     */
    <E> FieldType<E> findField(String name);

    /**
     * Get field type. this method search field only in {@link #getType()} class.<br>
     * Return null if no field was found for this name.<br>
     * This method can only return a field that matching the filter defined by {@link Odin#setFieldFilter(Predicate)}
     * @param name field name
     * @param <E> field content type
     * @return field type
     */
    <E> FieldType<E> getField(String name);

    /**
     * Get field type. this method is used to provide better performance in default object adapter. If no change was performed to class fields, the fields list has always the same order, so we
     * check actual index before search in all fields when reading this type.<br>
     * this method search field only in the class of this type.<br>
     * Return null if no field was found for this name.<br>
     * This method can only return a field that matching the filter defined by {@link Odin#setFieldFilter(Predicate)}
     * @param name field name
     * @param index field index
     * @param <E> field content type
     * @return field type
     */
    <E> FieldType<E> getField(String name, int index);

    /**
     * Get all fields of this {@link #getType()} class, this method does not return field of super class<br>
     * The result only contains fields that matching the filter defined by {@link Odin#setFieldFilter(Predicate)}
     * @return class fields
     */
    FieldType[] getFields();
}
